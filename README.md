# README #

### What is this repository for? ###

PolymerMD code using MPI library.
*_ PolymerMD_mpi.c : performs simulations starting from a random stretched configurations or from an input configuration. A simulation per each allocated CPU is performed
*_ wrapper_mpi.c: performs a MC optimization on the initial forcefield.dat file. Each step is tested on Ncpu runs, where Ncpu is the number of allocated CPUs
*_ genetic_mpi.c: performs a genetic optimization run combining several wrappers. The allocated CPUs are distributed among the wrappers.


### How do I get set up? ###

* to compile: make all

