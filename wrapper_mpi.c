//
//  wrapper.c
//
//  Created by Raffaello Potestio on May 7, 2014.
//  Copyright 2014 Max Planck Institute for Polymer Research. All rights reserved.
//



#define EXCL_VOL
#define SELF_INT_CHAIN
#define BENDING
#define TORSION
#define FIRST_ORDER
//#define SECOND_ORDER
//#define PRINT_GYR
#define VERBOSE
#define F_MAX (100000000)

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>
#include <stdbool.h>
#include <mpi.h>
#include "random.lib"
#include "my_memory.h"
#include "global_variables.c"
#include "definitions_and_structures.c"
#include "integrators.c"
#include "PolymerMD.h"
#include "input_output.c"
#include "compute_force.c"
#include "compute_energy.c"
#include "kabsch_wrapper.c"

//#define NITERS (100)
#define MAX_NBEADS (2000)
#define BLOCKSIZE (2)
#define LOWER_BOUND (4)
#define UPPER_BOUND (500)
#define TRAJ_CHUNK (50000)
#define RMSD_TOLERANCE (0.05)
#define WAIT_FOR_CHANGE (10)
#define EXPLORE_ELASTIC_PARAMS
//#define EXPLORE_ANGLES


void print_forcefield(char name[], chain_struct chain[]);
void print_forcefieldpp(char name[], chain_struct chain[], int pid);
void map_coords_on_vector(chain_struct chain[], double str[][3]);
double runner(int pid, chain_struct ext_chain[], chain_struct ref_chain[], bridge_struct bridge[]);




int main(int argc, char *argv[])
{
  int nprocs, i, j, k, n, l, pid, label, nblocks, check, block_index;
    double init_msd, avg_msd, msd_old, msd_new, delta_msd, beta, v;
    double param_old, param_new, sign;
    double str_target[MAX_NBEADS][3], str[MAX_NBEADS][3];
    int **cg_sequence;
    char name[100];

       
    if (argc <3 )
    {
      #ifdef VERBOSE
      fprintf(stderr,"Requires a parameter input file and a reference structure file. See source code.\n");
      #endif
      exit(1);
    }

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (world_rank == 0) {
       printf("Number of threads = %d\n\n", nprocs);
    }

    //DBG
    //FILE *outf;
    char rstr[80];
    sprintf(rstr,"outfile_%d.dat",world_rank);
    outf = fopen(rstr, "w");
    
    
    //msd array, one slot each process
    double msd[nprocs];
    double mymsd=0.0;

    beta = 1.0;

    sprintf(dirname, ".");
    sprintf(ffdirname, ".");
    
    chain_struct *chain, *temp_chain, *ref_chain;
    chain = NULL;
    temp_chain = NULL;
    ref_chain = NULL;
    bridge_struct *bridge;
    FILE *f_msd;

    if (world_rank == 0) {
      sprintf(name,"%s/msd_file.dat",dirname);
      if( ( f_msd = fopen(name,"w") ) == NULL )
	{
#ifdef VERBOSE
	  fprintf(stderr,"Cannot open file msd_file.dat\nAborting.\n");
#endif
	  exit(1);
	}

      fprintf(f_msd, "#iteration   msd\n");
      fclose(f_msd);
    }

    system_parameter_setup(world_rank, argv[1], &chain, &temp_chain, &bridge);
    remove_com(chain);


    
    //initialize bridge
    for(l=0; l<nbridges; l++){ 
      bridge[l].Vco = V_dsb(bridge[l].rco*bridge[l].sigma,bridge[l]); //the function take code units input
      bridge[l].fco = -dVdr_dsb(bridge[l].rco*bridge[l].sigma,bridge[l]); 
    }


    
    allocate_chain_struct(&ref_chain);
    read_conf(argv[2], ref_chain);

    read_Native_distance();

    map_coords_on_vector(chain, str);
    map_coords_on_vector(ref_chain, str_target);

    init_msd = best_rmsd(str, str_target, beads);
    init_msd = init_msd*init_msd;

    if (world_rank == 0) printf("Initial MSD: %8.5lf\n\n", init_msd);

    msd_old = init_msd;
    msd_new = init_msd;

    nblocks = (int)floor((double)(beads-2)/BLOCKSIZE) + 1;

    if (world_rank == 0) printf("The discretized sequence consists of %d blocks\n\n", nblocks);

    cg_sequence = i2t(nblocks, BLOCKSIZE);


    for(i = 0; i < beads-2; i++)
    {
        j = (int)floor((double)i/BLOCKSIZE);
        k = i%BLOCKSIZE;
        cg_sequence[j][k] = i+1;
        //printf("res %d block %d index %d\n", i+1, j, k);
    }




    /* MONTE CARLO BLOCK STARTS HERE */
    for(n = 0; n < niters; n++)
    {

#ifdef EXPLORE_ELASTIC_PARAMS
        // choose the parameter
        param_new = 0.0;	
	param_old=0.0;

	do
        {
          // choose a block and a parameter type
	  block_index = (int)intran2(&idum, nblocks);
	  k = (int)intran2(&idum, 2);
	  double rgas0 = gasdev(&idum);
	  printf("bi = %d\t k = %d\t rg = %.4f\n",block_index,k,rgas0);
	  //propagate random picks from root
	  
	  MPI_Barrier(MPI_COMM_WORLD);
	  MPI_Bcast(&block_index, 1, MPI_INT , 0,  MPI_COMM_WORLD);
	  MPI_Bcast(&k, 1, MPI_INT , 0,  MPI_COMM_WORLD);
	  MPI_Bcast(&rgas0, 1, MPI_DOUBLE , 0,  MPI_COMM_WORLD);
	  MPI_Barrier(MPI_COMM_WORLD);
	  
	  //fprintf(outf,"iter = %d\tbi = %d\t k = %d\t rg = %.4f\n",n,block_index,k,rgas0); //DBG
	  //fflush(outf);
	  
	  // store the old value
	  i = cg_sequence[block_index][0];
	  
	  check = 0;
	  if((k==1)&&(i==(beads-2))) check = 1;
        
	  switch(k)
            {
	    case 0:
	      param_old = chain[i].bendparam.kb;
	      break;
	    case 1:
	      param_old = chain[i].torsionparam.kt1;
	      break;
            }

            /*
            sign = 1.0;
            if(intran2(&idum, 2)>0.0) sign = -1.0;
            param_new = param_old + 1.0*sign;
            */
	  param_new = param_old + sigma_el*rgas0;



        }while((param_new < LOWER_BOUND)||(param_new > UPPER_BOUND)||(check==1));

        if(world_rank == 0 ) printf("--- %d %d %8.1lf %8.1lf\n\n", block_index, k, param_old, param_new);

	//---> MPI communicate block_index,k,param_old,param_new from 0 to others

        // change the parameter (every process does the same)
        for(i = 0; i < BLOCKSIZE; i++)
        {

            j = cg_sequence[block_index][i];

            check = 0;
            if((k==1)&&(i==(beads-2))) check = 1;

            switch(k)
            {
                case 0:
                  chain[j].bendparam.kb = param_new;
                  break;
                case 1:
                  chain[j].torsionparam.kt1 = param_new;
                  chain[j].torsionparam.kt2 = param_new/3.0;
                  break;
            }

            if(j == beads-2) break;

        }

#endif



#ifdef EXPLORE_ANGLES
        // assign temp angle
        k = (int)intran2(&idum, N_nonfixed_angle);
	double rgas1 = gasdev(&idum);
	//propagate random picks from root

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Bcast(&k, 1, MPI_INT , 0,  MPI_COMM_WORLD);
	MPI_Bcast(&rgas1, 1, MPI_DOUBLE , 0,  MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	
	i = nonfixed_angle[k];
        param_old = chain[i].bendparam.tetha;
	
	param_new = param_old + sigma_ang*rgas;
	//---> MPI communiicate i,param_old,param_new from 0 to others
       	chain[i].bendparam.tetha = param_new;
#endif
	
    	//---> MPI copy chain from rank = 0 to other processes (locked)
    	//MPI_Barrier(MPI_COMM_WORLD);
    	//MPI_Bcast(chain, beads, chain_mpi, 0,  MPI_COMM_WORLD);
    	//MPI_Barrier(MPI_COMM_WORLD);
	

	
	// run the simulation
        if(world_rank == 0) printf("Iteration # %d\n", n);
	fprintf(outf,"Iteration # %d\n", n);
	fprintf(outf,"\nMy seed is %ld\n",idum); //DBG
	fflush(outf);
	/*for(int k=0; k<beads; k++){
	  //fprintf(outf,"%d\t%.f %.f %.f %.f %.f %.f\t",n,chain[k].pos.bead,chain[k].vel.bead,chain[k].force.bead,chain[k].kinetic.bead,chain[k].A_term.bead,chain[k].random_term.bead);
	  //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].pos.x,chain[k].pos.y,chain[k].pos.z);
	  //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].vel.x,chain[k].vel.y,chain[k].vel.z);
	  //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].force.x,chain[k].force.y,chain[k].force.z);
	  //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].kinetic.x,chain[k].kinetic.y,chain[k].kinetic.z);
	  //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].A_term.x,chain[k].A_term.y,chain[k].A_term.z);
	  //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].random_term.x,chain[k].random_term.y,chain[k].random_term.z);
	  fprintf(outf,"%d  %d\t%.8lf\t%.8lf\t%d\t%.8lf\t%.8lf\t%.8lf\n",n,k,chain[k].bendparam.kb,chain[k].bendparam.tetha,chain[k].bendparam.change_angle,chain[k].torsionparam.kt1, chain[k].torsionparam.kt2, chain[k].torsionparam.phi);
	  }*/
	
	// nprocs runners
	//GENE change me in me_run
	
	label = n*1000+world_rank;
	
	initialize_chain(chain); //re-intialize configuration
	mymsd = runner(label, chain, ref_chain, bridge);
	//mymsd = 30.0;
	//fprintf(outf,"\nMy msd is %.8f\n",mymsd); //DBG
	//fflush(outf);
	//---> MPI (barrier to wait for all the runners to average)
	MPI_Barrier(MPI_COMM_WORLD);
        MPI_Allgather(&mymsd, 1, MPI_DOUBLE, msd, 1, MPI_DOUBLE, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
	//---> MPI communicate msd all around
	
	avg_msd = 0.0;
	for(int k = 0; k < nprocs; k++)
	  {
	    //fprintf(outf,"%d\t%.8lf\n",n,msd[k]); //DBG
	    msd[k] = msd[k]*msd[k];
	    if(world_rank == 0) printf("Proc %d - msd = %8.5lf\n", k, msd[k]);
	    
	    avg_msd += msd[k];
	  }
	avg_msd = avg_msd/nprocs;
	if(world_rank == 0) printf("Average msd = %8.5lf\n", avg_msd);
	  
	// calculate the difference between new and old MSD
	msd_new = avg_msd;
	delta_msd = msd_new - msd_old;
	
	// accept/reject new parameter
	if(delta_msd<=0.0)
	  {
	    // accept
	    msd_old = msd_new;
	  }
	else
	  {
	    
	    //v = 10.0; // greedy annealing
	    v = ran2(&idum); // Metropolis algorithm 	    
	    //propagate random pick from root
	    MPI_Barrier(MPI_COMM_WORLD);
	    MPI_Bcast(&v, 1, MPI_DOUBLE , 0,  MPI_COMM_WORLD);
	    MPI_Barrier(MPI_COMM_WORLD);
	    //fprintf(outf,"\ndelta =  %.8f\t random = %.8f\n",delta_msd,v); //DBG
	    //fflush(outf);
	    if(v > exp(-delta_msd*beta)) // reject
		{
#ifdef EXPLORE_ELASTIC_PARAMS
		  // change the parameter back
		  for(i = 0; i < BLOCKSIZE; i++)
		    {
		      
		      j = cg_sequence[block_index][i];
		      switch(k)
			{
                        case 0:
                          chain[j].bendparam.kb = param_old;
                          break;
                        case 1:
			  chain[j].torsionparam.kt1 = param_old;
			  chain[j].torsionparam.kt2 = param_old/3.0;
			  break;
			}
		      
		      if(j == beads-2) break;
		      
		    }
#endif
		  
#ifdef EXPLORE_ANGLES
		  // reassign old angle
		  chain[i].bendparam.tetha = param_old;
#endif
		  
		  
		  
		}
	    else
	      {
		// accept
		msd_old = msd_new;
	      }
	      
	    
	      
	  }
	
	//---> MPI 0 writes all the following

	if(world_rank == 0){
	  printf("New msd = %8.5lf\n\n", msd_old);
	  //Create output directory
	  struct stat st = {0};
	  sprintf(name,"%s/forcefield/",dirname);
	  if (stat(name, &st) == -1) {
	    mkdir(name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	  }
	  
	  sprintf(name, "%s/forcefield/%d_forcefield.dat",dirname, n*1000);
	  print_forcefield(name, chain);
	  if(delta_msd<=0.0)
	    {
	      
	      //////////////////////////////
	      sprintf(name, "%s/forcefieldpp.dat",dirname);
	      FILE *forcefieldpp_dat;
	      forcefieldpp_dat = fopen(name, "a");
	      
	      for(i = 0; i < beads-2; i++)
		{
		  fprintf(forcefieldpp_dat, "%3d%6d%13.5lf%13.5lf\n",n, i+1, chain[i+1].bendparam.kb, chain[i+1].torsionparam.kt1);
		}
	      
	      fclose(forcefieldpp_dat);
	      
	      /////////////////////////////////
	      sprintf(name, "%s/best_forcefield.dat",dirname);
	      print_forcefield(name, chain);
	    }
	  sprintf(name,"%s/msd_file.dat",dirname);
	  f_msd = fopen(name,"a");
	  fprintf(f_msd, "%8d   %8.5lf\n", n, msd_old);
	  fclose(f_msd);
	}
	//fprintf(outf,"prima barriera\n"); //DBG
	MPI_Barrier(MPI_COMM_WORLD);
	//fprintf(outf,"dopo barriera\n"); //DBG
	
    }
    /* MONTE CARLO BLOCK ENDS HERE */
    
    fclose(outf); 
    
    free(chain);
    free(temp_chain);
    free(ref_chain);
    free_i2t(cg_sequence);
    free_d2t(DIST);
    
    MPI_Finalize();
    return(0);

}




/********************************************/









void map_coords_on_vector(chain_struct chain[], double str[][3])
{
    int i;
    for(i = 0; i < beads; i++)
    {
        str[i][0] = chain[i].pos.x;
        str[i][1] = chain[i].pos.y;
        str[i][2] = chain[i].pos.z;
    }
}


void print_forcefield(char name[], chain_struct chain[])
{
    
    int i;
    //char name[100];
    //sprintf(name, "%d_forcefield.dat", pid);

    FILE *ff;
    if( ( ff = fopen(name,"w") ) == NULL )
    {
      #ifdef VERBOSE
      fprintf(stderr,"Cannot open file %s\nAborting.\n", name);
      #endif
      exit(1);
    }

    fprintf(ff, "#Elastic parameters          kb       theta         kt1         kt2         phi\n");
    fprintf(ff, "#atom\n");

    for(i = 0; i < beads-2; i++)
    {
      //printf("Writing %d %lf %lf %lf %lf %lf %d\n", i+1, chain[i+1].bendparam.kb,  chain[i+1].bendparam.tetha,chain[i+1].torsionparam.kt1, chain[i+1].torsionparam.kt2, chain[i+1].torsionparam.phi, chain[i+1].bendparam.change_angle);
        fprintf(ff, "%3d                    ", i+1);
        fprintf(ff, "%8.1lf    ", chain[i+1].bendparam.kb);
        fprintf(ff, "%8.5lf    ", chain[i+1].bendparam.tetha);
        fprintf(ff, "%8.1lf    ", chain[i+1].torsionparam.kt1);
        fprintf(ff, "%8.1lf    ", chain[i+1].torsionparam.kt2);
        fprintf(ff, "%8.5lf\n", chain[i+1].torsionparam.phi);
    }

    fclose(ff);





}







double runner(int pid, chain_struct ext_chain[], chain_struct ref_chain[], bridge_struct bridge[])
{

    int i, j, Nchunks, l, n, leave;
    double str1[beads][3], str2[beads][3];
    double msd, old_msd;
    bool continue_run = true;

    chain_struct *chain, *temp_chain;
    chain = NULL;
    temp_chain = NULL;

    allocate_chain_struct(&chain);
    allocate_chain_struct(&temp_chain);
    copy_chain_and_parameters(ext_chain, chain);
    copy_chain_and_parameters(ext_chain, temp_chain);

    map_coords_on_vector(ref_chain, str1);

    double **DISP;
    DISP = d2t(beads,3);

    energy_struct energy;
    initialize_energy_struct(&energy);

    neighbor_list *neighbors;
    neighbors = NULL;
    allocate_neighbor_list(&neighbors);
    update_neighbors_VL(chain, neighbors, DISP);

    //here the trajectory and energy file buffers are defined
    char name[128];
    sprintf(name, "%s/traj/traj_%d.vtf",dirname, pid);
    FILE *trj_vtf;
    trj_vtf = fopen(name, "w");
    FILE *gyration;
#ifdef PRINT_GYR
    sprintf(name, "%s/gyration/gyration_%d.vtf",dirname,pid);
    gyration = fopen(name, "w");
#endif
    sprintf(name, "%s/energy/potential_%d.dat",dirname,pid);
    FILE *potential;
    potential = fopen(name, "w");
    sprintf(name, "%s/energy/kinetic_%d.dat",dirname,pid);
    FILE *kinetic;
    kinetic = fopen(name, "w");
    sprintf(name, "%s/energy/totenergy_%d.dat",dirname,pid);
    FILE *totenergy;
    totenergy = fopen(name, "w");
 

    
    if(printfreq>0)
    {
      print_traj(chain, 0, trj_vtf, gyration);
        tmp_Nsteps = Nsteps;
        Nsteps = ((Nsteps/printfreq))*printfreq;
        #ifdef VERBOSE
        if(tmp_Nsteps != Nsteps) printf("Changed Nsteps from %d to %d\n", tmp_Nsteps, Nsteps);
        #endif
    }

    

    
    Nchunks = (int)floor(Nsteps/TRAJ_CHUNK);
    i = 1;
    leave = 0;

    //for(i = 1; i <= Nsteps; i++)
    for(l = 0; l < Nchunks; l++)
    {

        for(n = 0; n < TRAJ_CHUNK; n++)
        {

        #ifdef FIRST_ORDER
	  continue_run = force(chain, neighbors,bridge);
        if(continue_run == false) break;
        evolve_v_first_order(chain);
        evolve_x_first_order(chain, DISP);
        #endif



        #ifdef SECOND_ORDER
        copy_chain(chain, temp_chain);
        continue_run = force(temp_chain, neighbors,bridge);
        if(continue_run == false) break;
        evolve_x(chain, temp_chain, DISP);
        #endif
        
        for ( j = 0 ; j < beads ; j ++)
        {
          if (DISP[j][0]*DISP[j][0] + DISP[j][1]*DISP[j][1] + DISP[j][2]*DISP[j][2] >= 0.25 * DR2 )
          {
            update_neighbors_VL(chain, neighbors, DISP);
            break;
          }
        }

        #ifdef SECOND_ORDER
        continue_run = force(chain, neighbors,bridge);
        if(continue_run == false) break;
        evolve_v(chain, temp_chain);
        remove_com(chain);
        #endif

        if(printfreq>0)
        {
            if(i%printfreq==0)
            {
	      print_traj(chain, i, trj_vtf, gyration);
                Energy(chain, &energy, bridge);
                print_Energy(energy, i, potential, kinetic, totenergy);
            }
        }

        #ifdef VERBOSE
        if(i%10000==0) printf("Done %8.2e of %8.2e steps\n", (double)i, (double)Nsteps);
        #endif

        i++;

        }

        if(continue_run == false) break;

        map_coords_on_vector(chain, str2);
        msd = best_rmsd(str1, str2, beads);
	//leaving snippet
	/*
        if((l>0)&&(fabs(old_msd - msd) < RMSD_TOLERANCE))
        {
            leave++;
        }
        else
        {
            leave = 0;
        }
        old_msd = msd;
        //printf("ID %d msd %lf old_msd %lf leave %d\n", omp_get_thread_num(), msd, old_msd, leave);

        if(leave == WAIT_FOR_CHANGE)
        {
            printf("breaking run on proc rank %d\n", world_rank);
            break;
	    }*/


    }

    print_final_conf(pid, chain,i);

    map_coords_on_vector(chain, str2);
    msd = best_rmsd(str1, str2, beads);



    free_d2t(DISP);
    for(i = 0; i < beads; i++) free_i1t(neighbors[i].index);
    free(neighbors);
    free(chain);
    free(temp_chain);

    fclose(trj_vtf);
    fclose(kinetic);
    fclose(potential);
    fclose(totenergy);
#ifdef PRINT_GYR
    fclose(gyration);
#endif
    
    
    if(continue_run == false) msd = 1000000000;

    return msd;
    
}



/////////////////////////////////////////


/*void print_forcefieldpp(char name[], chain_struct chain[], int pid)
{
    
    int i;
    char name[101];

    //sprintf(name, "forcefieldpp_%d.dat", pid);
    FILE *forcefieldpp_dat;
    forcefieldpp_dat = fopen(namen, "a");

    for(i = 0; i < beads-2; i++)
    {
        fprintf(forcefieldpp_dat, "%3d                    ", n);
        fprintf(forcefieldpp_dat, "%3d                    ", i+1);
        fprintf(forcefieldpp_dat, "%8.1lf    ", chain[i+1].bendparam.kb);
        fprintf(forcefieldpp_dat, "%8.1lf    ", chain[i+1].torsionparam.kt1);

    }

    fclose(forcefieldpp_dat);

}*/





/////////////////////////////////////////






