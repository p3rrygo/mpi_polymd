bool force(chain_struct chain[], neighbor_list neighbors[], bridge_struct bridge[]);
void set_forces_to_zero(chain_struct chain[]);
bool fene_bond_force(chain_struct chain[]);
void bending_force(chain_struct chain[]);
void wca_excluded_volume_force(chain_struct chain[], neighbor_list neighbors[]);
void bridge_force(chain_struct chain[], bridge_struct bridge[]);
void torsion_force(chain_struct chain[]);
void torsion_force_dbg(chain_struct chain[], chain_struct chain_en[]); //DBG
void Capture_force(chain_struct chain[]);
double dVdr(double d);
double dVdr_LJ(double d);
double dVdr_dsb(double d,bridge_struct bridge);
double DV_nativeDr(double d, double d_zero);


bool force(chain_struct chain[], neighbor_list neighbors[], bridge_struct bridge[])
{

  bool check = true;

  set_forces_to_zero(chain);

  check = fene_bond_force(chain);

  #ifdef EXCL_VOL
  wca_excluded_volume_force(chain, neighbors);
  #endif

  if(nbridges>0) bridge_force(chain,bridge);
  
  #ifdef BENDING
  bending_force(chain);
  #endif

  #ifdef TORSION
  torsion_force(chain);
  #endif


  
  return check;

}



void set_forces_to_zero(chain_struct chain[])
{
    int i;
    for (i = 0; i < beads; i++)
    {
        chain[i].force.x = 0.0;
        chain[i].force.y = 0.0;
        chain[i].force.z = 0.0;
    }
}



bool fene_bond_force(chain_struct chain[])
{

  int i;
  double dsq, xsq, ysq, zsq, xdiff, ydiff, zdiff;
  bool check = true;

  for (i = 0; i < beads-1; i++)
  {

    xdiff = chain[i].pos.x - chain[i+1].pos.x;
    ydiff = chain[i].pos.y - chain[i+1].pos.y;
    zdiff = chain[i].pos.z - chain[i+1].pos.z;

    xsq = xdiff*xdiff;
    ysq = ydiff*ydiff;
    zsq = zdiff*zdiff;

    //dsq = 1.0 - (xsq + ysq + zsq)/dzerosq;

    dsq = (xsq + ysq + zsq)/dzerosq;

    if ( dsq > four_thirds_squared)
    {
      //fprintf(stderr,"bond %d %d broken - %lf %lf %lf %lf",i,i+1, dsq, xsq, ysq, zsq);
      //fprintf(stderr,". Aborting.\n");
      //exit(1);
      check = false;
      break;
    }


    if(dsq > fene_capping*fene_capping) dsq = fene_capping*fene_capping;

    dsq = 1.0 - dsq;

    dsq = 1.0/dsq;

    chain[i].force.x += - kappa*dsq*xdiff;
    chain[i].force.y += - kappa*dsq*ydiff;
    chain[i].force.z += - kappa*dsq*zdiff;

    chain[i+1].force.x += kappa*dsq*xdiff;
    chain[i+1].force.y += kappa*dsq*ydiff;
    chain[i+1].force.z += kappa*dsq*zdiff;
    

  }

  return check;

}




void bending_force(chain_struct chain[])
{

  int i;
  double d1, d2;
  double dsq1, dsq2, xsq1, ysq1, zsq1, xsq2, ysq2, zsq2;
  double xdiff1, ydiff1, zdiff1, xdiff2, ydiff2, zdiff2;
  double fbendx1, fbendy1, fbendz1, fbendx3, fbendy3, fbendz3, M, U, L;
  double kb, theta;


  for (i = 1; i < beads-1; i++)
    {

      xdiff1 = chain[i].pos.x - chain[i-1].pos.x;
      ydiff1 = chain[i].pos.y - chain[i-1].pos.y;
      zdiff1 = chain[i].pos.z - chain[i-1].pos.z;

      xdiff2 = chain[i+1].pos.x - chain[i].pos.x;
      ydiff2 = chain[i+1].pos.y - chain[i].pos.y;
      zdiff2 = chain[i+1].pos.z - chain[i].pos.z;

      xsq1 = xdiff1*xdiff1;
      ysq1 = ydiff1*ydiff1;
      zsq1 = zdiff1*zdiff1;

      xsq2 = xdiff2*xdiff2;
      ysq2 = ydiff2*ydiff2;
      zsq2 = zdiff2*zdiff2;
      
      d1 = sqrt(xsq1 + ysq1 + zsq1);
      d2 = sqrt(xsq2 + ysq2 + zsq2);
      
      M = - (xdiff1*xdiff2 + ydiff1*ydiff2 + zdiff1*zdiff2);
        
      U = M/(d1*d2);

      kb = chain[i].bendparam.kb;
      theta = chain[i].bendparam.tetha;

      L = 2*kb*(acos(U) - theta)/(sqrt(1-U*U));

      d1 = 1.0/d1;
      d2 = 1.0/d2;

      fbendx1 = L*d1*(xdiff2*d2 + U*xdiff1*d1);
      fbendy1 = L*d1*(ydiff2*d2 + U*ydiff1*d1);
      fbendz1 = L*d1*(zdiff2*d2 + U*zdiff1*d1);

      fbendx3 = -L*d2*(xdiff1*d1 + U*xdiff2*d2);
      fbendy3 = -L*d2*(ydiff1*d1 + U*ydiff2*d2);
      fbendz3 = -L*d2*(zdiff1*d1 + U*zdiff2*d2);

      chain[i-1].force.x += fbendx1;
      chain[i-1].force.y += fbendy1;
      chain[i-1].force.z += fbendz1;

      chain[i].force.x += -(fbendx1 + fbendx3);
      chain[i].force.y += -(fbendy1 + fbendy3);
      chain[i].force.z += -(fbendz1 + fbendz3);

      chain[i+1].force.x += fbendx3;
      chain[i+1].force.y += fbendy3;
      chain[i+1].force.z += fbendz3;
    }

}




void wca_excluded_volume_force(chain_struct chain[], neighbor_list neighbors[])
{

  int i, j, k, n;
  double d, f, dsq, xsq, ysq, zsq, xdiff, ydiff, zdiff, d_zero;

#ifdef SELF_INT_CHAIN
    for (i = 0; i < beads; i++)
    {

      n = neighbors[i].n;
      for(j = 0; j < n; j++)
      {

        k = neighbors[i].index[j];

        xdiff = chain[i].pos.x - chain[k].pos.x;
        ydiff = chain[i].pos.y - chain[k].pos.y;
        zdiff = chain[i].pos.z - chain[k].pos.z;

        xsq = xdiff*xdiff;
        ysq = ydiff*ydiff;
        zsq = zdiff*zdiff;
        dsq = xsq + ysq + zsq;
            
        d = sqrt(dsq);
        f = - dVdr(d)/d;

        chain[i].force.x += f*xdiff;
        chain[i].force.y += f*ydiff;
        chain[i].force.z += f*zdiff;
	//printf("1 %d %d %d: %lf - %lf - %lf - %lf - %lf - %lf - %lf - %lf\n",i,k,n, xdiff, ydiff,zdiff, d, f, chain[i].force.x, chain[i].force.y, chain[i].force.z); //DBG
        // adding native (attractive) non-bonded interactions
        d_zero = DIST[i][k];
        if(d_zero>0.0)
        {
          f = - DV_nativeDr(d, d_zero)/d;

          chain[i].force.x += f*xdiff;
          chain[i].force.y += f*ydiff;
          chain[i].force.z += f*zdiff;
        }
	//printf("2 %d %d: %lf - %lf - %lf\n",i,j, chain[i].force.x, chain[i].force.y, chain[i].force.z); //DBG


      }
    }
#else
    for (i = 0; i < beads-1; i++)
    {
        
      xdiff = chain[i].pos.x - chain[i+1].pos.x;
      ydiff = chain[i].pos.y - chain[i+1].pos.y;
      zdiff = chain[i].pos.z - chain[i+1].pos.z;

      xsq = xdiff*xdiff;
      ysq = ydiff*ydiff;
      zsq = zdiff*zdiff;
      dsq = xsq + ysq + zsq;
            
      d = sqrt(dsq);
      f = - dVdr(d)/d;

      chain[i].force.x += f*xdiff;
      chain[i].force.y += f*ydiff;
      chain[i].force.z += f*zdiff;

      chain[i+1].force.x += -f*xdiff;
      chain[i+1].force.y += -f*ydiff;
      chain[i+1].force.z += -f*zdiff;      


    }



#endif


}


 void bridge_force(chain_struct chain[], bridge_struct bridge[]){
   
  int i, j, k, n;
  double d, f, dsq, xsq, ysq, zsq, xdiff, ydiff, zdiff;
  
  for (n = 0; n < nbridges; n++){
    i=bridge[n].bead_i;
    j=bridge[n].bead_j;
    
    xdiff = chain[i].pos.x - chain[j].pos.x;
    ydiff = chain[i].pos.y - chain[j].pos.y;
    zdiff = chain[i].pos.z - chain[j].pos.z;
    
    xsq = xdiff*xdiff;
    ysq = ydiff*ydiff;
    zsq = zdiff*zdiff;
    dsq = xsq + ysq + zsq;
            
    d = sqrt(dsq);
    f = - dVdr_dsb(d,bridge[n]);
 
    chain[i].force.x += f*xdiff;
    chain[i].force.y += f*ydiff;
    chain[i].force.z += f*zdiff;
    chain[j].force.x -= f*xdiff;
    chain[j].force.y -= f*ydiff;
    chain[j].force.z -= f*zdiff;

    
  }

}
 




void torsion_force(chain_struct chain[])
{

    int i, j, k;
    double a[3], b[3], c[3], u[3], v[3], f_u[3], f_v[3], q0[3], q1[3], q2[3], q3[3];
    double du_da[3][3], du_db[3][3], dv_db[3][3];
    double abs_u, abs_v, u_scal_v, Q, P;
    double kt1, kt2, phi_0, phi;
    double d3[3], sgn;


    for (i = 1; i < beads-2; i++)
    {


        a[0] = chain[i].pos.x - chain[i-1].pos.x;
        a[1] = chain[i].pos.y - chain[i-1].pos.y;
        a[2] = chain[i].pos.z - chain[i-1].pos.z;

        b[0] = chain[i+1].pos.x - chain[i].pos.x;
        b[1] = chain[i+1].pos.y - chain[i].pos.y;
        b[2] = chain[i+1].pos.z - chain[i].pos.z;

        c[0] = chain[i+2].pos.x - chain[i+1].pos.x;
        c[1] = chain[i+2].pos.y - chain[i+1].pos.y;
        c[2] = chain[i+2].pos.z - chain[i+1].pos.z;

        u[0] = vector_p_0(a, b);
        u[1] = vector_p_1(a, b);
        u[2] = vector_p_2(a, b);
        
        v[0] = vector_p_0(b, c);
        v[1] = vector_p_1(b, c);
        v[2] = vector_p_2(b, c);

        abs_u = scalar_p(u, u);
        abs_u = sqrt(abs_u);

        abs_v = scalar_p(v, v);
        abs_v = sqrt(abs_v);

        u_scal_v = scalar_p(u, v);

        for(j = 0; j < 3; j++)
          {
            f_u[j] = ( v[j] - u[j]*u_scal_v/(abs_u*abs_u) )/(abs_u*abs_v);
            f_v[j] = ( u[j] - v[j]*u_scal_v/(abs_v*abs_v) )/(abs_u*abs_v);
          }

        for(j = 0; j < 3; j++)
          {
            du_da[j][j] = 0.0;
            du_db[j][j] = 0.0;
            dv_db[j][j] = 0.0;
          }

        du_da[0][1] =   b[2];
        du_da[1][0] = - b[2];
        du_da[2][0] =   b[1];
        du_da[0][2] = - b[1];
        du_da[1][2] =   b[0];
        du_da[2][1] = - b[0];

        du_db[0][1] = - a[2];
        du_db[1][0] =   a[2];
        du_db[2][0] = - a[1];
        du_db[0][2] =   a[1];
        du_db[1][2] = - a[0];
        du_db[2][1] =   a[0];

        dv_db[0][1] =   c[2];
        dv_db[1][0] = - c[2];
        dv_db[2][0] =   c[1];
        dv_db[0][2] = - c[1];
        dv_db[1][2] =   c[0];
        dv_db[2][1] = - c[0];

        for(j = 0; j < 3; j++)
          {

            q0[j] = 0.0;
            q1[j] = 0.0;
            q2[j] = 0.0;
            q3[j] = 0.0;
            for(k = 0; k < 3; k++)
              {
                q0[j] += - f_u[k]*du_da[k][j];
                q1[j] +=   f_u[k]*(du_da[k][j] - du_db[k][j]) - f_v[k]*dv_db[k][j];
                q2[j] +=   f_u[k]*du_db[k][j] + f_v[k]*(dv_db[k][j] + du_da[k][j]);
                q3[j] += - f_v[k]*du_da[k][j];
              }
          }

        Q = u_scal_v/(abs_u*abs_v);

        if(Q >  torsion_cutoff) Q =  torsion_cutoff;
        if(Q < -torsion_cutoff) Q = -torsion_cutoff;

        kt1 = chain[i].torsionparam.kt1;
        kt2 = chain[i].torsionparam.kt2;
        phi_0 = chain[i].torsionparam.phi;

        d3[0] = vector_p_0(u, v);
        d3[1] = vector_p_1(u, v);
        d3[2] = vector_p_2(u, v);

        sgn = scalar_p(d3, b);
        sgn = sgn/sqrt(sgn*sgn);

        phi = sgn*acos(Q);

        P = (kt1*sin(phi - phi_0) + 3*kt2*sin(3*(phi - phi_0)))/sin(phi);

        chain[i-1].force.x += P*q0[0];
        chain[i-1].force.y += P*q0[1];
        chain[i-1].force.z += P*q0[2];

        chain[i].force.x   += P*q1[0];
        chain[i].force.y   += P*q1[1];
        chain[i].force.z   += P*q1[2];

        chain[i+1].force.x += P*q2[0];
        chain[i+1].force.y += P*q2[1];
        chain[i+1].force.z += P*q2[2];

        chain[i+2].force.x += P*q3[0];
        chain[i+2].force.y += P*q3[1];
        chain[i+2].force.z += P*q3[2];

	
    }

}



void torsion_force_dbg(chain_struct chain[], chain_struct chain_en[])
{

    int i, j, k;
    double a[3], b[3], c[3], u[3], v[3], f_u[3], f_v[3], q0[3], q1[3], q2[3], q3[3];
    double du_da[3][3], du_db[3][3], dv_db[3][3];
    double abs_u, abs_v, u_scal_v, Q, P;
    double kt1, kt2, phi_0, phi;
    double d3[3], sgn;


    for (i = 1; i < beads-2; i++)
    {


        a[0] = chain[i].pos.x - chain[i-1].pos.x;
        a[1] = chain[i].pos.y - chain[i-1].pos.y;
        a[2] = chain[i].pos.z - chain[i-1].pos.z;

        b[0] = chain[i+1].pos.x - chain[i].pos.x;
        b[1] = chain[i+1].pos.y - chain[i].pos.y;
        b[2] = chain[i+1].pos.z - chain[i].pos.z;

        c[0] = chain[i+2].pos.x - chain[i+1].pos.x;
        c[1] = chain[i+2].pos.y - chain[i+1].pos.y;
        c[2] = chain[i+2].pos.z - chain[i+1].pos.z;

        u[0] = vector_p_0(a, b);
        u[1] = vector_p_1(a, b);
        u[2] = vector_p_2(a, b);
        
        v[0] = vector_p_0(b, c);
        v[1] = vector_p_1(b, c);
        v[2] = vector_p_2(b, c);

        abs_u = scalar_p(u, u);
        abs_u = sqrt(abs_u);

        abs_v = scalar_p(v, v);
        abs_v = sqrt(abs_v);

        u_scal_v = scalar_p(u, v);

        for(j = 0; j < 3; j++)
          {
            f_u[j] = ( v[j] - u[j]*u_scal_v/(abs_u*abs_u) )/(abs_u*abs_v);
            f_v[j] = ( u[j] - v[j]*u_scal_v/(abs_v*abs_v) )/(abs_u*abs_v);
          }

        for(j = 0; j < 3; j++)
          {
            du_da[j][j] = 0.0;
            du_db[j][j] = 0.0;
            dv_db[j][j] = 0.0;
          }

        du_da[0][1] =   b[2];
        du_da[1][0] = - b[2];
        du_da[2][0] =   b[1];
        du_da[0][2] = - b[1];
        du_da[1][2] =   b[0];
        du_da[2][1] = - b[0];

        du_db[0][1] = - a[2];
        du_db[1][0] =   a[2];
        du_db[2][0] = - a[1];
        du_db[0][2] =   a[1];
        du_db[1][2] = - a[0];
        du_db[2][1] =   a[0];

        dv_db[0][1] =   c[2];
        dv_db[1][0] = - c[2];
        dv_db[2][0] =   c[1];
        dv_db[0][2] = - c[1];
        dv_db[1][2] =   c[0];
        dv_db[2][1] = - c[0];

        for(j = 0; j < 3; j++)
          {

            q0[j] = 0.0;
            q1[j] = 0.0;
            q2[j] = 0.0;
            q3[j] = 0.0;
            for(k = 0; k < 3; k++)
              {
                q0[j] += - f_u[k]*du_da[k][j];
                q1[j] +=   f_u[k]*(du_da[k][j] - du_db[k][j]) - f_v[k]*dv_db[k][j];
                q2[j] +=   f_u[k]*du_db[k][j] + f_v[k]*(dv_db[k][j] + du_da[k][j]);
                q3[j] += - f_v[k]*du_da[k][j];
              }
          }

        Q = u_scal_v/(abs_u*abs_v);

        if(Q >  torsion_cutoff) Q =  torsion_cutoff;
        if(Q < -torsion_cutoff) Q = -torsion_cutoff;

        kt1 = chain[i].torsionparam.kt1;
        kt2 = chain[i].torsionparam.kt2;
        phi_0 = chain[i].torsionparam.phi;

        d3[0] = vector_p_0(u, v);
        d3[1] = vector_p_1(u, v);
        d3[2] = vector_p_2(u, v);

        sgn = scalar_p(d3, b);
        sgn = sgn/sqrt(sgn*sgn);

        phi = sgn*acos(Q);

        P = (kt1*sin(phi - phi_0) + 3*kt2*sin(3*(phi - phi_0)))/sin(phi);

        //chain[i-1].force.x += P*q0[0];
        //chain[i-1].force.y += P*q0[1];
        //chain[i-1].force.z += P*q0[2];

	// chain[i].force.x   += P*q1[0];
        //chain[i].force.y   += P*q1[1];
        //chain[i].force.z   += P*q1[2];

        //chain[i+1].force.x += P*q2[0];
        //chain[i+1].force.y += P*q2[1];
        //chain[i+1].force.z += P*q2[2];

        //chain[i+2].force.x += P*q3[0];
        //chain[i+2].force.y += P*q3[1];
        //chain[i+2].force.z += P*q3[2];

	chain_en[i].force.x = phi;
	chain_en[i].force.y = phi_0 ;
	chain_en[i].force.z = cos(phi-phi_0);
	chain_en[i].kinetic.x = kt1*(1 - cos(phi - phi_0));
	chain_en[i].kinetic.y = kt2*(1 - cos(3*(phi - phi_0)));
	chain_en[i].kinetic.z = kt1*(1 - cos(phi - phi_0)) + kt2*(1 - cos(3*(phi - phi_0)));

    }




      
    
    
    
}


void Capture_force(chain_struct chain[])
{

  double F;
  int i;

  for (i=0; i<beads-1; i++)
      {

	 F = sqrt(chain[i].force.x*chain[i].force.x + chain[i].force.y*chain[i].force.y + chain[i].force.z*chain[i].force.z);

           if(F_MAX < F) 
                     {
                      	  chain[i].force.x = chain[i].force.x*(F_MAX/F);
      			  chain[i].force.y = chain[i].force.y*(F_MAX/F);
     		          chain[i].force.z = chain[i].force.z*(F_MAX/F);
                     }
                                 
       }
}


double dVdr(double d)
{

    double f, r, rto6;

    f = 0.0;
    r = d;

    if(r <= twotoonesixth)
    {
        rto6 = r*r*r*r*r*r;
        rto6 = 1.0/rto6;
        f = -fourEpsilon*(12.0*rto6*rto6- 6.0*rto6)/d;
    }


    return(f);

}

//LJ force

double dVdr_LJ(double d)
{

    double f, r, rto6;

    f = 0.0;
    r = d;

    rto6 = r*r*r*r*r*r;
    rto6 = 1.0/rto6;
    f = -fourEpsilon*(12.0*rto6*rto6- 6.0*rto6)/d;

    return(f);

}

//disulfide bridge, SF LJ force, with re-defined sigma and epsilon

double dVdr_dsb(double d,bridge_struct bridge)
{

  double f,r;

  f = 0.0;
  r = d/bridge.sigma;
  
  if(r <= bridge.rco) {
    f=bridge.eps*dVdr_LJ(r)-bridge.fco;
  }
  
  return(f);

}



double DV_nativeDr(double d, double d_zero)
{

    double f, r;
    double eps, sigsq, cut;

    eps = native_contact_strength;
    sigsq = native_contact_width*native_contact_width;
    cut = native_contact_cutoff;

    f = 0.0;
    r = d - d_zero;

    if(fabs(r) <= cut)
    {
        // exp(- r^2 / ( 2 * 0.25^2 ) )
        f = eps*exp(-0.5*r*r/sigsq)*r/sigsq;
    }


    return(f);

}
