#include <sys/types.h>
#include <sys/stat.h>
#include <sys/stat.h>


void get_beadn(char filename[]);
void system_parameter_setup(int pid, char filename[], chain_struct **chain_ptr, chain_struct **temp_chain_ptr, bridge_struct **bridge_ptr);
void read_conf(char filename[], chain_struct chain[]);
void read_elastic_param(chain_struct chain[]);
void read_bridge(char filename[], bridge_struct bridge[]);
void print_traj(chain_struct chain[], int t,  FILE *trj_vtf, FILE *gyration);
void print_Energy(energy_struct energy, int t,  FILE *potential, FILE *kinetic, FILE *totenergy);
void print_final_conf(int pid, chain_struct chain[],int n);
void print_parameters_on_file(FILE *file, int i);
void initialize_energy_struct(energy_struct *energy);
void allocate_neighbor_list(neighbor_list **neighbors_ptr);
void allocate_chain_struct(chain_struct **chain_ptr);
void read_Native_distance();
void print_native_contact_number(chain_struct chain[], int t, FILE *nativecon);


void allocate_chain_struct(chain_struct **chain_ptr)
{
  chain_struct *chain      = (chain_struct*  ) malloc(beads * sizeof(chain_struct));
  *chain_ptr = chain;
}



void allocate_neighbor_list(neighbor_list **neighbors_ptr)
{
  int i;

  neighbor_list *neighbors  = (neighbor_list* ) malloc(beads * sizeof(neighbor_list));
  for(i = 0; i < beads; i++) neighbors[i].index = i1t(beads);
  *neighbors_ptr = neighbors;

}

void initialize_energy_struct(energy_struct *energy)
{
  energy->fene = 0.0;
  energy->wca = 0.0;
  energy->bending = 0.0;
  energy->bridge = 0.0;
  energy->torsion = 0.0;
  energy->kinetic = 0.0;
}

void get_beadn(char filename[])
{
  int check;
  char stringa[1024];
  FILE *f_params;
  double value;
  if( ( f_params = fopen(filename,"r") ) == NULL )
    {
#ifdef VERBOSE
      fprintf(stderr,"Cannot open input file %s\nAborting.\n", filename);
#endif
      exit(1);
    }
  while((check=fscanf(f_params,"%s %lf", stringa, &value))==2){
    if ( strcmp( stringa, "#Nbeads" ) == 0 ){
      beads = (int)value;
      break;
    }
  }
  return;
}
    

void system_parameter_setup(int pid, char filename[], chain_struct **chain_ptr, chain_struct **temp_chain_ptr, bridge_struct **bridge_ptr)
{
    int i,k,check, l, q;
    double value;
    int N_parameters = 18;
    int parameter_check[N_parameters];
    char *parameter[N_parameters];
    char stringa[1024];

    dobrownian = 0;

    //init niters, nbridges and sigmut

    niters=1;
    nbridges=0;
    sigma_el=2.5; //default value
    sigma_ang=0.1; //default value
    long int inseed=-1;
    
    FILE *f_params;
    if( ( f_params = fopen(filename,"r") ) == NULL )
    {
      #ifdef VERBOSE
      fprintf(stderr,"Cannot open input file %s\nAborting.\n", filename);
      #endif
      exit(1);
    }

    /*
     *----PARAMETERS----*
     */

    
    parameter[0]  = "#read_chain";      // Read the chain from file ( != 0) or build it ( = 0)
    parameter[1]  = "#Nbeads";          // Number of beads (has to be set also for externally input chains)
    parameter[2]  = "#timestep";        // dt
    parameter[3]  = "#Nsteps";          // Number of steps
    parameter[4]  = "#FENE_bond";       // bond length
    parameter[5]  = "#FENE_kappa";      // kappa
    parameter[6]  = "#dump_traj";       // trajectory printout frequency
    parameter[7]  = "#RV";              // radius
    parameter[8]  = "#epsilon";         // Epsilon
    parameter[9]  = "#tau_frict";       // tau
    parameter[10] = "#temp";            // temperature; in units of KbT
  /*  parameter[11] = "#Bend_kappa";      // bending elastic constant
    parameter[12] = "#tetha_0";         // reference value of the bending formation
    parameter[13] = "#Torsion_kappa1";      // torsion elastic constant1
    parameter[14] = "#Torsion_kappa2";      // torsion elastic constant2
    parameter[15] = "#phi_0";         // reference value of the torsion formation*/

    parameter[11] = "#niters";            // Number of Monte Carlo interations in wrapper
    parameter[12] = "#nbridges";            // Number of disulphide bridges
    parameter[13] = "#sigmael";            // Sigma of gaussian elastic coefficient mutation
    parameter[14] = "#sigmaang";            // Sigma of gaussian native angle mutation
    parameter[15] = "#seed";            // Input initial seed
    parameter[16] = "#Neqsteps";            // Equilibration steps
    parameter[17] = "#eqtimestep";            // Equilibration timestep
    
    for( i = 0 ; i < N_parameters ; i++ ) { parameter_check[i]=0;  }
    /************************/
    k=0;
    while((check=fscanf(f_params,"%s %lf", stringa, &value))==2 && k<N_parameters)
    {
      for( i = 0 ; i < N_parameters ; i++ )
      {
        if ( strcmp( stringa, parameter[i] ) == 0 )
        {
          k++;
          switch(i)
          {
            case 0:
              readchain = (int)value;
              parameter_check[i]=1;
              break;
            case 1:
              beads = (int)value;
              parameter_check[i]=1;
              break;
            case 2:
              dt = value;
              parameter_check[i]=1;
              break;
            case 3:
              Nsteps = (int)value;
              parameter_check[i]=1;
              break;
            case 4 :
              bond = value;
              parameter_check[i]=1;
              break;
            case 5 :
              kappa = value;
              parameter_check[i]=1;
              break;
            case 6 :
              printfreq = (int)value;
              parameter_check[i]=1;
              break;
            case 7 :
              RV = value;
              parameter_check[i]=1;
              break;
            case 8:
              fourEpsilon = value;
              fourEpsilon = 4.0 * fourEpsilon;
              parameter_check[i]=1;
              break;
            case 9:
              tau = value;
              parameter_check[i]=1;
              break;
            case 10:
              temperature = value;
              parameter_check[i]=1;
              break;
	    case 11:
              niters = value;
              parameter_check[i]=1;
              break;
	    case 12:
              nbridges = value;
              parameter_check[i]=1;
              break;
      	    case 13:
              sigma_el = value;
              parameter_check[i]=1;
              break;
      	    case 14:
              sigma_ang = value;
              parameter_check[i]=1;
              break;
	    case 15:
	      inseed = value;
	      parameter_check[i]=1;
              break;
	  case 16:
	    Neqsteps = value;
	    parameter_check[i]=1;
              break;
	  case 17:
	    dteq = value;
	    parameter_check[i]=1;
	    break;
	    
	      
          /*  case 11 :
              kappa_bend = value;
              parameter_check[i]=1;
              break;
            case 12 :
              tetha = value;
              parameter_check[i]=1;
              break;
            case 13 :
              kappa_tors1 = value;
              parameter_check[i]=1;
              break;
            case 14 :
              kappa_tors2 = value;
              parameter_check[i]=1;
              break;
            case 15 :
              phi = value;
              parameter_check[i]=1;
              break;*/
          } 
        }
      }
    }
    for( i = 0 ; i < N_parameters-7 ; i++ ) //no check for last 5 parameters
    {
       if(parameter_check[i] == 0 )
       {
         #ifdef VERBOSE
         fprintf(stderr,"%s not set! Abort. \n",parameter[i]);
         #endif
         exit(1);
       }
    }


    rouse_time = 0;
    if(temperature > 0.0)
      {
        rouse_time = (beads*beads*0.97*0.97)/(tau*mypi*mypi*temperature);
      }

#ifdef VERBOSE
    if(world_rank ==0){ //only root process writes this
      printf("\n********************\n");
      printf("**** parameters ****\n");
      printf("********************\n");
      printf("Process ID         = %10d\n", pid);
      printf("Number of beads    = %10d\n", beads);
      printf("Input from file    = %10d\n", readchain);
      printf("Number of steps    = %10.0e\n", (double)Nsteps);
      printf("Traj. print freq.  = %10d\n", printfreq);
      printf("NList verl. rad.   = %10.2lf\n", RV);
      printf("********************\n");
      printf("***** lengths ******\n");
      printf("Bond               = %10.2lf\n", bond);
      printf("Sigma (hard coded) = %10.2lf\n", 1.0);
      printf("NList cutoff       = %10.2lf\n", cutoff);
      printf("********************\n");
      printf("***** energies *****\n");
      printf("Kappa              = %10.2lf\n", kappa);
      /*  printf("Kappa_bend         = %10.2lf\n", kappa_bend);
	  printf("Kappa_tors1        = %10.2lf\n", kappa_tors1);
	  printf("Kappa_tors2        = %10.2lf\n", kappa_tors2);
	  printf("tetha              = %10.2lf\n", tetha);
	  printf("phi                = %10.2lf\n", phi);*/
      printf("Epsilon            = %10.2lf\n", fourEpsilon/4.0);
      printf("Temperature        = %10.2lf\n", temperature);
      printf("********************\n");
      printf("******* times ******\n");
      printf("Rouse time         = %10.2lf\n", rouse_time);
      printf("Timestep           = %10.2e\n", dt);
      printf("Traj duration (MD) = %10.2e\n", Nsteps*dt);
      printf("Fastest rouse time = %10.2e\n", rouse_time/(3*beads*beads));
      printf("Friction tau       = %10.2lf\n", tau);
      printf("Number of MC iterations = %d (meaningful only for optimization)\n", niters);
      printf("Number of ds bridges = %d \n", nbridges);
      printf("Sigma of coefficient gaussian mutations = %10.2f \n", sigma_el);
      printf("Sigma of native angles gaussian mutations = %10.2f \n", sigma_ang);
      printf("%d equilibration steps of length %.10.2e \n", Neqsteps,dteq);
      printf("Sigma of native angles gaussian mutations = %10.2f \n", sigma_ang);
      if(inseed!=-1) printf("Input seed provided = %ld \n", inseed);
    }
#endif
    
    cutoff = twotoonesixth;
    DR2    = (RV - cutoff) * (RV - cutoff);

    chain_struct *chain      = (chain_struct*  ) malloc(beads * sizeof(chain_struct));
    chain_struct *temp_chain = (chain_struct*  ) malloc(beads * sizeof(chain_struct));

    //create and init bridge struct
    bridge_struct *bridge;
    if(nbridges>0){
      bridge = (bridge_struct*  ) malloc(nbridges * sizeof(bridge_struct));
      read_bridge(filename, bridge);
    }

    initialize_chain(temp_chain);
    idum = get_seed(); //-1 ;
    if(inseed!=-1) idum=inseed;
    //srand(idum);
    fprintf(outf,"Initial seed :\t%ld\n\n",idum);
    initialize_chain(chain);
    fprintf(outf,"Seed :\t%ld\n\n",idum); //DBG
    
    
    fclose(f_params);
    //if readchain!=0 the chain configuration in the .kaw file is read.
    if(readchain!=0) read_conf(filename, chain);
    read_elastic_param(chain);

    nonfixed_angle = i1t(beads); //global variable -- lib/global_variables.c

    for(i = 0; i < beads; i++) nonfixed_angle[i] = 0;
    
    N_nonfixed_angle = 0;
    l = 0;

    for(i = 0; i < beads; i++)
    {
      q = chain[i].bendparam.change_angle;
      
      if(q==1)
        {
          nonfixed_angle[l] = i;
          l++;
        }
    }

    N_nonfixed_angle = l;



    copy_chain_and_parameters(chain, temp_chain);
    frict = dt/tau;
    brownian = sqrt(2.0*temperature*frict);
    if(temperature > 0.000000000) dobrownian = 1;


#ifdef VERBOSE
    if(world_rank ==0){ //only root process writes this
      printf("Do thermostatted dynamics: %d\n", dobrownian);

#ifdef EXCL_VOL
      printf("EXCL_VOL           ON\n");
#else
      printf("EXCL_VOL           OFF\n");
#endif
#ifdef SELF_INT_CHAIN
      printf("SELF_INT_CHAIN     ON\n");
#else
      printf("SELF_INT_CHAIN     OFF\n");
#endif
#ifdef BENDING
      printf("BENDING            ON\n");
#else
      printf("BENDING            OFF\n");
#endif
#ifdef TORSION
      printf("TORSION            ON\n");
#else
      printf("TORSION            OFF\n");
#endif
#ifdef FIRST_ORDER
      printf("FIRST_ORDER        ON\n");
#else
      printf("SECOND_ORDER       ON\n");
#endif
#ifdef PRINT_GYR
      printf("PRINT_GYR          ON\n");
#else
      printf("PRINT_GYR          OFF\n");
#endif
   
      printf("*******************************\n");
    }
#endif
      
    //Create output directories
    //GENE : for wrapper dirlabel is "", for PMD is "run_%d_%d", for genetic is "cycle_%d/run_%d"
    
    char strdir[128];
    struct stat st = {0};
    
    strdir[0]=0;
    sprintf(strdir,"%s/traj/",dirname);
    if (stat(strdir, &st) == -1) {
      mkdir(strdir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }
    strdir[0]=0;
    sprintf(strdir,"%s/output/",dirname);
    if (stat(strdir, &st) == -1) {
      mkdir(strdir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }
    strdir[0]=0;
    sprintf(strdir,"%s/energy/",dirname);
    if (stat(strdir, &st) == -1) {
      mkdir(strdir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }
    strdir[0]=0;
    sprintf(strdir,"%s/native_contacts/",dirname);
    if (stat(strdir, &st) == -1) {
      mkdir(strdir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }
#ifdef PRINT_GYR
    strdir[0]=0;
    sprintf(strdir,"%s/gyration/",dirname);
    if (stat(strdir, &st) == -1) {
      mkdir(strdir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }
#endif    
    
    *chain_ptr = chain;
    *temp_chain_ptr = temp_chain;
    *bridge_ptr = bridge;
    
}


void print_traj(chain_struct chain[], int t, FILE *trj_vtf, FILE *gyration)
{

    int i;

    //Create output directories

    //GENE : for wrapper dirlabel is "", for PMD is "run_%d_%d", for genetic is "cycle_%d/run_%d"
    //char name[128];
    //sprintf(name, "%s/traj/traj_%d.vtf",dirname, pid);
    
    //FILE *trj_vtf;
    //trj_vtf = fopen(name, "a");

    if(t==0)
    {
      print_parameters_on_file(trj_vtf, t);
      fprintf(trj_vtf, "#seed %ld\n",idum); 
      fprintf(trj_vtf, "#Nbeads %d\natom 0:%d radius %8.3lf\n", beads, beads-1, 0.5);
      for(i = 0; i < beads; i++)
      {
        fprintf(trj_vtf, "atom %d\n", i);
      }
      for(i = 0; i < beads-1; i++)
      {
        fprintf(trj_vtf, "bond %d:%d\n", i+1, i);
      }
    }

    fprintf(trj_vtf, "\ntimestep ordered # t (steps) = %d\n", t);
    
    for(i = 0; i < beads; i++)
      fprintf(trj_vtf, "%13.5lf%13.5lf%13.5lf\n",
	      chain[i].pos.x, chain[i].pos.y, chain[i].pos.z);
    //fclose(trj_vtf);
    
#ifdef PRINT_GYR
    
    double sqxdiffgyr, sqydiffgyr, sqzdiffgyr, Gyration;
    int j;
    
    Gyration = 0;  
    for (i = 0; i < beads; i++)
      {
	for (j = 0; j < beads; j++)
	  {
	    
	    sqxdiffgyr = (chain[i].pos.x - chain[j].pos.x)*(chain[i].pos.x - chain[j].pos.x);
	    sqydiffgyr = (chain[i].pos.y - chain[j].pos.y)*(chain[i].pos.y - chain[j].pos.y);
	    sqzdiffgyr = (chain[i].pos.z - chain[j].pos.z)*(chain[i].pos.z - chain[j].pos.z);
	    Gyration += (0.5/(beads*beads))*(sqxdiffgyr + sqydiffgyr + sqzdiffgyr);
          }
      }
    
    //GENE : add dirlabel
    
    
    //sprintf(name, "%s/gyration/gyration_%d.vtf",dirname,pid);
    //FILE *gyration;
    //gyration = fopen(name, "a");
    fprintf(gyration, " %d  %16.6lf\n", t, Gyration);
    //fclose(gyration); 

#endif

}

void read_bridge(char filename[], bridge_struct bridge[])
{

  int i,j,l=0;
  double eps, sig, rco;
  char stringa[1024];
  FILE * f_in;
  f_in = fopen ( filename, "r" );
  i=0;
  do{
    fscanf ( f_in, "%s", stringa );
    l = strcmp ( stringa, "#Bridges" );
    i++;
    if (i>1000)
      {
#ifdef VERBOSE
        fprintf(stderr,"Failed to find Bridges string in %s\n",filename);
        fprintf(stderr,"Aborting.\nCiao.\n");
#endif
        exit(1);
      }
  }while( l !=0 );
  
  i=0;

  for(l=0; l<nbridges; l++){
    fscanf(f_in, "%d %d %lf %lf %lf\n", &i, &j, &eps, &sig, &rco);
    bridge[l].bead_i = i;
    bridge[l].bead_j = j;
    bridge[l].eps = eps; //provided in code units
    bridge[l].sigma = sig; //provided in code units
    bridge[l].rco = rco; //input provided in own sigma units
    //init remaining parameters to 0
    //printf("%d %d %lf %lf %lf\n", bridge[l].bead_i, bridge[l].bead_j, bridge[l].eps, bridge[l].sigma, bridge[l].rco); //test
    bridge[l].Vco = 0.0;
    bridge[l].fco = 0.0;
  }
  fclose (f_in);

}

  
  
  



void read_conf(char filename[], chain_struct chain[])
{

    int i,l=0;
    double x, y, z, vx, vy, vz;
    char stringa[1024];
    FILE * f_in;
    f_in = fopen ( filename, "r" );
    i=0;
    do{
      fscanf ( f_in, "%s", stringa );
      l = strcmp ( stringa, "#Coordinates" );
      i++;
      if (i>1000)
      {
        #ifdef VERBOSE
        fprintf(stderr,"Failed to find Coordinates string in %s\n",filename);
        fprintf(stderr,"Aborting.\nCiao.\n");
        #endif
        exit(1);
      }
    }while( l !=0 );

    i = 0;
    //printf("reading coordinates\n");
    while(!feof(f_in))
    {
        fscanf(f_in, "%lf %lf %lf %lf %lf %lf\n", &x, &y, &z, &vx, &vy, &vz);
	//printf("%lf %lf %lf %lf %lf %lf\n", x, y, z, vx, vy, vz);
        chain[i].pos.x = x;
        chain[i].pos.y = y;
        chain[i].pos.z = z;
        chain[i].vel.x = vx;
        chain[i].vel.y = vy;
        chain[i].vel.z = vz;

       if(i == beads)
	 {
          #ifdef VERBOSE
          fprintf(stderr, "Number of found coordinates larger than expected.\nAborting.\n");
          #endif
          exit(1);
        }
        i++;
    }
    //printf("read coordinates?\n");
    if(i < beads)
    {
      #ifdef VERBOSE
      fprintf(stderr, "Number of found coordinates smaller than expected, namely %d instead of %d.\nAborting.\n", i, beads);
      #endif
      exit(1);
    }

    
    fclose (f_in);

}



void print_final_conf(int pid, chain_struct chain[],int n)
{

    int i;
    char name[128], name2[128];

    //GENE : add dirlabel

    sprintf(name, "%s/output/output_%d.dat",dirname,pid);
    sprintf(name2, "%s/output/output_%d.vtf",dirname,pid);

 
    
    FILE *f_final;
    if( ( f_final = fopen(name,"w") ) == NULL )
    {
      #ifdef VERBOSE
      fprintf(stderr,"Cannot open input file %s\nAborting.\n", name);
      #endif
      exit(1);
    }

    FILE *f_final_vtf;
    if( ( f_final_vtf = fopen(name2,"w") ) == NULL )
    {
      #ifdef VERBOSE
      fprintf(stderr,"Cannot open input file %s\nAborting.\n", name2);
      #endif
      exit(1);
    }

    print_parameters_on_file(f_final, n);
    fprintf(f_final, "#Coordinates\n");
    for(i = 0; i < beads; i++)
      fprintf(f_final, "%13.5lf%13.5lf%13.5lf %13.5lf%13.5lf%13.5lf\n",
      chain[i].pos.x, chain[i].pos.y, chain[i].pos.z,
      chain[i].vel.x, chain[i].vel.y, chain[i].vel.z);

    
    for(i = 0; i < beads; i++) fprintf(f_final_vtf, "atom %d\n", i);
    for(i = 0; i < beads-1; i++) fprintf(f_final_vtf, "bond %d:%d\n", i+1, i);

    fprintf(f_final_vtf, "\nOutput conf\n");

    for(i = 0; i < beads; i++)
      fprintf(f_final_vtf, "%13.5lf%13.5lf%13.5lf\n",
      chain[i].pos.x, chain[i].pos.y, chain[i].pos.z);




    fclose(f_final);
    fclose(f_final_vtf);


}


void print_Energy(energy_struct energy, int t, FILE *potential, FILE *kinetic, FILE *totenergy)
{
    double chain_potential, chain_kinetic, chain_totenergy;
    //char name[128];

    chain_potential = 0;
    chain_kinetic = 0;
    chain_totenergy = 0;

    chain_kinetic = energy.kinetic;
    //fprintf(fdbg,"%.8f\n",energy.bending); //DBG
    //fflush(fdbg); //DBG
	
    chain_potential = energy.fene + energy.wca + energy.bending + energy.torsion + energy.bridge;
    chain_totenergy = chain_kinetic + chain_potential;

    //GENE : add dirlabel
    
    //sprintf(name, "%s/energy/potential_%d.dat",dirname,pid);


    //FILE *potential;
    //potential = fopen(name, "a");
    fprintf(potential, " %8d %8.3f %8.3lf %8.3lf %8.3lf %8.3lf %8.3lf\n", t, chain_potential, energy.fene, energy.wca, energy.bending, energy.torsion,energy.bridge);
   
    //fclose(potential);

    //GENE : add dirlabel
    
    //sprintf(name, "%s/energy/kinetic_%d.dat",dirname,pid);
    
    //FILE *kinetic;
    //kinetic = fopen(name, "a");
    fprintf(kinetic, " %8d %8.3lf\n", t, chain_kinetic);
   
    //fclose(kinetic);

    //GENE : add dirlabel
    
    //sprintf(name, "%s/energy/totenergy_%d.dat",dirname,pid);
    //FILE *totenergy;
    //totenergy = fopen(name, "a");
    fprintf(totenergy, " %8d %8.3f\n", t, chain_totenergy);
   
    //fclose(totenergy);

}


void read_elastic_param(chain_struct chain[])
{

    int i, l, P, change;
    double kappa_bend, tetha, kappa_tors1, kappa_tors2, phi;
    char stringa[1024],stringa2[1024];
    FILE * f_in;
    
    //GENE : add dirlabel
    char name[128];
    sprintf(name, "%s/forcefield.dat",ffdirname);
    f_in = fopen ( name, "r" );
    i = 0;
    l = 0;

   do{
     fgets(stringa,sizeof(stringa),f_in);
     sscanf (stringa, "%s", stringa2);
     l = strcmp ( stringa2, "#atom");
      i++;
      if (i>1000)
      {
        #ifdef VERBOSE
        fprintf(stderr,"Failed to find Coordinates string in %s\n", "forcefield.dat");
        fprintf(stderr,"Aborting.\nCiao.\n");
        #endif
        exit(1);
      }
    }while( l !=0 );
    i=1;

    change = 0;
  
    fgets(stringa,sizeof(stringa),f_in);
    while(!feof(f_in))
    {
      int match = sscanf(stringa, "%d %lf %lf %lf %lf %lf %d\n", &P, &kappa_bend, &tetha, &kappa_tors1, &kappa_tors2, &phi, &change);
      chain[i].bendparam.kb = kappa_bend;
      chain[i].bendparam.tetha = tetha;

      if(match == 7) chain[i].bendparam.change_angle = change;
      
      chain[i].torsionparam.kt1 = kappa_tors1;
      chain[i].torsionparam.kt2 = kappa_tors2;
      chain[i].torsionparam.phi = phi;
      //printf("%d %lf %lf %lf %lf %lf %d %d\n", i, chain[i].bendparam.kb,  chain[i].bendparam.tetha,chain[i].torsionparam.kt1, chain[i].torsionparam.kt2, chain[i].torsionparam.phi, chain[i].bendparam.change_angle, match);
      i++;
      fgets(stringa,sizeof(stringa),f_in); 
      }
    
    fclose (f_in);

}



void read_Native_distance()
{
    int i, II, JJ;
    double distance;

    
    DIST = d2t(beads, beads);
    
    FILE * f_in;
    f_in = fopen ( "Distance.dat", "r" );

    for(II = 0; II < beads; II++)
    {
      for(JJ = 0; JJ < beads; JJ++) DIST[II][JJ] = 0.0;
    }

    i=0;

    while(!feof(f_in))
    {
        fscanf(f_in, "%d %d %lf\n", &II, &JJ, &distance);
        //printf("%d %d %lf\n", II, JJ, distance);

        DIST[II][JJ] = distance;
        DIST[JJ][II] = distance;
        i++;
       
      }
    
    fclose (f_in);

}



void print_parameters_on_file(FILE *file, int i)
{
   
    fprintf(file, "#read_chain            %8d\n", readchain);
    fprintf(file, "#Nbeads                %8d\n", beads);
    fprintf(file, "#timestep              %8.6lf\n", dt);
    fprintf(file, "#Nsteps                %8d\n", Nsteps);
    fprintf(file, "#FENE_bond             %8.3lf\n", bond);
    fprintf(file, "#FENE_kappa            %8.3lf\n", kappa);
    fprintf(file, "#dump_traj             %8d\n", printfreq);
    fprintf(file, "#RV                    %8.3lf\n", RV);
    fprintf(file, "#epsilon               %8.3lf\n", fourEpsilon*0.25);
    fprintf(file, "#tau_frict             %8.3lf\n", tau);
    fprintf(file, "#temp                  %8.3lf\n", temperature);
    fprintf(file, "#niters                %8d\n", niters);
    fprintf(file, "#bridges               %8d\n", nbridges);
    fprintf(file, "#sigmael               %8.3lf\n", sigma_el);
    fprintf(file, "#sigmaang              %8.3lf\n", sigma_ang);

 /*   fprintf(file, "#Bend_kappa            %8.3lf\n", kappa_bend);
    fprintf(file, "#tetha_0               %8.3lf\n", tetha);
    fprintf(file, "#Torsion_kappa1        %8.3lf\n", kappa_tors1);
    fprintf(file, "#Torsion_kappa2        %8.3lf\n", kappa_tors2);
    fprintf(file, "#phi_0                 %8.3lf\n", phi);*/
}






void print_native_contact_number(chain_struct chain[], int t, FILE *nativecon)
{

    int i, j, q, qtot;
    char name[128];
    char strdir[128];
    double r, d, dsq, xsq, ysq, zsq, xdiff, ydiff, zdiff, d_zero;


    //FILE *fp;
    //struct stat st = {0};

    //GENE : add dirlabel
    
    //sprintf(strdir,"%s/native_contacts/",dirname);  //string is "run_pid_me/native_contacts/"
    
    //if (stat(strdir, &st) == -1) {
    //mkdir(strdir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    //}

    //GENE : add dirlabel
    
    //sprintf(name, "%s/native_contacts/native_contacts_%d.dat",dirname,pid);   

    //fp = fopen(name, "a");


    q = 0;
    qtot = 0;

    for (i = 0; i < beads-1; i++)
    {
      for(j = i+1; j < beads; j++)
      {
        xdiff = chain[i].pos.x - chain[j].pos.x;
        ydiff = chain[i].pos.y - chain[j].pos.y;
        zdiff = chain[i].pos.z - chain[j].pos.z;

        xsq = xdiff*xdiff;
        ysq = ydiff*ydiff;
        zsq = zdiff*zdiff;
        
        dsq = xsq + ysq + zsq;
        
        d = sqrt(dsq);
        
        d_zero = DIST[i][j];
        
        r = d - d_zero;
        
        if(d_zero>0.0)
          {
            if(fabs(r) <= 1.0) q++;
            qtot++;
          }
      
      }
    }

    fprintf(nativecon, "%d %d %d\n", t, q, qtot);


    //fclose(fp);

}














