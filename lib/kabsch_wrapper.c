/**************************************/
/* Essential routines to compute RMSD */
/* between two structures.            */
/* Copyright C. Mecheletti, SISSA     */
/**************************************/


double  best_rmsd(double str1[][3], double str2[][3],int length);
double  optimal_alignment(double str1[][3], double str2[][3], double str3[][3],int length, double u[][3], double *cm1, double *cm2);
void myjacobi (double a[][3], int n, double *d, double v[][3], int *nrot);
void vecprod_d(double *a, double *b, double *c); /* c = axb (in 3d). */
double scal_d(double *a, double *b, int dim);    /* prod. scal a.b */
double coseno(double *vec1, double *vec2, int dim );
double norm_d(double *a, int dim);               /* norma vettore */
void normalize_d(double *a, int dim);            /* normalizza vettore */
double dist_d(double *a, double *b, int dim);    /* norma a-b */
double det (double a1, double a2, double a3, double b1, double b2, double b3, double c1, double c2, double c3);
void vec_sum_d (double *a, double *b, double *c, double d, int dim);
void print_vec_d (double *a, int dim);
void zero_vec_d (double *a, int dim);
#define dist3d(a,b) (sqrt((a[0]-b[0])*(a[0]-b[0])+(a[1]-b[1])*(a[1]-b[1])+(a[2]-b[2])*(a[2]-b[2])))





double  best_rmsd(double str1[][3], double str2[][3],int length)
{
/* ritorna il migliore rmsd (Kabsch) tra str1 e str2. Le coordinate di str1 e str2 vengono preservate  */

        double str3[2000][3], cm1[3],cm2[3], rmsd;
        double u[3][3];

        if (length > 2000) {
                printf("aumenta la size vettore in best_rmsd\n");
                exit(1);
        }       
        rmsd=optimal_alignment(str1,str2,str3,length,u,cm1,cm2);
        return(rmsd);
}




double  optimal_alignment(double str1[][3], double str2[][3], double str3[][3],int length, double u[][3], double *cm1, double *cm2)   {

/* ritorna il migliore rmsd tra str1 e str2 e mette in str3 la
prima struttura allineata sulla seconda, in u la matrice di rotazione */

  void myjacobi (double a[][3], int n, double *d, double v[][3], int *nrot);

  int i, j, k, sign[3], order[3], nrot;
  double e, e0;
  double r[3][3], rt[3][3], temp, **x, **y;
  double a[3][3], eval[3], evec[3][3];
  double eigenvalues[3], eigenvectors[3][3], b[3][3];


  x = d2t(length,3);
  y = d2t(length,3);

  zero_vec_d (cm1, 3);
  zero_vec_d (cm2, 3);

  for (i = 0; i < length; i++)
    {
      for (j = 0; j < 3; j++)
	{
	  cm1[j] += str1[i][j] / length;
	  cm2[j] += str2[i][j] / length;

	}
    }
/*    printf("cm1 %lf %lf %lf\n",cm1[0],cm1[1],cm1[2]);
    printf("cm2 %lf %lf %lf\n",cm2[0],cm2[1],cm2[2]);
*/
  for (i = 0; i < length; i++)
    {
      for (j = 0; j < 3; j++)
	{
	  x[i][j] = str1[i][j] - cm1[j];
	  y[i][j] = str2[i][j] - cm2[j];
	}
    }
    
  /* Mettiamo un pelo di noise per evitare allineamenti perfetti */
    
  e0 = 0.0;
  for (i = 0; i < length; i++)
    {
      e0 += 0.5 * norm_d (x[i], 3) * norm_d (x[i], 3);
      e0 += 0.5 * norm_d (y[i], 3) * norm_d (y[i], 3);
    }

  for (i = 0; i < 3; i++)
    {
      for (j = 0; j < 3; j++)
	{
	  r[i][j] = 0.0;
	  for (k = 0; k < length; k++)
	    {
	      r[i][j] += y[k][i] * x[k][j];
	    }
	  rt[j][i] = r[i][j];
	}
    }



  for (i = 0; i < 3; i++)
    {
      for (j = 0; j < 3; j++)
	{
	  a[i][j] = 0;
	  for (k = 0; k < 3; k++)
	    {
	      a[i][j] += rt[i][k] * r[k][j];
	    }
	}
    }


  myjacobi (a, 3, eval, evec, &nrot);
/* aggiungiamo delle piccole quantita' per rimuovere degenerazioni */
  eval[0]+=0.0000000000000001;
  eval[1]+=0.00000000000000001;
  eval[2]+=0.00000000000000002;
        
  if ((eval[0] < eval[1]) && (eval[0] < eval[2]))
    {
      order[0] = 1;
      order[1] = 2;
      order[2] = 0;
    }

  if ((eval[1] < eval[0]) && (eval[1] < eval[2]))
    {
      order[0] = 0;
      order[1] = 2;
      order[2] = 1;
    }

  if ((eval[2] < eval[0]) && (eval[2] < eval[1]))
    {
      order[0] = 0;
      order[1] = 1;
      order[2] = 2;
    }


  for (i = 0; i < 3; i++)
    {
      eigenvalues[i] = eval[order[i]];
      for (j = 0; j < 3; j++)
	{
	  eigenvectors[i][j] = evec[j][order[i]];
	}
    }


  normalize_d (eigenvectors[0], 3);
  normalize_d (eigenvectors[1], 3);
  vecprod_d (eigenvectors[0], eigenvectors[1], eigenvectors[2]);
  normalize_d (eigenvectors[2], 3);

  for (i = 0; i < 2; i++)
    {
      for (j = 0; j < 3; j++)
	{
	  b[i][j] = 0;
	  for (k = 0; k < 3; k++)
	    {
	      b[i][j] += r[j][k] * eigenvectors[i][k];
	    }
	}
      normalize_d (b[i], 3);
    }


  vecprod_d (b[0], b[1], b[2]);
  normalize_d (b[2], 3);


  temp = 0.0;
  for (i = 0; i < 3; i++)
    {
      for (j = 0; j < 3; j++)
	{
	  temp += b[2][i] * r[i][j] * eigenvectors[2][j];
	}
    }
  sign[2] = +1;
  if (temp < 0)
  sign[2] = -1;
  sign[0]=sign[1]=1;
        
  e = e0 - sqrt (eigenvalues[0]) - sqrt (eigenvalues[1]) - sign[2] * sqrt (eigenvalues[2]);



/********************/
  e =2.0 * e / length;
  if (e <0.0) {	
    if (fabs(e) < 1.0e-3) { 
      //if(VERBOSE==1)
if(2==1)
printf("Warning. In Kabsch alignment found slightly negative value of e (%e). Roundoff error? I will set it equal to zero.\n",e); e=0.0;}/* occasionally, when dealing with two practically identical configurations
				  the value of e may be slightly negative due to the small offsets and roundoff errors. 
				  In this case we set it equal to zero. */
  else {fprintf(stderr,"ERROR. In Kabsch alignment found negative value of e: (%lf)\n",e); exit(1);} 
  }
  e = sqrt (e);
/********************/


  for(i=0; i < 3; i++){
    for(j=0; j < 3; j++){
      u[i][j]=0.0;
        for(k=0; k < 3; k++){
          u[i][j]+= b[k][i]*eigenvectors[k][j];

        }
/*       printf("%10.4lf ",u[i][j]); */

    }
/*        printf("\n"); */
  }

/* allinea la 1 sulla 2 - compreso shift del centro di massa */
  for (i = 0; i < length; i++){
      for (j = 0; j < 3; j++){
        str3[i][j]=cm2[j]; 
        for (k = 0; k < 3; k++){
           str3[i][j]+=u[j][k]*x[i][k];
        }
      }
/*        printf("%8.3lf %8.3lf %8.3lf\n",str3[i][0],str3[i][1],str3[i][2]);
        printf("AAAA %8.3lf %8.3lf %8.3lf\n",x[i][0],x[i][1],x[i][2]);
*/
  }
	
  free_d2t(x);
  free_d2t(y);      
  return (e);

}









void myjacobi (double a[][3], int n, double *d, double v[][3], int *nrot)
{
  
/* modificata 24/1/2007 per eliminare controlli di uguaglianza tra floats */

  int j, iq, ip, i;
  double tresh, theta, tau, t, sm, s, h, g, c;
  double b[3], z[3];
  
#define ROTATE(a,i,j,k,l) g=a[i][j];h=a[k][l];a[i][j]=g-s*(h+g*tau);a[k][l]=h+s*(g-h*tau);
  
  for (ip = 0; ip <= (n - 1); ip++)
    {
      for (iq = 0; iq <= (n - 1); iq++)
	v[ip][iq] = 0.0;
      v[ip][ip] = 1.0;
    }
  for (ip = 0; ip <= (n - 1); ip++)
    {
      b[ip] = d[ip] = a[ip][ip];
      z[ip] = 0.0;
    }
  *nrot = 0;
  for (i = 1; i <= 500; i++)
    {
      sm = 0.0;
      for (ip = 0; ip <= n - 2; ip++)
	{
	  for (iq = ip + 1; iq <= (n - 1); iq++)
	    sm += fabs (a[ip][iq]);
	}
      if (sm == 0.0)
	{
	  return;
	}
      if (i < 4)
	tresh = 0.2 * sm / (n * n);
      else
	tresh = 0.0;
      for (ip = 0; ip <= n - 2; ip++)
	{
	  for (iq = ip + 1; iq <= (n - 1); iq++)
	    {
	      g = 100.0 * fabs (a[ip][iq]);
	      if (i > 4 && (fabs ((fabs (d[ip]) + g) - fabs (d[ip])) < 1.0e-6)
		  && (fabs( (fabs(d[iq]) + g)- fabs (d[iq])) < 1.0e-6))
		a[ip][iq] = 0.0;
	      else if (fabs (a[ip][iq]) > tresh)
		{
		  h = d[iq] - d[ip];
		  if (  fabs((fabs (h) + g) - fabs (h)) < 1.0e-6)
		    t = (a[ip][iq]) / h;
		  else
		    {
		      theta = 0.5 * h / (a[ip][iq]);
		      t = 1.0 / (fabs (theta) + sqrt (1.0 + theta * theta));
		      if (theta < 0.0)
			t = -t;
		    }
		  c = 1.0 / sqrt (1 + t * t);
		  s = t * c;
		  tau = s / (1.0 + c);
		  h = t * a[ip][iq];
		  z[ip] -= h;
		  z[iq] += h;
		  d[ip] -= h;
		  d[iq] += h;
		  a[ip][iq] = 0.0;
		  for (j = 0; j <= ip - 1; j++)
		    {
		      ROTATE (a, j, ip, j, iq)
		    }
		  for (j = ip + 1; j <= iq - 1; j++)
		    {
		      ROTATE (a, ip, j, j, iq)
		    }
		  for (j = iq + 1; j <= (n - 1); j++)
		    {
		      ROTATE (a, ip, j, iq, j)
		    }
		  for (j = 0; j <= (n - 1); j++)
		    {
		      ROTATE (v, j, ip, j, iq)
		    }
		  ++(*nrot);
		}
	    }
	}
      for (ip = 0; ip <= (n - 1); ip++)
	{
	  b[ip] += z[ip];
	  d[ip] = b[ip];
	  z[ip] = 0.0;
	}
    }
  printf ("Too many iterations in routine JACOBI %lf",sm);
  /*  exit (1); */
#undef ROTATE
}





/*******************************/
void vecprod_d (double *a, double *b, double *c)
{

  c[0] = a[1] * b[2] - a[2] * b[1];
  c[1] = a[2] * b[0] - a[0] * b[2];
  c[2] = a[0] * b[1] - a[1] * b[0];
}
/*******************************/

double scal_d (double *a, double *b, int dim)
{

  int i;
  double temp;

  temp = 0.0;
  for (i = 0; i < dim; i++)
    {
      temp += a[i] * b[i];
    }
  return (temp);
}
/*******************************/


double coseno(double *vec1, double *vec2, int dim ){

  double temp;

  temp= scal_d(vec1, vec2,dim)/(norm_d(vec1, dim) * norm_d(vec2,dim)); 

  return(temp);

}
/*******************************/

double norm_d (double *a, int dim)
{

  return (sqrt (scal_d (a, a, dim)));
}
/*******************************/
void normalize_d (double *a, int dim)
{
  int i;
  double temp;

  temp = norm_d (a, dim);
  for (i = 0; i < dim; i++)
    {
      a[i] = a[i] / temp;
    }
}



/*******************************/

double dist_d (double *a, double *b, int dim)
{

  int i;
  double temp;

  temp = 0.0;
  for (i = 0; i < dim; i++)
    {
      temp += (a[i] - b[i]) * (a[i] - b[i]);
    }

  temp = sqrt (temp);
  return (temp);
}

/*******************************/

double det (double a1, double a2, double a3,
     double b1, double b2, double b3,
     double c1, double c2, double c3)
{

  double temp;

  temp = a1 * (b2 * c3 - b3 * c2);
  temp += a2 * (b3 * c1 - b1 * c3);
  temp += a3 * (b1 * c2 - b2 * c1);
  return (temp);
}

/*******************************/

void vec_sum_d (double *a, double *b, double *c, double d, int dim)
{


  int i;
  for (i = 0; i < dim; i++)
    {
      c[i] = a[i] + d * b[i];
    }
}

/*******************************/

void print_vec_d (double *a, int dim)
{

  int i;

  for (i = 0; i < dim; i++)
    {
      printf ("%4d %10.5lf\n", i, a[i]);
    }
}

/*******************************/

void zero_vec_d (double *a, int dim)
{

  int i;
  for (i = 0; i < dim; i++)
    {
      a[i] = 0;
    }
}


















