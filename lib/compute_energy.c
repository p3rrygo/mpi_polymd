void Energy(chain_struct chain[], energy_struct *energy, bridge_struct bridge[]);
double fene_bond_energy(chain_struct chain[]);
double bending_energy(chain_struct chain[]);
double bending_energy_diagno(chain_struct chain[], chain_struct chain_en[], double alpha);
double wca_excluded_volume_energy(chain_struct chain[]);
double torsion_energy(chain_struct chain[]);
double torsion_energy_diagno(chain_struct chain[], chain_struct chain_en[], double alpha);
double kinetic_energy(chain_struct chain[]);
double V_native(double d, double d_zero);
double bridge_energy(chain_struct chain[], bridge_struct bridge[]);
double V_LJ(double d);
double V_dsb(double d, bridge_struct bridge);


void Energy(chain_struct chain[], energy_struct *energy, bridge_struct bridge[])
{


    energy->fene = fene_bond_energy(chain);

    #ifdef EXCL_VOL
    energy->wca = wca_excluded_volume_energy(chain);
    #endif

    #ifdef BENDING
    energy->bending = bending_energy(chain);
    #endif

    #ifdef TORSION
    energy->torsion = torsion_energy(chain);
    #endif

    if(nbridges>0) energy->bridge = bridge_energy(chain,bridge);
          
    energy->kinetic = kinetic_energy(chain);

}










double fene_bond_energy(chain_struct chain[])
{

	int i;
	double dsq, xsq, ysq, zsq, xdiff, ydiff, zdiff, erg;

	erg = 0.0;

	for (i = 0; i < beads-1; i++)
    {

        xdiff = chain[i].pos.x - chain[i+1].pos.x;
        ydiff = chain[i].pos.y - chain[i+1].pos.y;
        zdiff = chain[i].pos.z - chain[i+1].pos.z;

        xsq = xdiff*xdiff;
        ysq = ydiff*ydiff;
        zsq = zdiff*zdiff;

        dsq = (xsq + ysq + zsq)/dzerosq;

        if(dsq > fene_capping*fene_capping)
        {
            erg += - 0.5*kappa*dzerosq*log(1-fene_capping*fene_capping) + (sqrt(dsq) - fene_capping)*dzerosq*kappa*fene_capping/(1.0 - fene_capping*fene_capping);
        }
        else
        {
            erg += - 0.5*kappa*dzerosq*log(1-dsq);
        }

    }

    return  erg;

}



double wca_excluded_volume_energy(chain_struct chain[])
{

	int i, j;
	double d, dsq, xsq, ysq, zsq, xdiff, ydiff, zdiff, dto6, erg, d_zero;

	erg = 0.0;


	for (i = 0; i < beads-1; i++)
    {
        #ifdef SELF_INT_CHAIN
    	for(j = i+1; j < beads; j++)
        #else
        j = i+1;
        #endif
        {

            xdiff = chain[i].pos.x - chain[j].pos.x;
            ydiff = chain[i].pos.y - chain[j].pos.y;
            zdiff = chain[i].pos.z - chain[j].pos.z;

            xsq = xdiff*xdiff;
            ysq = ydiff*ydiff;
            zsq = zdiff*zdiff;
            dsq = xsq + ysq + zsq;
            d = sqrt(dsq);
            
          	if(d <= twotoonesixth)
           	{
           		dto6 = d*d*d*d*d*d;
           		dto6 = 1.0/dto6;
           		erg += fourEpsilon*(dto6*dto6 - dto6 + 0.25);
           	}

            // adding native (attractive) non-bonded interactions
            d_zero = DIST[i][j];
            if(d_zero>0.0)
            {
                erg += V_native(d, d_zero);
                //printf("contact %d %d\n", i, j);
            }



         }

     }

     return erg;

}

double bridge_energy(chain_struct chain[], bridge_struct bridge[])
{

  int i, j,n;
  double d, dsq, xsq, ysq, zsq, xdiff, ydiff, zdiff, dto6, erg, d_zero;
  
  erg = 0.0;

  for (n = 0; n < nbridges; n++){
    i=bridge[n].bead_i;
    j=bridge[n].bead_j;
    xdiff = chain[i].pos.x - chain[j].pos.x;
    ydiff = chain[i].pos.y - chain[j].pos.y;
    zdiff = chain[i].pos.z - chain[j].pos.z;
    xsq = xdiff*xdiff;
    ysq = ydiff*ydiff;
    zsq = zdiff*zdiff;
    dsq = xsq + ysq + zsq;
    d = sqrt(dsq);
    
    erg += V_dsb(d,bridge[n]);
    //fprintf(fdbg,"%d\t%d\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\n",i,j,chain[i].pos.x,chain[j].pos.x,d,V_dsb(d,bridge[n]),V_LJ(d)); //DBG
  }

  return erg;

}

double bending_energy(chain_struct chain[])
{

	int i;
	double xsq1, ysq1, zsq1, xsq2, ysq2, zsq2, xdiff1, ydiff1, zdiff1, xdiff2, ydiff2, zdiff2;
	double d1, d2, M, U, theta, kb, erg;
	erg = 0;
	for (i = 1; i < beads-1; i++)
    {

    	xdiff1 = chain[i].pos.x - chain[i-1].pos.x;
        ydiff1 = chain[i].pos.y - chain[i-1].pos.y;
        zdiff1 = chain[i].pos.z - chain[i-1].pos.z;
	//fprintf(fdbg,"%.8f\t%.8f\t%.8f\t",xdiff1,ydiff1,zdiff1); //DBG
        xdiff2 = chain[i+1].pos.x - chain[i].pos.x;
        ydiff2 = chain[i+1].pos.y - chain[i].pos.y;
        zdiff2 = chain[i+1].pos.z - chain[i].pos.z;
	//fprintf(fdbg,"%.8f\t%.8f\t%.8f\t",xdiff2,ydiff2,zdiff2); //DBG
        xsq1 = xdiff1*xdiff1;
        ysq1 = ydiff1*ydiff1;
        zsq1 = zdiff1*zdiff1;
	
        xsq2 = xdiff2*xdiff2;
        ysq2 = ydiff2*ydiff2;
        zsq2 = zdiff2*zdiff2;
      
        d1 = sqrt(xsq1 + ysq1 + zsq1);
        d2 = sqrt(xsq2 + ysq2 + zsq2);
	//fprintf(fdbg,"%.8f\t%.8f\t",d1,d2); //DBG

        M = - (xdiff1*xdiff2 + ydiff1*ydiff2 + zdiff1*zdiff2);
        
        U = M/(d1*d2);
	//fprintf(fdbg,"\t%.8f\t",U); //DBG
        kb = chain[i].bendparam.kb;
        theta = chain[i].bendparam.tetha;
	//fprintf(fdbg,"%.8f\t%.8f\t",theta,kb); //DBG
        erg += kb*(acos(U) - theta)*(acos(U) - theta);
        //fprintf(fdbg,"%.8f\t%.8f\t%.8f\n",kb,U,erg); //DBG
    }

    return erg;
}

double bending_energy_diagno(chain_struct chain[], chain_struct chain_en[], double alpha)
{

	int i;
	double xsq1, ysq1, zsq1, xsq2, ysq2, zsq2, xdiff1, ydiff1, zdiff1, xdiff2, ydiff2, zdiff2;
	double d1, d2, M, U, theta, kb, erg, th_diff2;
	erg = 0;
	for (i = 1; i < beads-1; i++)
    {

    	xdiff1 = chain[i].pos.x - chain[i-1].pos.x;
        ydiff1 = chain[i].pos.y - chain[i-1].pos.y;
        zdiff1 = chain[i].pos.z - chain[i-1].pos.z;
	//fprintf(fdbg,"%.8f\t%.8f\t%.8f\t",xdiff1,ydiff1,zdiff1); //DBG
        xdiff2 = chain[i+1].pos.x - chain[i].pos.x;
        ydiff2 = chain[i+1].pos.y - chain[i].pos.y;
        zdiff2 = chain[i+1].pos.z - chain[i].pos.z;
	//fprintf(fdbg,"%.8f\t%.8f\t%.8f\t",xdiff2,ydiff2,zdiff2); //DBG
        xsq1 = xdiff1*xdiff1;
        ysq1 = ydiff1*ydiff1;
        zsq1 = zdiff1*zdiff1;
	
        xsq2 = xdiff2*xdiff2;
        ysq2 = ydiff2*ydiff2;
        zsq2 = zdiff2*zdiff2;
      
        d1 = sqrt(xsq1 + ysq1 + zsq1);
        d2 = sqrt(xsq2 + ysq2 + zsq2);
	//fprintf(fdbg,"%.8f\t%.8f\t",d1,d2); //DBG

        M = - (xdiff1*xdiff2 + ydiff1*ydiff2 + zdiff1*zdiff2);
        
        U = M/(d1*d2);
	//fprintf(fdbg,"\t%.8f\t",U); //DBG
        kb = chain[i].bendparam.kb;
        theta = chain[i].bendparam.tetha;
	//fprintf(fdbg,"%.8f\t%.8f\t",theta,kb); //DBG
	th_diff2=(acos(U) - theta)*(acos(U) - theta); //angular difference
        erg += kb*th_diff2;
	chain_en[i].bendparam.kb = alpha*kb*th_diff2 + (1.-alpha)* chain_en[i].bendparam.kb;
	chain_en[i].bendparam.tetha = alpha*th_diff2 + (1.-alpha)* chain_en[i].bendparam.tetha;
        //fprintf(fdbg,"%d\t%.8f\t%.8f\n",i,kb,acos(U)); //DBG
    }

    return erg;
}






double kinetic_energy(chain_struct chain[])
{

	int i;
	double vx, vy, vz, erg;

	erg = 0;

    for(i = 0; i < beads; i++)
    {

    	vx = chain[i].vel.x;
    	vy = chain[i].vel.y;
    	vz = chain[i].vel.z;

    	erg += 0.5*(vx*vx + vy*vy + vz*vz);

    }

    return erg;

}




double torsion_energy(chain_struct chain[])
{
    int i, j, k;
    double a[3], b[3], c[3], d[3], u[3], v[3];
    double abs_u, abs_v, u_scal_v, Q, erg;
    double kt1, kt2, phi, phi_0, sgn;

    erg = 0.0;


    for (i = 1; i < beads-2; i++)
    {


        a[0] = chain[i].pos.x - chain[i-1].pos.x;
        a[1] = chain[i].pos.y - chain[i-1].pos.y;
        a[2] = chain[i].pos.z - chain[i-1].pos.z;

        b[0] = chain[i+1].pos.x - chain[i].pos.x;
        b[1] = chain[i+1].pos.y - chain[i].pos.y;
        b[2] = chain[i+1].pos.z - chain[i].pos.z;

        c[0] = chain[i+2].pos.x - chain[i+1].pos.x;
        c[1] = chain[i+2].pos.y - chain[i+1].pos.y;
        c[2] = chain[i+2].pos.z - chain[i+1].pos.z;

        u[0] = vector_p_0(a, b);
        u[1] = vector_p_1(a, b);
        u[2] = vector_p_2(a, b);
        
        v[0] = vector_p_0(b, c);
        v[1] = vector_p_1(b, c);
        v[2] = vector_p_2(b, c);

        abs_u = scalar_p(u, u);
        abs_u = sqrt(abs_u);

        abs_v = scalar_p(v, v);
        abs_v = sqrt(abs_v);

        u_scal_v = scalar_p(u, v);

        Q = u_scal_v/(abs_u*abs_v);

        //if(Q >  torsion_cutoff) Q =  torsion_cutoff;
        //if(Q < -torsion_cutoff) Q = -torsion_cutoff;

	if(Q >  1.0) Q =  1.0;
        if(Q < -1.0) Q = -1.0;

        kt1 = chain[i].torsionparam.kt1;
        kt2 = chain[i].torsionparam.kt2;
        phi_0 = chain[i].torsionparam.phi;

	d[0] = vector_p_0(u, v);
        d[1] = vector_p_1(u, v);
        d[2] = vector_p_2(u, v);

        sgn = scalar_p(d, b);
        sgn = sgn/sqrt(sgn*sgn);

        phi = sgn*acos(Q);

	erg += kt1*(1 - cos(phi - phi_0)) + kt2*(1 - cos(3*(phi - phi_0)));
    }
      

 return erg;

}

double torsion_energy_diagno(chain_struct chain[], chain_struct chain_en[], double alpha)
{
    int i, j, k;
    double a[3], b[3], c[3], d[3], u[3], v[3];
    double abs_u, abs_v, u_scal_v, Q, erg;
    double kt1, kt2, phi_0, phi,sgn, phi_delta, et_diff;

    erg = 0.0;


    for (i = 1; i < beads-2; i++)
    {


        a[0] = chain[i].pos.x - chain[i-1].pos.x;
        a[1] = chain[i].pos.y - chain[i-1].pos.y;
        a[2] = chain[i].pos.z - chain[i-1].pos.z;

        b[0] = chain[i+1].pos.x - chain[i].pos.x;
        b[1] = chain[i+1].pos.y - chain[i].pos.y;
        b[2] = chain[i+1].pos.z - chain[i].pos.z;

        c[0] = chain[i+2].pos.x - chain[i+1].pos.x;
        c[1] = chain[i+2].pos.y - chain[i+1].pos.y;
        c[2] = chain[i+2].pos.z - chain[i+1].pos.z;

        u[0] = vector_p_0(a, b);
        u[1] = vector_p_1(a, b);
        u[2] = vector_p_2(a, b);
        
        v[0] = vector_p_0(b, c);
        v[1] = vector_p_1(b, c);
        v[2] = vector_p_2(b, c);

        abs_u = scalar_p(u, u);
        abs_u = sqrt(abs_u);

        abs_v = scalar_p(v, v);
        abs_v = sqrt(abs_v);

        u_scal_v = scalar_p(u, v);

        Q = u_scal_v/(abs_u*abs_v);

        //if(Q >  torsion_cutoff) Q =  torsion_cutoff;
        //if(Q < -torsion_cutoff) Q = -torsion_cutoff;
	
	if(Q >  1.0) Q =  1.000000;
        if(Q < -1.0) Q = -1.000000;

	
        kt1 = chain[i].torsionparam.kt1;
        kt2 = chain[i].torsionparam.kt2;
        phi_0 = chain[i].torsionparam.phi;

	d[0] = vector_p_0(u, v);
        d[1] = vector_p_1(u, v);
        d[2] = vector_p_2(u, v);

        sgn = scalar_p(d, b);
        sgn = sgn/sqrt(sgn*sgn);

        phi = sgn*acos(Q);
	phi_delta = 1 - cos(phi - phi_0);
	et_diff = kt1*(phi_delta) + kt2*(1 - cos(3*(phi - phi_0)));
        erg += et_diff;
	chain_en[i].torsionparam.kt1 = alpha*et_diff + (1.-alpha)*chain_en[i].torsionparam.kt1;
	chain_en[i].torsionparam.phi = alpha*phi_delta + (1.-alpha)*chain_en[i].torsionparam.phi;
		
	
	//chain_en[i].pos.x=phi;
	//chain_en[i].pos.y=phi_0;
	//chain_en[i].pos.z=cos(phi-phi_0);
	//chain_en[i].vel.x=kt1*(1-cos(phi-phi_0));
	//chain_en[i].vel.y=cos(3*(phi-phi_0));
	//chain_en[i].vel.z=kt2*(1-cos(3*(phi-phi_0)));

	
	//fprintf(fdbg2,"%d\t%.8f\n",i,acos(Q)); //DBG
	//chain_en[i].torsionparam.phi=acos(Q) - phi;
	//fprintf(fdbg,"bead %d \t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\n",i,phi_0,phi,acos(Q),Q,3*(phi-phi_0),kt1*(1 - cos(phi - phi_0)) + kt2*(1 - cos(3*(phi - phi_0))),erg); //DBG
	
    }
      

 return erg;

}





double V_native(double d, double d_zero)
{

    double erg, r;
    double eps, sigsq, cut;

    eps = native_contact_strength;
    sigsq = native_contact_width*native_contact_width;
    cut = native_contact_cutoff;

    erg = 0.0;
    r = d - d_zero;

    if(fabs(r) <= cut)
    {
        // exp(- r^2 / ( 2 * 0.25^2 ) )
        erg = -eps*(exp(-0.5*r*r/sigsq) - exp(-0.5*cut*cut/sigsq));
    }


    return(erg);

}


//LJ potential

double V_LJ(double d)
{

    double erg, dto6;

    dto6 = d*d*d*d*d*d;
    dto6 = 1.0/dto6;
    erg = fourEpsilon*(dto6*dto6 - dto6);
    return(erg);

}

//disulfide bridge, SF LJ potential, with re-defined sigma and epsilon

double V_dsb(double d, bridge_struct bridge)
{
  
  double erg, r;
  
  erg=0.0;
  r=d/bridge.sigma;
  if(r <= bridge.rco) erg=bridge.eps*V_LJ(r)-bridge.Vco+(r-bridge.rco)*bridge.fco;
  return(erg);
}


