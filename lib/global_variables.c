
/* global variables */
int beads;
int nblocks;
int Nsteps;
int printfreq;
int printfinal;
int neighborfreq;
int readchain;
int dobrownian;
int tmp_Nsteps;
long int idum;
double dt;
double bond;
double dzerosq;
double kappa;
double fourEpsilon;
double tau;
double frict;
double temperature;
double brownian;
double rouse_time;
double cutoff;
double RV;
double DR2;
double **DIST;
int *nonfixed_angle;
int N_nonfixed_angle;
int niters;
int nbridges;
double sigma_el;
double sigma_ang;
int randff;
double dteq;
int Neqsteps;

double mink;
double maxk;
double alpha_runner;
int min_crossorder;

//DBG

FILE *fdbg;
FILE *fdbg2;

//MPI

int world_rank, myrun, run_rank;
char dirname[100];
char cydirname[100];
char ffdirname[100];
char windirname[100];
char outdirname[100];
FILE *outf;
//FILE *trj_vtf,*potential,*kinetic,*totenergy,*nativecon,*gyration,*msd_file;
