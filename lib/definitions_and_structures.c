
/* defines */
#define mypi (3.14159265359)
#define twopi (6.28318530718)
#define twotoonesixth (1.12246204831)  //  2^(1/6)
#define invsqrt_three (0.57735026918962576451) // 1/sqrt(3)
#define torsion_cutoff (0.9995)
#define fene_capping (0.9995)
#define four_thirds_squared (1.77777777778)
#define native_contact_strength (0.0)
#define native_contact_width (0.2)
#define native_contact_cutoff (0.75)


#define scalar_p(a,b) a[0]*b[0] + a[1]*b[1] + a[2]*b[2]
#define vector_p_0(a, b) a[1]*b[2] - a[2]*b[1]
#define vector_p_1(a, b) a[2]*b[0] - a[0]*b[2]
#define vector_p_2(a, b) a[0]*b[1] - a[1]*b[0]



typedef struct vec_{

    double x;
    double y;
    double z;
    double bead;
    
} vec;

typedef struct bendvec_{
    double kb; 
    double tetha;
    int change_angle;
} bendvec;

typedef struct torsvec_{
    double kt1;
    double kt2;
    double phi;
} torsvec;

typedef struct chain_struct_{

    vec pos;
    vec vel;
    vec force;
    vec kinetic;
    vec A_term;
    vec random_term;
    bendvec bendparam;
    torsvec torsionparam; 

} chain_struct;

typedef struct neighbor_list_{

    int n;
    int *index;

} neighbor_list;


typedef struct energy_struct_{

    double fene;
    double wca;
    double bridge;
    double bending;
    double torsion;
    double kinetic;
    
} energy_struct;


typedef struct bridge_struct_{

  int bead_i;
  int bead_j;
  double eps;
  double sigma;
  double rco;
  double Vco;
  double fco;
    
} bridge_struct;
