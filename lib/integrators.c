void evolve_v_first_order(chain_struct chain[]);
void evolve_x_first_order(chain_struct chain[], double **DISP);
void evolve_x(chain_struct chain[], chain_struct temp_chain[], double **DISP);
void evolve_v(chain_struct chain[], chain_struct temp_chain[]);




void evolve_x(chain_struct chain[], chain_struct temp_chain[], double **DISP)
{

    int i;
    double rx, ry, rz;
    double sx, sy, sz;
    double vx, vy, vz;
    double fx, fy, fz;
    double ax, ay, az;

    rx = 0.0;
    ry = 0.0;
    rz = 0.0;
    sx = 0.0;
    sy = 0.0;
    sz = 0.0;

    /* Here we update the coordinates of the chain
        using the previously computed forces. */
    for (i = 0; i < beads; i++)
    {


        /* this velocity Verlet update was implemented following Tuckermann, pag 591 */
        if(dobrownian!=0)
        {

          rx = brownian*gasdev(&idum);
          ry = brownian*gasdev(&idum);
          rz = brownian*gasdev(&idum);
          
          sx = brownian*gasdev(&idum);
          sy = brownian*gasdev(&idum);
          sz = brownian*gasdev(&idum);

          vx = temp_chain[i].vel.x;
          vy = temp_chain[i].vel.y;
          vz = temp_chain[i].vel.z;

          fx = temp_chain[i].force.x;
          fy = temp_chain[i].force.y;
          fz = temp_chain[i].force.z;

          chain[i].random_term.x = rx;
          chain[i].random_term.y = ry;
          chain[i].random_term.z = rz;

          ax = 0.5*dt*dt*fx - 0.5*dt*frict*vx + 0.5*dt*( rx + sx*invsqrt_three );
          ay = 0.5*dt*dt*fy - 0.5*dt*frict*vy + 0.5*dt*( ry + sy*invsqrt_three );
          az = 0.5*dt*dt*fz - 0.5*dt*frict*vz + 0.5*dt*( rz + sz*invsqrt_three );

          chain[i].A_term.x = ax;
          chain[i].A_term.y = ay;
          chain[i].A_term.z = az;



          chain[i].pos.x = temp_chain[i].pos.x + temp_chain[i].vel.x*dt + ax;
          chain[i].pos.y = temp_chain[i].pos.y + temp_chain[i].vel.y*dt + ay;
          chain[i].pos.z = temp_chain[i].pos.z + temp_chain[i].vel.z*dt + az;

          
        }

        else
        {
          chain[i].pos.x = temp_chain[i].pos.x + temp_chain[i].vel.x*dt + 0.5*dt*dt*temp_chain[i].force.x;
          chain[i].pos.y = temp_chain[i].pos.y + temp_chain[i].vel.y*dt + 0.5*dt*dt*temp_chain[i].force.y;
          chain[i].pos.z = temp_chain[i].pos.z + temp_chain[i].vel.z*dt + 0.5*dt*dt*temp_chain[i].force.z;
        }
        
        DISP[i][0] += chain[i].pos.x - temp_chain[i].pos.x;
        DISP[i][1] += chain[i].pos.y - temp_chain[i].pos.y;
        DISP[i][2] += chain[i].pos.z - temp_chain[i].pos.z;
    }

}

void evolve_v(chain_struct chain[], chain_struct temp_chain[])
{

    int i;
    double rx, ry, rz;
    double ax, ay, az;
    
    rx = 0.0;
    ry = 0.0;
    rz = 0.0;

    /* Here we update the velocities of the chain
        using the previously computed positions and forces. */
    for (i = 0; i < beads; i++)
    {
        
        /* this velocity Verlet update was implemented following Tuckermann, pag 591 */
        if(dobrownian!=0)
        {
            rx = chain[i].random_term.x;
            ry = chain[i].random_term.y;
            rz = chain[i].random_term.z;

            ax = chain[i].A_term.x;
            ay = chain[i].A_term.y;
            az = chain[i].A_term.z;
 

            chain[i].vel.x = (1.0 - frict)*temp_chain[i].vel.x + 0.5*dt*(chain[i].force.x + temp_chain[i].force.x) + rx - ax/tau;
            chain[i].vel.y = (1.0 - frict)*temp_chain[i].vel.y + 0.5*dt*(chain[i].force.y + temp_chain[i].force.y) + ry - ay/tau;
            chain[i].vel.z = (1.0 - frict)*temp_chain[i].vel.z + 0.5*dt*(chain[i].force.z + temp_chain[i].force.z) + rz - az/tau;

        }

       else
         {

            chain[i].vel.x = temp_chain[i].vel.x + 0.5*dt*(chain[i].force.x + temp_chain[i].force.x);
            chain[i].vel.y = temp_chain[i].vel.y + 0.5*dt*(chain[i].force.y + temp_chain[i].force.y);
            chain[i].vel.z = temp_chain[i].vel.z + 0.5*dt*(chain[i].force.z + temp_chain[i].force.z);
         
        }
    }

}


void evolve_v_first_order(chain_struct chain[])
{

    int i;
    double rx, ry, rz;
    double randX[beads], randY[beads], randZ[beads];

    for (i = 0; i < beads; i++)
      {
        randX[i] = gasdev(&idum);
        randY[i] = gasdev(&idum);
        randZ[i] = gasdev(&idum);
      }

    /* Here we update the velocities of the chain
       using the previously computed positions and forces. */


    for (i = 0; i < beads; i++)
    {

        rx = 0.0;
        ry = 0.0;
        rz = 0.0;
       
        if(dobrownian!=0)
        {
            rx = brownian*randX[i];
            ry = brownian*randY[i];
            rz = brownian*randZ[i];

            chain[i].vel.x = (1.0 - frict)*chain[i].vel.x + dt*chain[i].force.x + rx;
            chain[i].vel.y = (1.0 - frict)*chain[i].vel.y + dt*chain[i].force.y + ry;
            chain[i].vel.z = (1.0 - frict)*chain[i].vel.z + dt*chain[i].force.z + rz;
        }
      else
        {
            chain[i].vel.x = chain[i].vel.x + dt*chain[i].force.x;
            chain[i].vel.y = chain[i].vel.y + dt*chain[i].force.y;
            chain[i].vel.z = chain[i].vel.z + dt*chain[i].force.z;
        }
    }

}


void evolve_x_first_order(chain_struct chain[], double **DISP)
{

    int i;

    /* Here we update the coordinates of the chain
        using the previously computed forces. */
    for (i = 0; i < beads; i++)
    {
        chain[i].pos.x += chain[i].vel.x*dt;
        chain[i].pos.y += chain[i].vel.y*dt;
        chain[i].pos.z += chain[i].vel.z*dt;
        DISP[i][0] += chain[i].vel.x*dt;
        DISP[i][1] += chain[i].vel.y*dt;
        DISP[i][2] += chain[i].vel.z*dt;
    }

}









