//
//  genetic_mpi.c
//
//  Created by Claudio Perego and Raffaello Potestio on September 2017.
//  Copyright 2017 Max Planck Institute for Polymer Research. All rights reserved.
//



#define EXCL_VOL
#define SELF_INT_CHAIN
#define BENDING
#define TORSION
#define FIRST_ORDER
//#define SECOND_ORDER
//#define PRINT_GYR
#define VERBOSE
#define F_MAX (100000000)

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>
#include <stdbool.h>
#include <mpi.h>
#include <fcntl.h>
#include <errno.h>
#include "random.lib"
#include "my_memory.h"
#include "global_variables.c"
#include "definitions_and_structures.c"
#include "integrators.c"
#include "PolymerMD.h"
#include "input_output.c"
#include "compute_force.c"
#include "compute_energy.c"
#include "kabsch_wrapper.c"

//#define NITERS (100)
#define MAX_NBEADS (2000)
#define BLOCKSIZE (2)
#define LOWER_BOUND (4)
#define UPPER_BOUND (500)
#define TRAJ_CHUNK (5000)
#define RMSD_TOLERANCE (0.05)
#define WAIT_FOR_CHANGE (10)
#define EXPLORE_ELASTIC_PARAMS
#define MINK 30.0
#define MAXK 60.0
#define MIN_CROSSORDER 3

//#define EXPLORE_ANGLES


void print_forcefield(char name[], chain_struct chain[]);
void print_forcefieldpp(char name[], chain_struct chain[], int pid);
void print_randforcefield(char name[], char namein[]);
void map_coords_on_vector(chain_struct chain[], double str[][3]);
double runner(int pid, chain_struct ext_chain[], chain_struct ref_chain[], bridge_struct bridge[], FILE *run_out, int which_msd, int ifnc, double *ncf);
int copy_file(const char *from, const char *to);
double expmovav(double alpha, FILE *infile, FILE *outfile, int which);
void ffmixer(int nwin, int npop,  int lessfit,  char *winnersdir, int *myffmix);
int combo_generator(int *ffmix, int nwin, int npop,  int lessfit,  int order, int ready, int nblocks);
void BubbleSort(double *a, int *b, int array_size, int sign);
double compute_nc(chain_struct chain[]);

int main(int argc, char *argv[])
{
  int world_size, i, j, k, n, l, pid, label, nblocks, check, block_index;
  double init_msd, avg_msd, msd_old, msd_new, delta_msd, rate_old, rate_new,delta_rate, delta, expo, v;
  double param_old, param_new, sign;
  double str_target[MAX_NBEADS][3], str[MAX_NBEADS][3];
  int **cg_sequence;
  char name[100];
  char winnersdir[100];
  ///GENE parameters, given in input line
  ///Default values defined
  int npop = 1;
  int nwin = 1;
  int lessfit = 0;
  randff = 0; //global
  int ncycles = 1;
  int icycle = 0;
  double alpha_av = 0.02; ///exponential averaging alpha
  double thres=0.9; ///default rate
  double w_thres=0.01;
  double beta=1.0; //default beta for Metropolis
  double ncthres=0.0;
  int ifnc=0;

  mink=MINK;
  maxk=MAXK;
  min_crossorder=MIN_CROSSORDER;


  //criteria control variables
  int crit_wrap=0; //default: msd criterion for local
  int crit_rank=1; //default: success rate criterion for ranking
  int which_av=1;  //default: moving average over best forcefield
  int which_msd=0; //default: minimum along trajectory

  alpha_runner=0.4; //for msd moving average in runner routine
  //DBG
  //FILE *dbg_ffmix;
  //dbg_ffmix=fopen("ffmix.dat","w");


  ///GENE : Init MPI_COMM_WORLD

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  srand(time(NULL) + world_rank); //init seed for random call ;


  if (world_rank == 0) {
    printf("Number of threads = %d\n\n", world_size);
    if (argc <4 )
      {
#ifdef VERBOSE
	fprintf(stderr,"Requires a PolymerMD input file, a reference structure file and a Optimization input file. See source code.\n");
#endif
	exit(1);
      }

    //read parameters
    FILE *input;
    //open  input parameter file
    if((input = fopen(argv[3], "r")) == NULL) printf("\nError reading vtf file");
    //printf("%s\n",argv[3]);
    int N_parameters = 19;
    int parameter_check[N_parameters];
    char  **parameter;
    parameter = (char **) calloc(N_parameters, sizeof(char *));
    for(int ii = 0; ii < N_parameters; ii++) {
      parameter[ii] = (char *) calloc(1024, sizeof(char));
    }
    char string[1024];

    sprintf(parameter[0],"#npop");   // population size
    sprintf(parameter[1],"#nwin");       // number of winners
    sprintf(parameter[2],"#threshold");    // folding  msd threshold
    sprintf(parameter[3],"#fermiw");    // fermi function width (opt)
    sprintf(parameter[4],"#lowfit");        // number  of low fit candidates (opt)
    sprintf(parameter[5],"#rand");          // randomized low fit (opt)
    sprintf(parameter[6],"#alpha");    // exponential average factor for local mutations (opt)
    sprintf(parameter[7],"#icycle");    // initial cycle (opt)
    sprintf(parameter[8],"#fcycle");    // final cycle (opt)
    sprintf(parameter[9],"#local_crit");    // criterion for local mutations (opt) (if 0 msd, else rate)
    sprintf(parameter[10],"#rank_crit");    // criterion for ranking (opt) (if 1 rate, else msd)
    sprintf(parameter[11],"#msd_traj");    // msd used for folding (opt)
    sprintf(parameter[12],"#min_cross_order");    // minimum order of crossover (opt)
    sprintf(parameter[13],"#min_randk");    // min k of random generation (opt)
    sprintf(parameter[14],"#max_randk");    // max k of random generation (opt)
    sprintf(parameter[15],"#best_average");  // if != 1, mutated instead of best in exponential average (opt)
    sprintf(parameter[16],"#alpha_runner");  // alpha of runner exponential average (opt)
    sprintf(parameter[17],"#beta_accept");  // beta for mutation acceptance rule (opt)
    sprintf(parameter[18],"#nc_threshold");  // native contact threshold for rate (opt)


    for(int ii = 0 ; ii < N_parameters ; ii++ ) parameter_check[ii]=0;
    double value;
    int kk=0;
    bzero(string, sizeof(string));
    while((check=fscanf(input,"%s %lf", string, &value))==2 && kk<N_parameters) {
      //printf("%d\t%s\t%lf\n",check,string,value);
      for( i = 0 ; i < N_parameters ; i++ )
	{
	  if ( strcmp( string, parameter[i] ) == 0 )
	    {
	      kk++;
	      switch(i)
		{
		case 0:
		  npop = (int)value;
		  parameter_check[i]=1;
		  break;
		case 1:
		  nwin = (int)value;
		  parameter_check[i]=1;
		  break;
		case 2:
		  thres = (double)value;
		  parameter_check[i]=1;
		  break;
		case 3:
		  w_thres = (double)value;
		  parameter_check[i]=1;
		  break;
		case 4:
		  lessfit = (int)value;
		  parameter_check[i]=1;
		  break;
		case 5:
		  randff = (int)value;
		  parameter_check[i]=1;
		  break;
		case 6:
		  alpha_av = (double)value;
		  parameter_check[i]=1;
		  break;
		case 7:
		  icycle = (int)value;
		  parameter_check[i]=1;
		  break;
		case 8:
		  ncycles = (int)value;
		  parameter_check[i]=1;
		  break;
		case 9:
		  crit_wrap = (int)value;
		  parameter_check[i]=1;
		  break;
		case 10:
		  crit_rank = (int)value;
		  parameter_check[i]=1;
		  break;
		case 11:
		  which_msd = (int)value;
		  parameter_check[i]=1;
		  break;
		case 12:
		  min_crossorder = (int)value;
		  parameter_check[i]=1;
		  break;
		case 13:
		  mink = (double)value;
		  parameter_check[i]=1;
		  break;
		case 14:
		  maxk = (double)value;
		  parameter_check[i]=1;
		  break;
		case 15:
		  which_av = (int)value;
		  parameter_check[i]=1;
		  break;
		case 16:
		  alpha_runner = (double)value;
		  parameter_check[i]=1;
		  break;
    case 17:
		  beta = (double)value;
		  parameter_check[i]=1;
		  break;
    case 18:
      ncthres = (double)value;
      parameter_check[i]=1;
      break;
		}
	    }
	}
      bzero(string, sizeof(string));
    }

    for(int ii = 0 ; ii < N_parameters-16 ; ii++ ) { //no check for optional parameters
      if(parameter_check[ii] == 0 )
	{
	  fprintf(stderr,"%s not set! Abort. \n",parameter[ii]);
	  exit(1);
	}
    }

    printf("Optimizing with %d runs, choosing %d winners.\n Moving average with alpha = %.4f\n",npop,nwin,alpha_av);
    printf("Mixing %d bad-fitting forcefields.\n",lessfit);
    printf("Crossover order = %d.\n",min_crossorder);
    if(randff!=0){
      printf("Low-fit forcefields are randomly generated.\n");
      printf("Range of random k: [ %.4f , %.4f ].\n",mink,maxk);
    }
    printf("Msd threshold = %.4f\tFermi function width = %.4f\n",thres,w_thres);
    if(which_msd!=0){
      printf("Final snap msd is used to identify folding.\n");
    }else{
      printf("Min msd (exponentially averaged) is used to identify folding.\n");
    }
    printf("Performing %d cycles. Initial = %d, final = %d\n",ncycles-icycle,icycle,ncycles-1);

    //local mutation criterion
    if ( crit_wrap!=0 ){
      crit_wrap=1; //set binary
      printf("Local mutation accept/reject criterion: rate of runs below msd threshold\n");
    }else{
      printf("Local mutation accept/reject criterion: average lowest msd from reference structure\n");
    }

    //ranking criterion
    if ( crit_rank==1 ){
      printf("Ranking criterion: rate of runs below msd threshold\n");
    }else{
      crit_rank=0; //set binary
      printf("Ranking criterion: average lowest msd from reference structure\n");
    }

    //averaging set
    if ( which_av==1 ){
      printf("Moving average of best results along each run.\n");
    }else{
      which_av=0; //set binary
      printf("Moving average of mutations along each run.\n");
    }

    printf("Trajectory msd with moving average = %.4f.\n",alpha_runner);
    printf("Beta for acceptance rate = %.4f.\n",beta);

    if (ncthres>0.0){
      printf("Success rate computed with native contacts.\n");
      printf("Native contact threshold for folding = %.4f.\n",ncthres);
      ifnc=1;
    }
    //read the number of beads right away
    get_beadn(argv[1]);
    printf("Number of beads = %d\n\n", beads);
    //if (run_rank == 0) fprintf(f_run_out,"The discretized sequence consists of %d blocks\n\n", nblocks); fflush(f_run_out);
    fclose(input);
  }

  //communicate parameters from root to all processes
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Bcast(&npop, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&nwin, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&thres, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&w_thres, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&lessfit, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&randff, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&alpha_av, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&icycle, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&ncycles, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&crit_wrap, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&crit_rank, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&which_msd, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&min_crossorder, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&mink, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&maxk, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&which_av, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&alpha_runner, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&beta, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&ncthres, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&ifnc, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&beads, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD);

  nblocks = (int)floor((double)(beads-2)/BLOCKSIZE);
  if((beads-2)%BLOCKSIZE!=0) nblocks=nblocks+1; //extra block for the remainder
  //allocate ffmix matrix, npop*nblocks-sized array containing the index of the original forcefield.

  int *ffmix = (int *) malloc( npop*nblocks * sizeof(int));
  //initial state of ffmix reproduces previous npop forcefields (0...npop-1)
  for(int i=0; i<npop; i++){
    for(int j=0; j<nblocks; j++) ffmix[i*nblocks+j] = i;
  }
  //myffmix will be filled locally for each run
  int *myffmix = (int *) malloc( nblocks * sizeof(int));
  for(int i=0; i<nblocks; i++) myffmix[i]=-1;

  if (world_rank == 0) {
    printf("The discretized sequence consists of %d blocks\n\n", nblocks);
  }




  ///GENE

  //set size of population runs
  int run_size = (int) ((double)world_size)/((double)npop);
  if(run_size == 0){
    fprintf(stderr,"Population number larger than number of processes\n");
    exit(1);
  }


  if(world_size%npop){
    fprintf(stderr,"ERROR: the number of processes is not a multiple of the population number, exiting.\n");
    exit(1);
    ///Extra runs will be performed by the remaining processes, but eventually unused by the ranking
  }

  //Loop over cycles
  for(int ncyc=icycle; ncyc<ncycles; ncyc++){

    //Create directories
    MPI_Barrier(MPI_COMM_WORLD);
    struct stat st = {0};
    cydirname[0]=0;
    sprintf(cydirname,"cycle_%d/",ncyc);

    if(world_rank == 0) {
      if (stat(cydirname, &st) == -1) {
	mkdir(cydirname, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);

    //Split MPI_COMM_WORLD in npop wrappers

    myrun = world_rank/run_size; //color of wrapper

    MPI_Comm run_comm;
    MPI_Comm_split(MPI_COMM_WORLD, myrun, world_rank, &run_comm);

    int run_rank;
    MPI_Comm_rank(run_comm, &run_rank); //get rank of communicator
    MPI_Comm_size(run_comm, &run_size); //get size of communicator


    if (world_rank == 0) {
      printf("Size of run communicators = %d\n\n",run_size);
    }

    if (world_rank == world_size-1) {
      printf("Size of last communicator = %d\n\n",run_size);
    }

    sprintf(dirname,"%srun_%d",cydirname,myrun);
    sprintf(outdirname,"%s/outfiles",dirname);

    //generate run and outf directory

    if(run_rank == 0){
      //fprintf(outf,"I am creating %s. \n",dirname); fflush(outf);
      struct stat st = {0};
      if (stat(dirname, &st) == -1) {
	mkdir(dirname, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
      }
      struct stat st2 = {0};
      if (stat(outdirname, &st2) == -1) {
	mkdir(outdirname, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
      }
    }
    MPI_Barrier(run_comm);
    //forcefield preparation

    //ffdirname equal to dirname, the forcefields will be placed in the run_i directories
    sprintf(ffdirname,"%s",dirname);
    sprintf(winnersdir,"cycle_%d/winners",ncyc-1);
    if(ncyc==0){ //copy forcefield.dat to run directory
      if(run_rank == 0){
	//move forcefields in run_i
	char ff_from[100];
	char ff_to[100];
	sprintf(ff_to,"%s/forcefield.dat",ffdirname);
	sprintf(ff_from,"forcefield.dat");
	copy_file(ff_from,ff_to);
      }
    }else{ //chose winners and mix them
      if(world_rank==0){
	//generate combos (only root)

	int combos_ready=nwin; //filled rows of ffmix (initially the winners)
	int order=min_crossorder;
	while(combos_ready<npop){
	  //generate all possible order-combinations
	  combos_ready=combo_generator(ffmix, nwin, npop, lessfit, order, combos_ready, nblocks);
	  order++;
	  //if combos_ready==npop ffmix is complete
	}
	/*fprintf(dbg_ffmix,"ncyc = %d \n\n",ncyc);
	  for(int j=0; j<nblocks; j++){
	  for(int i=0; i<npop; i++){
	  fprintf(dbg_ffmix,"%d\t",ffmix[i*nblocks+j]);
	  }
	  fprintf(dbg_ffmix,"\n");
	  }*/

	//generate random forcefields
	if(randff!=0){
	  for(i=0; i<lessfit; i++){ //these are the losers (or random generated)
	    int k=nwin+i;
	    int last=npop-lessfit+i;
	    char namein[100];
	    sprintf(namein, "%s/%d_forcefield.dat",winnersdir, last); //winner file
	    char nameout[100];
	    sprintf(nameout, "%s/%d_randff.dat",winnersdir, last); //rand file
	    print_randforcefield(nameout, namein); //print random forcefield on file
	  }
	}

      }


      //communicate ffmix to all processes
      MPI_Barrier(MPI_COMM_WORLD);
      MPI_Bcast(ffmix, nblocks*npop, MPI_INT, 0, MPI_COMM_WORLD);
      MPI_Barrier(MPI_COMM_WORLD);

      //every run_rank==0 process writes its own forcefield and copies it in the right directory
      if(run_rank == 0){
	//copy to myffmix
	for(int i=0; i<nblocks; i++) myffmix[i]=ffmix[myrun*nblocks+i];
	//write forcefield.dat
	ffmixer(nwin, npop, lessfit, winnersdir, myffmix);
      }

    }
    MPI_Barrier(MPI_COMM_WORLD);


    //Processor output files
    char rstr[80];
    sprintf(rstr,"%s/out_%d_%d.dat",outdirname,run_rank,world_rank);
    outf = fopen(rstr, "w");

    fprintf(outf,"My world rank is %d, I am the %d process of %d run, of size %d\n\n",world_rank,run_rank,myrun,run_size);
    fprintf(outf,"ffdir %s\ncydir %s\ndir %s\n",ffdirname,cydirname,dirname);  //fflush(outf);



    //wrappers

    //msd array, one slot each process
    double msd[run_size];
    double nc[run_size];
    double mync=0.0;
    double mymsd=0.0;
    double msd_run_av=0.0;
    double msd_run_av_n=0.0;
    double rate_run_av=0.0;
    double rate_run_av_n=0.0;


    chain_struct *chain, *temp_chain, *ref_chain;
    chain = NULL;
    temp_chain = NULL;
    ref_chain = NULL;
    bridge_struct *bridge;
    FILE *f_msd,*f_run_out,*f_rate;

    if (run_rank == 0) {
      sprintf(name, "%s/msd_file.dat",dirname);
      if( ( f_msd = fopen(name,"w") ) == NULL )
	{
#ifdef VERBOSE
	  fprintf(stderr,"Cannot open file msd_file.dat\nAborting.\n");
#endif
	  exit(1);
	}
      fprintf(f_msd, "#iteration   msd   msd_new\n");
      fclose(f_msd);

      sprintf(name, "%s/rate_file.dat",dirname);
      if( ( f_rate = fopen(name,"w") ) == NULL )
	{
#ifdef VERBOSE
	  fprintf(stderr,"Cannot open file rate_file.dat\nAborting.\n");
#endif
	  exit(1);
	}
      fprintf(f_rate, "#iteration   rate   rate_new\n");
      fclose(f_rate);


      //run_output_file

      sprintf(name, "%s/run_output.dat",dirname);
      if( ( f_run_out = fopen(name,"w") ) == NULL )
	{
#ifdef VERBOSE
	  fprintf(stderr,"Cannot open file run_output.dat\nAborting.\n");
#endif
	  exit(1);
	}

    }
    MPI_Barrier(run_comm);
    system_parameter_setup(world_rank, argv[1], &chain, &temp_chain,&bridge);
    remove_com(chain);

    //initialize bridge
    for(l=0; l<nbridges; l++){
      bridge[l].Vco = V_dsb(bridge[l].rco*bridge[l].sigma,bridge[l]); //the function take code units input
      bridge[l].fco = -dVdr_dsb(bridge[l].rco*bridge[l].sigma,bridge[l]);
    }



    allocate_chain_struct(&ref_chain);
    read_conf(argv[2], ref_chain);

    read_Native_distance();

    map_coords_on_vector(chain, str);
    map_coords_on_vector(ref_chain, str_target);

    init_msd = best_rmsd(str, str_target, beads);
    init_msd = init_msd*init_msd; //msd, not rmsd

    //if (run_rank == 0) fprintf(f_run_out,"Initial MSD: %8.5lf\n\n", init_msd); fflush(f_run_out);
    fprintf(outf,"Initial MSD: %8.5lf\n\n", init_msd); //fflush(outf);

    msd_old = init_msd;
    msd_new = init_msd;

    rate_old=0.0;
    rate_new=rate_old;

    cg_sequence = i2t(nblocks, BLOCKSIZE);


    for(i = 0; i < beads-2; i++)
      {
        j = (int)floor((double)i/BLOCKSIZE);
        k = i%BLOCKSIZE;
        cg_sequence[j][k] = i+1;
      }

    /*if (world_rank == 0){
      for(i=0; i<nblocks; i++){
	printf("%d block =\t", i);
	for(j=0; j<BLOCKSIZE; j++){
	  printf("%d\t",cg_sequence[i][j]);
	}
	printf("\n");
      }
      }*/


    delta=0.0; //initialize delta
    /* MONTE CARLO BLOCK STARTS HERE */
    for(n = 0; n < niters; n++){
      if(n>0){ //first run without changes
#ifdef EXPLORE_ELASTIC_PARAMS
        // choose the parameter
        param_new = 0.0;
	param_old=0.0;

	do
	  {
	    // choose a block and a parameter type
	    block_index = (int)intran2(&idum, nblocks);
	    k = (int)intran2(&idum, 2);
	    double rgas0 = gasdev(&idum);

	    //propagate random picks from root

	    //fprintf(outf,"Before %d - %d - %8.5lf\n", block_index, k, rgas0); fflush(outf);	    //DBG

	    MPI_Barrier(run_comm);
	    MPI_Bcast(&block_index, 1, MPI_INT , 0,  run_comm);
	    MPI_Bcast(&k, 1, MPI_INT , 0,  run_comm);
	    MPI_Bcast(&rgas0, 1, MPI_DOUBLE , 0,  run_comm);
	    MPI_Barrier(run_comm);

	    //fprintf(outf,"After %d - %d - %8.5lf\n", block_index, k, rgas0); fflush(outf);	    //DBG

            // store the old value
            i = cg_sequence[block_index][0];

            check = 0;
            if((k==1)&&(i==(beads-2))) check = 1;

            switch(k)
	      {
	      case 0:
		param_old = chain[i].bendparam.kb;
		break;
	      case 1:
		param_old = chain[i].torsionparam.kt1;
		break;
	      }

            /*
	      sign = 1.0;
	      if(intran2(&idum, 2)>0.0) sign = -1.0;
	      param_new = param_old + 1.0*sign;
            */
            param_new = param_old + sigma_el*rgas0;
	    //if(block_index == nblocks-1){
	    //  fprintf(outf,"WARNING bi = %d\t i= %d\tpar = %.4f\n\n", block_index, cg_sequence[block_index][0],param_new); //DBG
	    //}


	  }while((param_new < LOWER_BOUND)||(param_new > UPPER_BOUND)||(check==1));

        //if(run_rank == 0 ) fprintf(f_run_out,"--- %d %d %8.1lf %8.1lf\n\n", block_index, k, param_old, param_new); fflush(f_run_out);
	fprintf(outf,"Parameter change --- %d\t%d\t%8.1lf\t%8.1lf\n\n", block_index, k, param_old, param_new); //fflush(outf);

	// change the parameter (every process does the same)
        for(i = 0; i < BLOCKSIZE; i++)
	  {

            j = cg_sequence[block_index][i];

            check = 0;
            if((k==1)&&(i==(beads-2))) check = 1;

            switch(k)
	      {
	      case 0:
		chain[j].bendparam.kb = param_new;
		break;
	      case 1:
		chain[j].torsionparam.kt1 = param_new;
		chain[j].torsionparam.kt2 = param_new/3.0;
		break;
	      }

            if(j == beads-2) break;

	  }

#endif



#ifdef EXPLORE_ANGLES
        // assign temp angle
        k = (int)intran2(&idum, N_nonfixed_angle);
	double rgas1 = gasdev(&idum);
	//propagate random picks from root

	MPI_Barrier(run_comm);
	MPI_Bcast(&k, 1, MPI_INT , 0,  run_comm);
	MPI_Bcast(&rgas1, 1, MPI_DOUBLE , 0,  run_comm);
	MPI_Barrier(run_comm);

	i = nonfixed_angle[k];
        param_old = chain[i].bendparam.tetha;

	param_new = param_old + sigma_ang*rgas;
	//---> MPI communiicate i,param_old,param_new from 0 to others
       	chain[i].bendparam.tetha = param_new;
#endif

    	//---> MPI copy chain from rank = 0 to other processes (locked)
    	//MPI_Barrier(run_comm);
    	//MPI_Bcast(chain, beads, chain_mpi, 0,  run_comm);
    	//MPI_Barrier(run_comm);

      }

      // run the simulation
      //if(run_rank == 0) fprintf(f_run_out,"Iteration # %d\n", n);  fflush(f_run_out);
      fprintf(outf,"Iteration # %d\n", n);  //fflush(outf);
      //print seed on output file
      fprintf(outf,"\nMy seed is %ld\n",idum);

      /*for(int k=0; k<beads; k++){
      //fprintf(outf,"%d\t%.f %.f %.f %.f %.f %.f\t",n,chain[k].pos.bead,chain[k].vel.bead,chain[k].force.bead,chain[k].kinetic.bead,chain[k].A_term.bead,chain[k].random_term.bead);
      //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].pos.x,chain[k].pos.y,chain[k].pos.z);
      //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].vel.x,chain[k].vel.y,chain[k].vel.z);
      //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].force.x,chain[k].force.y,chain[k].force.z);
      //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].kinetic.x,chain[k].kinetic.y,chain[k].kinetic.z);
      //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].A_term.x,chain[k].A_term.y,chain[k].A_term.z);
      //fprintf(outf,"%.8lf\t%.8lf\t%.8lf\t",chain[k].random_term.x,chain[k].random_term.y,chain[k].random_term.z);
      fprintf(outf,"%d  %d\t%.8lf\t%.8lf\t%d\t%.8lf\t%.8lf\t%.8lf\n",n,k,chain[k].bendparam.kb,chain[k].bendparam.tetha,chain[k].bendparam.change_angle,chain[k].torsionparam.kt1, chain[k].torsionparam.kt2, chain[k].torsionparam.phi);
      }*/

      // nprocs runners
      label = n*1000+run_rank;
      initialize_chain(chain); //re-intialize configuration
      remove_com(chain);
      mymsd = runner(label, chain, ref_chain, bridge, outf, which_msd, ifnc, &mync);
      fprintf(outf,"My msd  = %8.5lf\t", mymsd*mymsd); //fflush(outf); //again msd, not rmsd
      if(ifnc){
        fprintf(outf,"My nc  = %8.5lf\n", mync); //fflush(outf); //again msd, not rmsd
      }else{
        fprintf(outf,"\n");
      }
      //for(int rr = 0; rr < run_size; rr++) fprintf(outf,"msd %d = %8.5lf\n", rr, msd[rr]); fflush(outf); //DBG
      //---> MPI (barrier to wait for all the runners to average)
      MPI_Barrier(run_comm);
      MPI_Allgather(&mymsd, 1, MPI_DOUBLE, msd, 1, MPI_DOUBLE, run_comm);
      MPI_Allgather(&mync, 1, MPI_DOUBLE, nc, 1, MPI_DOUBLE, run_comm);
      MPI_Barrier(run_comm);
      //---> MPI communicate msd all around
      //msd[] contain all RMSD values
      rate_new= 0.0;
      avg_msd = 0.0;
      for(int rr = 0; rr < run_size; rr++)
	{
	  //fprintf(outf,"%d\t%.8lf\n",n,msd[k]); //DBG
	  msd[rr] = msd[rr]*msd[rr]; //msd, not rmsd
	  if(run_rank == 0) fprintf(f_run_out,"Proc %d - msd = %8.5lf\n", rr, msd[rr]); //fflush(f_run_out);
	  fprintf(outf,"Proc %d - msd = %8.5lf\n", rr, msd[rr]); //fflush(outf);

	  //increase rate with Fermi function
	  double delta_fermi;
	  if(ifnc){
	    delta_fermi = (ncthres-nc[rr])/w_thres;
	  }else{
	    delta_fermi = (msd[rr]-thres)/w_thres;
	  }
	  if(delta_fermi <= -20.0){
	    rate_new+=1.0;
	  }else if (delta_fermi >-20.0 && delta_fermi <= 20.0){
	    rate_new+=1.0/(exp(delta_fermi)+1.0);
	  }else{
	    rate_new+=0.0;
	  }

	  avg_msd += msd[rr];
	}
      avg_msd = avg_msd/run_size;
      rate_new = rate_new/run_size;
      if(run_rank == 0){
        if(ifnc) {
          fprintf(f_run_out,"Average msd = %8.5lf\t Rate, nc over %.4f = %8.5lf\n", avg_msd,ncthres,rate_new); fflush(f_run_out);
        }else{
          fprintf(f_run_out,"Average msd = %8.5lf\t Rate below %.4f = %8.5lf\n", avg_msd,thres,rate_new); fflush(f_run_out);
        }

      } fprintf(outf,"Average msd = %8.5lf\t Rate below %.4f = %8.5lf\n", avg_msd,thres,rate_new); fflush(outf);

      // calculate the difference between new and old MSD
      msd_new = avg_msd;
      if(n>0){
	delta_msd = msd_new - msd_old;
	delta_rate = -(rate_new - rate_old);


	if(crit_wrap==1){//rate criterion
	  delta=delta_rate;
	  expo=exp(-delta_rate*((double)beta)*run_size);
	}else{//msd criterion
	  delta=delta_msd;
	  expo=exp(-delta_msd*((double)beta));
	}

	// accept/reject new parameter
	if(delta<=0.0)
	  {
	    // accept
	    msd_old = msd_new;
	    rate_old = rate_new;
	    fprintf(outf,"Mutation accepted\t%.8f\t%.8f\n",delta,expo);
	  }
	else
	  {

	    //v = 10.0; // greedy annealing
	    v = ran2(&idum); // Metropolis algorithm
	    //propagate random pick from root
	    MPI_Barrier(run_comm);
	    MPI_Bcast(&v, 1, MPI_DOUBLE , 0,  run_comm);
	    MPI_Barrier(run_comm);

	    if(v > expo) // reject
	      {
		fprintf(outf,"Mutation rejected\t%.8f\t%.8f\t%.8f\n",delta,expo,v);
#ifdef EXPLORE_ELASTIC_PARAMS
		// change the parameter back
		for(i = 0; i < BLOCKSIZE; i++)
		  {

		    j = cg_sequence[block_index][i];
		    switch(k)
		      {
		      case 0:
			chain[j].bendparam.kb = param_old;
			break;
		      case 1:
			chain[j].torsionparam.kt1 = param_old;
			chain[j].torsionparam.kt2 = param_old/3.0;
			break;
		      }

		    if(j == beads-2) break;

		  }
#endif

#ifdef EXPLORE_ANGLES
		// reassign old angle
		chain[i].bendparam.tetha = param_old;
#endif



	      }
	    else
	      {
		// accept
		msd_old = msd_new;
		rate_old = rate_new;
		fprintf(outf,"Mutation accepted\t%.8f\t%.8f\t%.8f\n",delta,expo,v);
	      }



	  }

      }else{ //first step, no mutations
	msd_old=msd_new;
	rate_old=rate_new;
      }
      //---> MPI 0 writes all the following

      if(run_rank == 0){
	fprintf(f_run_out,"New msd = %8.5lf\tNew rate = %8.5lf\n\n", msd_old,rate_old); //fflush(f_run_out);
	//Create output directory
	struct stat st = {0};
	sprintf(name,"%s/forcefield/",dirname);
	if (stat(name, &st) == -1) {
	  mkdir(name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}

	sprintf(name, "%s/forcefield/%d_forcefield.dat",dirname, n*1000);
	print_forcefield(name, chain);
	sprintf(name, "%s/last_forcefield.dat",dirname);
	print_forcefield(name, chain);
	if(delta<=0.0)
	  {

	    //////////////////////////////
	    sprintf(name, "%s/forcefieldpp.dat",dirname);
	    FILE *forcefieldpp_dat;
	    forcefieldpp_dat = fopen(name, "a");

	    for(i = 0; i < beads-2; i++)
	      {
		fprintf(forcefieldpp_dat, "%3d%6d%13.5lf%13.5lf\n",n, i+1, chain[i+1].bendparam.kb, chain[i+1].torsionparam.kt1);
	      }

	    fclose(forcefieldpp_dat);

	    /////////////////////////////////
	    sprintf(name, "%s/best_forcefield.dat",dirname);
	    print_forcefield(name, chain);
	  }
	sprintf(name,"%s/msd_file.dat",dirname);
	f_msd = fopen(name,"a");
	fprintf(f_msd, "%8d   %8.5lf   %8.5lf\n", n, msd_old,msd_new);
	fclose(f_msd);
	sprintf(name,"%s/rate_file.dat",dirname);
	f_rate = fopen(name,"a");
	fprintf(f_rate, "%8d   %8.5lf   %8.5lf\n", n, rate_old,rate_new);
	fclose(f_rate);
      }

      MPI_Barrier(run_comm);

    }
    /* MONTE CARLO BLOCK ENDS HERE */
    //Ranking and Chosing winner FFs
    //Use of stat.cpp and mix_ff.cpp
    //Any run communicator needs to know where to pick the correct ff

    //get running average msd/rate of forcefields
    char param[100];
    if (run_rank == 0) {
      sprintf(param,"rate");
      sprintf(name,"%s/rate_file.dat",dirname);
      FILE *f_data;
      //fprintf(f_run_out,"Trying to open %s\n",name); //DBG
      f_data = fopen(name,"r");
      FILE *f_stat;
      sprintf(name,"%s/stat.dat",dirname);
      //fprintf(f_run_out,"Trying to open %s\n",name); //DBG
      f_stat = fopen(name,"w");
      rate_run_av=expmovav(alpha_av,f_data,f_stat,0);
      rewind(f_data);
      rate_run_av_n=expmovav(alpha_av,f_data,f_stat,1);
      fclose(f_data);
      fclose(f_stat);
      fprintf(f_run_out,"Rate: Running av. %s  =  %.9g\tnew = %.9g\n",param,rate_run_av,rate_run_av_n);

      sprintf(param,"msd");
      sprintf(name,"%s/msd_file.dat",dirname);
      f_data = fopen(name,"r");
      sprintf(name,"%s/stat.dat",dirname);
      f_stat = fopen(name,"a");
      msd_run_av=expmovav(alpha_av,f_data,f_stat,0);
      rewind(f_data);
      msd_run_av_n=expmovav(alpha_av,f_data,f_stat,1);
      fclose(f_data);
      fclose(f_stat);
      fprintf(f_run_out,"Msd: Running av. %s  =  %.9g\tnew = %.9g\n",param,msd_run_av,msd_run_av_n);

      fclose(f_run_out);
    }

    fclose(outf);

    //rank different forcefields

    double run_av,run_av_n;
    if(crit_rank==1){//rate criterion ranking
      run_av=rate_run_av;
      run_av_n=rate_run_av_n;
      sprintf(param,"rate");
    }else{ //msd criterion ranking
      run_av=msd_run_av;
      run_av_n=msd_run_av_n;
      sprintf(param,"msd");
    }

    int id_mov_av[npop];
    int id_mov_av_n[npop];
    double mov_av[npop];
    double mov_av_n[npop];
    double mov_av_world[world_size];
    double mov_av_world_n[world_size];

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Allgather(&run_av, 1, MPI_DOUBLE, mov_av_world, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Allgather(&run_av_n, 1, MPI_DOUBLE, mov_av_world_n, 1, MPI_DOUBLE, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);
    for(int l=0; l<npop;l++){ //get the running average
      mov_av[l]=mov_av_world[l*run_size];
      mov_av_n[l]=mov_av_world_n[l*run_size];
      id_mov_av[l]=l;
      id_mov_av_n[l]=l;
    }


    int sign=1;
    if(crit_rank==1) sign=-1; //reverse sorting order
    BubbleSort(mov_av,id_mov_av,npop,sign); //sort averages
    BubbleSort(mov_av_n,id_mov_av_n,npop,sign); //sort averages

    sprintf(windirname,"%swinners/",cydirname); //windir known to all processes
    if(world_rank==0){ //create winner directory
      struct stat st = {0};
      if (stat(windirname, &st) == -1) {
	mkdir(windirname, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
      }
      FILE *f_msdw,*f_msdnw;
      //write the msd winner file
      sprintf(name,"%s/%s_ave_1_winners.dat",windirname,param);
      f_msdw = fopen(name,"w");
      fprintf(f_msdw,"Run #\t<%s>\n",param);
      for(int l=0; l<nwin;l++) fprintf(f_msdw,"%d\t%.9g\n",id_mov_av_n[l],mov_av_n[l]);
      fprintf(f_msdw,"\n");
      for(int l=nwin; l<npop;l++) fprintf(f_msdw,"%d\t%.9g\n",id_mov_av_n[l],mov_av_n[l]);
      fclose(f_msdw);
      sprintf(name,"%s/%s_ave_0_winners.dat",windirname,param);
      f_msdnw = fopen(name,"w");
      fprintf(f_msdnw,"Run #\t<%s>\n",param);
      for(int l=0; l<nwin;l++) fprintf(f_msdnw,"%d\t%.9g\n",id_mov_av[l],mov_av[l]);
      fprintf(f_msdnw,"\n");
      for(int l=nwin; l<npop;l++) fprintf(f_msdnw,"%d\t%.9g\n",id_mov_av[l],mov_av[l]);
      fclose(f_msdnw);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    //each run_rank=0 copies its last forcefield in winner directory

    if(run_rank==0){

      //get rrk==0 ranking
      int run_ranking=0;
      for(int l=0; l<npop;l++){
	int id;
	if(which_av==1) {
	  id=id_mov_av[l];
	}else{
	  id=id_mov_av_n[l];
	}
	if(id*run_size == world_rank) run_ranking=l;
      }

      char winnerfile[100];
      char lastfile[100];
      sprintf(winnerfile,"%s/%d_forcefield.dat",windirname,run_ranking);
      sprintf(lastfile,"%s/last_forcefield.dat",dirname);
      //fprintf(f_run_out,"Trying to open %s\n",winnerfile); //DBG
      //ff_w = fopen(winnerfile,"w");
      //fprintf(f_run_out,"Trying to open %s\n",lastfile); //DBG
      //ff_l = fopen(lastfile,"r");
      copy_file(lastfile,winnerfile);
    }



    free(chain);
    free(temp_chain);
    free(ref_chain);

  }

  free(ffmix);
  free(myffmix);
  //fclose(dbg_ffmix); //DBG

  //fclose(outf); //DBG

  free_i2t(cg_sequence);
  free_d2t(DIST);

  MPI_Finalize();
  return(0);

}




/********************************************/









void map_coords_on_vector(chain_struct chain[], double str[][3])
{
  int i;
  for(i = 0; i < beads; i++)
    {
      str[i][0] = chain[i].pos.x;
      str[i][1] = chain[i].pos.y;
      str[i][2] = chain[i].pos.z;
    }
}


void print_forcefield(char name[], chain_struct chain[])
{

  int i;
  //char name[100];
  //sprintf(name, "%d_forcefield.dat", pid);

  FILE *ff;
  if( ( ff = fopen(name,"w") ) == NULL )
    {
#ifdef VERBOSE
      fprintf(stderr,"Cannot open file %s\nAborting.\n", name);
#endif
      exit(1);
    }

  fprintf(ff, "#Elastic parameters          kb       theta         kt1         kt2         phi\n");
  fprintf(ff, "#atom\n");

  for(i = 0; i < beads-2; i++)
    {
      //printf("Writing %d %lf %lf %lf %lf %lf %d\n", i+1, chain[i+1].bendparam.kb,  chain[i+1].bendparam.tetha,chain[i+1].torsionparam.kt1, chain[i+1].torsionparam.kt2, chain[i+1].torsionparam.phi, chain[i+1].bendparam.change_angle);
      fprintf(ff, "%3d                    ", i+1);
      fprintf(ff, "%8.1lf    ", chain[i+1].bendparam.kb);
      fprintf(ff, "%8.5lf    ", chain[i+1].bendparam.tetha);
      fprintf(ff, "%8.1lf    ", chain[i+1].torsionparam.kt1);
      fprintf(ff, "%8.1lf    ", chain[i+1].torsionparam.kt2);
      fprintf(ff, "%8.5lf\n", chain[i+1].torsionparam.phi);
    }

  fclose(ff);





}

void print_randforcefield(char name[], char namein[])
{

  int i,j,l;
  //char name[100];
  //sprintf(name, "%d_forcefield.dat", pid);

  //srand(time(NULL)); //init seed for random call ;
  FILE *ff,*ffin;
  if( ( ff = fopen(name,"w") ) == NULL )
    {
#ifdef VERBOSE
      fprintf(stderr,"Cannot open file %s\nAborting.\n", name);
#endif
      exit(1);
    }

  if( ( ffin = fopen(namein,"r") ) == NULL )
    {
#ifdef VERBOSE
      fprintf(stderr,"Cannot open file %s\nAborting.\n", name);
#endif
      exit(1);
    }

  //read ffin and set angles
  char stringa[1024],stringa2[1024];
  i = 0;
  l = 0;

  double *thetav = (double *)calloc (beads, sizeof(double)); //theta vector
  double *phiv = (double *)calloc (beads, sizeof(double)); //theta vector

  do{
    fgets(stringa,sizeof(stringa),ffin);
    sscanf (stringa, "%s", stringa2);
    l = strcmp ( stringa2, "#atom");
    i++;
    if (i>1000)
      {
#ifdef VERBOSE
        fprintf(stderr,"Failed to find Coordinates string in %s\n", namein);
        fprintf(stderr,"Aborting.\nCiao.\n");
#endif
        exit(1);
      }
  }while( l !=0 );
  i=1;

  int change = 0;

  fgets(stringa,sizeof(stringa),ffin);
  while(!feof(ffin))
    {
      int P;
      double kappa_bend, tetha, kappa_tors1, kappa_tors2, phi;
      int match = sscanf(stringa, "%d %lf %lf %lf %lf %lf %d\n", &P, &kappa_bend, &tetha, &kappa_tors1, &kappa_tors2, &phi, &change);
      thetav[i] = tetha;
      phiv[i] = phi;
      //printf("%d %lf %lf %lf %lf %lf %d %d\n", i, chain[i].bendparam.kb,  chain[i].bendparam.tetha,chain[i].torsionparam.kt1, chain[i].torsionparam.kt2, chain[i].torsionparam.phi, chain[i].bendparam.change_angle, match);
      i++;
      fgets(stringa,sizeof(stringa),ffin);
    }

  fclose (ffin);



  nblocks = (int)floor((double)(beads-2)/BLOCKSIZE);
  if((beads-2)%BLOCKSIZE!=0) nblocks=nblocks+1;

  fprintf(ff, "#Elastic parameters          kb       theta         kt1         kt2         phi\n");
  fprintf(ff, "#atom\n");

  for(i = 0; i < nblocks; i++){
    double rkb = mink+(maxk-mink)*((double)rand() / (double)RAND_MAX);
    double rkt = mink+(maxk-mink)*((double)rand() / (double)RAND_MAX);

    for(j=0; j< BLOCKSIZE; j++){
      int index=(i*BLOCKSIZE)+j+1;
      fprintf(ff, "%3d                    ", index);
      fprintf(ff, "%8.1lf    ", rkb);
      fprintf(ff, "%8.5lf    ", thetav[index]);
      fprintf(ff, "%8.1lf    ", rkt);
      fprintf(ff, "%8.1lf    ", rkt/3.);
      fprintf(ff, "%8.5lf\n", phiv[index]);
      if(index == beads-2) break;
    }
  }

  fclose(ff);
  free(thetav);
  free(phiv);

}








double runner(int pid, chain_struct ext_chain[], chain_struct ref_chain[], bridge_struct bridge[], FILE *run_out, int which_msd, int ifnc, double *ncf)
{

  int i, j, Nchunks, l, n, leave;
  double str1[beads][3], str2[beads][3];
  double msd, old_msd, moving_msd, min_moving_msd, nc, old_nc, moving_nc, max_moving_nc;
  bool continue_run = true;

  chain_struct *chain, *temp_chain;
  chain = NULL;
  temp_chain = NULL;

  allocate_chain_struct(&chain);
  allocate_chain_struct(&temp_chain);
  copy_chain_and_parameters(ext_chain, chain);
  copy_chain_and_parameters(ext_chain, temp_chain);

  //first msd computation
  map_coords_on_vector(ref_chain, str1);
  map_coords_on_vector(chain, str2);
  moving_msd = best_rmsd(str1, str2, beads);
  if(ifnc){
    moving_nc = compute_nc(chain);
    max_moving_nc = moving_nc;
  }
  min_moving_msd=moving_msd;

  double **DISP;
  DISP = d2t(beads,3);

  energy_struct energy;
  initialize_energy_struct(&energy);

  neighbor_list *neighbors;
  neighbors = NULL;
  allocate_neighbor_list(&neighbors);
  update_neighbors_VL(chain, neighbors, DISP);

  //here the trajectory and energy file buffers are defined
  char name[128];
  sprintf(name, "%s/traj/traj_%d.vtf",dirname, pid);
  FILE *trj_vtf;
  trj_vtf = fopen(name, "w");
  FILE *gyration;
#ifdef PRINT_GYR
  sprintf(name, "%s/gyration/gyration_%d.vtf",dirname,pid);
  gyration = fopen(name, "w");
#endif
  sprintf(name, "%s/energy/potential_%d.dat",dirname,pid);
  FILE *potential;
  potential = fopen(name, "w");
  sprintf(name, "%s/energy/kinetic_%d.dat",dirname,pid);
  FILE *kinetic;
  kinetic = fopen(name, "w");
  sprintf(name, "%s/energy/totenergy_%d.dat",dirname,pid);
  FILE *totenergy;
  totenergy = fopen(name, "w");


  if(printfreq>0)
    {
      print_traj(chain, 0, trj_vtf, gyration);
      tmp_Nsteps = Nsteps;
      Nsteps = ((Nsteps/printfreq))*printfreq;
#ifdef VERBOSE
      if(tmp_Nsteps != Nsteps) fprintf(run_out,"Changed Nsteps from %d to %d\n", tmp_Nsteps, Nsteps); //fflush(run_out);
#endif
    }

  Nchunks = (int)floor(Nsteps/TRAJ_CHUNK);
  i = 1;
  leave = 0;

  //for(i = 1; i <= Nsteps; i++)
  for(l = 0; l < Nchunks; l++)
    {

      for(n = 0; n < TRAJ_CHUNK; n++)
	{

#ifdef FIRST_ORDER
	  continue_run = force(chain, neighbors,bridge);
	  if(continue_run == false) break;
	  evolve_v_first_order(chain);
	  evolve_x_first_order(chain, DISP);
#endif



#ifdef SECOND_ORDER
	  copy_chain(chain, temp_chain);
	  continue_run = force(temp_chain, neighbors,bridge);
	  if(continue_run == false) break;
	  evolve_x(chain, temp_chain, DISP);
#endif

	  for ( j = 0 ; j < beads ; j ++)
	    {
	      if (DISP[j][0]*DISP[j][0] + DISP[j][1]*DISP[j][1] + DISP[j][2]*DISP[j][2] >= 0.25 * DR2 )
		{
		  update_neighbors_VL(chain, neighbors, DISP);
		  break;
		}
	    }

#ifdef SECOND_ORDER
	  continue_run = force(chain, neighbors,bridge);
	  if(continue_run == false) break;
	  evolve_v(chain, temp_chain);
	  remove_com(chain);
#endif

	  if(printfreq>0)
	    {
	      if(i%printfreq==0)
		{
		  print_traj(chain, i, trj_vtf, gyration);
		  Energy(chain, &energy, bridge);
		  print_Energy(energy, i, potential, kinetic, totenergy);
		}
	    }

	  //#ifdef VERBOSE
	  //if(i%10000==0) fprintf(run_out,"Done %8.2e of %8.2e steps\n", (double)i, (double)Nsteps);// fflush(run_out);
	  //#endif

	  i++;

	}

      if(continue_run == false) break;

      map_coords_on_vector(chain, str2);
      msd = best_rmsd(str1, str2, beads);
      //compute moving_msd and min
      if(l==0){
        moving_msd= msd;
      }else{
        moving_msd = msd*alpha_runner+(1.-alpha_runner)*moving_msd;
      }
      if(moving_msd<min_moving_msd) min_moving_msd=moving_msd;

      if(ifnc){
        nc = compute_nc(chain);
        if(l==0){
          moving_nc= nc;
        }else{
          moving_nc = nc*alpha_runner+(1.-alpha_runner)*moving_nc;
        }
        if(moving_nc>max_moving_nc) max_moving_nc=moving_nc;
      }
      //leaving snippet
      /*if((l>0)&&(fabs(old_msd - msd) < RMSD_TOLERANCE))
	{
	fprintf(run_out,"%d chunk, %d step: leave ++ = %d, old_msd = %.4f , msd = %.4f\tdiff: %.4f\n", l,i,leave,old_msd,msd,fabs(old_msd-msd)); //DBG
	leave++;
	}
	else
	{
	leave = 0;
	}
	old_msd = msd;
	//printf("ID %d msd %lf old_msd %lf leave %d\n", omp_get_thread_num(), msd, old_msd, leave);

	if(leave == WAIT_FOR_CHANGE)
	{
	fprintf(run_out,"breaking run on proc rank %d, run %d, run rank %d\n", world_rank,myrun,run_rank);
	break;
	}*/
      //fprintf(run_out,"%.4f\t%.4f\t%.4f\n",nc,moving_nc,max_moving_nc);
      
    }

  print_final_conf(pid, chain,i);

  //map_coords_on_vector(chain, str2);
  //msd = best_rmsd(str1, str2, beads);

  free_d2t(DISP);
  for(int ii = 0; ii < beads; ii++) free_i1t(neighbors[ii].index);
  free(neighbors);

  fclose(trj_vtf);
  fclose(kinetic);
  fclose(potential);
  fclose(totenergy);
#ifdef PRINT_GYR
  fclose(gyration);
#endif

  if(continue_run == false){
    fprintf(run_out,"FENE bond broken at %8.2e of %8.2e steps\n Restarting run\n", (double)i, (double)Nsteps);
    fprintf(run_out,"my seed is now %ld\n", idum);  fflush(run_out);
    //rerun
    initialize_chain(chain);
    min_moving_msd=runner(pid, chain,ref_chain,bridge,run_out,which_msd,ifnc,&max_moving_nc);
    if(which_msd!=0){
      msd=min_moving_msd;
      nc=max_moving_nc;
    }
  }

  free(chain);
  free(temp_chain);
  //fprintf(run_out,"My nc is %.4f, with smoothed maximum %.4f\n",nc,max_moving_nc);
  if(which_msd==0){
    *ncf=max_moving_nc;
    return min_moving_msd;
  }else{
    *ncf=nc;
    return msd;
  }

}



/////////////////////////////////////////


/*void print_forcefieldpp(char name[], chain_struct chain[], int pid)
  {

  int i;
  char name[101];

  //sprintf(name, "forcefieldpp_%d.dat", pid);
  FILE *forcefieldpp_dat;
  forcefieldpp_dat = fopen(name, "a");

  for(i = 0; i < beads-2; i++)
  {
  fprintf(forcefieldpp_dat, "%3d                    ", n);
  fprintf(forcefieldpp_dat, "%3d                    ", i+1);
  fprintf(forcefieldpp_dat, "%8.1lf    ", chain[i+1].bendparam.kb);
  fprintf(forcefieldpp_dat, "%8.1lf    ", chain[i+1].torsionparam.kt1);

  }

  fclose(forcefieldpp_dat);

  }*/





/////////////////////////////////////////

int copy_file(const char *from, const char *to)
{
  int fd_to, fd_from;
  char buf[4096];
  ssize_t nread;
  int saved_errno;

  fd_from = open(from, O_RDONLY);
  if (fd_from < 0)
    return -1;

  fd_to = open(to, O_WRONLY | O_CREAT | O_EXCL, 0666);
  if (fd_to < 0)
    goto out_error;

  while (nread = read(fd_from, buf, sizeof buf), nread > 0)
    {
      char *out_ptr = buf;
      ssize_t nwritten;

      do {
	nwritten = write(fd_to, out_ptr, nread);

	if (nwritten >= 0)
	  {
	    nread -= nwritten;
	    out_ptr += nwritten;
	  }
	else if (errno != EINTR)
	  {
	    goto out_error;
	  }
      } while (nread > 0);
    }

  if (nread == 0)
    {
      if (close(fd_to) < 0)
        {
	  fd_to = -1;
	  goto out_error;
        }
      close(fd_from);

      /* Success! */
      return 0;
    }

 out_error:
  saved_errno = errno;

  close(fd_from);
  if (fd_to >= 0)
    close(fd_to);

  errno = saved_errno;
  return -1;
}


double expmovav(double alpha, FILE *infile, FILE *outfile, int which){

  double read0=0;
  double read1=0;
  double in=0;
  double first=0;
  int N=0;
  int iter=0;
  double max=-1.e16;
  double min=1.e16;
  double mean=0;
  double mean2=0;
  double moving=0;
  double sdev=0;
  char buff[1024];

  //skip first line
  fgets(buff,sizeof(buff), infile);
  fgets(buff,sizeof(buff), infile);

  sscanf(buff,"%d %lf %lf",&iter,&read0,&read1);

  if(which==0){
    in=read0;
  }else{
    in=read1;
  }
  first=in;
  N++;
  if(in<min) min=in;
  if(in>max) max=in;
  mean+=in;
  mean2+=in*in;
  moving=in;
  //fprintf(outfile,"%.8f\t%.8f\t%.8f\n",in,mean,moving); //DBG
  //next steps
  fgets(buff,sizeof(buff), infile);
  while(feof(infile)==0){
    sscanf(buff,"%d %lf %lf",&iter,&read0,&read1);
    if(which==0){
      in=read0;
    }else{
      in=read1;
    }
    N++;
    if(in<min) min=in;
    if(in>max) max=in;
    mean+=in;
    mean2+=in*in;
    moving=alpha*in+(1.-alpha)*moving;
    //fprintf(outfile,"%.8f\t%.8f\t%.8f\n",in,mean,moving); //DBG
    fgets(buff,sizeof(buff), infile);
  }

  mean=mean/N;
  mean2=mean2/N;
  sdev=sqrt(N*(mean2-mean*mean)/(N-1)); //standard deviation
  fprintf(outfile,"N  =  %d\nFirst  =  %.9g\nLast  =  %.9g\nMin  =  %.9g\nMax  =  %.9g\n<x>  =  %.9g\nst.dev(x)  =  %.9g\nMoving <x>  =  %.9g\n",N,first,in,min,max,mean,sdev,moving);

  return moving;
}

void BubbleSort(double *a, int *b, int array_size, int sign)
{
  double temp;
  int temp2;
  int i, j;
  for (i = 0; i < (array_size - 1); ++i)
    {
      for (j = 0; j < array_size - 1 - i; ++j )
        {
	  if (sign*a[j] > sign*a[j+1])
            {
              temp = a[j+1];
              temp2 = b[j+1];
              a[j+1] = a[j];
              b[j+1] = b[j];
              a[j] = temp;
              b[j] = temp2;
            }
        }
    }
}


int combo_generator(int *ffmix, int nwin, int npop, int lessfit, int order, int ready, int nblocks){

  FILE *fdbg_combo;
  char fname[100];
  sprintf(fname,"%scombos.dat",cydirname);
  fdbg_combo = fopen(fname,"a");
  //srand(time(NULL)); //init seed for random call ;
  int nout=npop-ready; //number of required combos

  //number of parents for crossover
  int ncomb=nwin+lessfit;
  //generate and initialize combo_mat, order*npop matrix containing the index of the original forcefield (in ffmix this is indicated per-block, here only order positions)

  int **combo_mat = (int **) malloc( npop * sizeof(int*));
  //initialized as ffmix [0 - npop-1]
  for(int i=0; i<npop; i++){
    combo_mat[i] = (int *) malloc( order * sizeof(int));
    for(int j=0; j<order ; j++) combo_mat[i][j] = i;
  }
  //compute number of max combinations
  int max_combo = ((int) (pow((double)ncomb,(double)order)) - ncomb) ; //max number of combo that can be generated
  fprintf(fdbg_combo,"ready %d\torder %d\t max combinations = %d\n",ready,order,max_combo);
  int generated=0;

  for(int i=0; i<max_combo; i++){
//loop on maximum number of combo (i and generated are almost the same, a part that generated is incremented before the possible break)

    //generate a integer combo
    bool isnew=0;
    while(!isnew){ //repeat while combo is not new

      //random pick of order number
      for(int j=0; j<order; j++) combo_mat[nwin+i][j] = rand() % ncomb;// (int)intran2(&combo_seed, ncomb); //fill up first empty row

      //check if generated combo is new
      int different=0;

      for(int old_combo=0; old_combo<nwin+i; old_combo++) { //loop on given combos
	//compare with an old combo
	bool areequal=1;
	for(int index=0; index<order; index++){
	  if(combo_mat[nwin+i][index]!=combo_mat[old_combo][index]){
	    areequal=0;
	    break; //different, compare the following
	  }
	}
	if(areequal) break; //loop did not break, combos are equal!!! break and restart
	different++; //if it does not break the two combos are different
      }
      if(different==nwin+generated) isnew=1; //the combo is new, set to true. While loop is over
    }

    //fill in ffmix array with the new combo

    //number of blocks mutuated by each forcefield (nblocks/order+1)
    int bead_chunk=(int)((double)nblocks)/((double)order)+1;
    //fprintf(fdbg_combo,"Passing generated-th combo to ff: beadchunk = %d\n",bead_chunk);
    for(int j=0; j<order; j++){ //loop over order
      for(int k=0; k<bead_chunk; k++){ //loop over beadchunk size
	//fprintf(fdbg_combo,"beadchunk*j+k = %d nblocks = %d\n",bead_chunk*j+k,nblocks);
	if(bead_chunk*j+k==nblocks) break; //don't exceed ffmix size (should be useful for last block)
	int index=(ready+i)*nblocks+bead_chunk*j+k;
	ffmix[index]=combo_mat[nwin+i][j];
	//fprintf(fdbg_combo,"combo bit %d\t index %d (will be column %d, position %d)\n",j,index,(ready+i),bead_chunk*j+k);
      }
    }
    //increase generated and break if generated is == nout. Return 1 or 0 accordingly
    generated++;
    //fprintf(fdbg_combo,"\n\n generated = %d (%d) / %d\n\n",generated,i,nout);
    if(generated == nout) break; //all the required combos have been generated
  }

  //print combo matrix
  fprintf(fdbg_combo,"%d order combos\n",order);

  for(int j=0; j<order; j++){
    for(int i=0; i<nwin+generated; i++){
      int ffbit=combo_mat[i][j];
      if(ffbit>=nwin) ffbit=npop-lessfit+ffbit-nwin;
      fprintf(fdbg_combo,"%d\t",ffbit);
    }
    fprintf(fdbg_combo,"\n");
  }

  //print ffmix
  fprintf(fdbg_combo,"ffmix:\n");

  for(int j=0; j<nblocks; j++){
    for(int i=0; i<nwin+generated; i++){
      int ij=nblocks*i+j;
      int ffbit=ffmix[ij];
      if(ffbit>=nwin) ffbit=npop-lessfit+ffbit-nwin;
      fprintf(fdbg_combo,"%d\t",ffbit);
    }
    fprintf(fdbg_combo,"\n");
  }


  //free combo matrix
  for(int i=0; i<npop; i++) free(combo_mat[i]);
  free(combo_mat);
  fclose(fdbg_combo);
  return ready+generated;
}


void ffmixer(int nwin, int npop, int lessfit, char *winnersdir, int *myffmix){
  FILE **infiles;
  infiles = (FILE **)calloc (nwin+lessfit, sizeof(FILE*)); //nwin file pointers defined
  FILE *outfile;
  FILE *dbg; //DBG
  char name[100];
  int i;
  typedef char line_type[1024];
  int nff=nwin+lessfit;
  sprintf(name, "%s/ffmix.dat",ffdirname); //out file
  if((dbg = fopen(name, "w")) == NULL) printf("\nError reading output file");

  line_type *line = (line_type *) calloc (nff, sizeof(line_type)); //nwin read buffers defined
  fprintf(dbg,"My world rank is %d, I am the %d process of %d run\n\n",world_rank,run_rank,myrun); fflush(fdbg);
  fprintf(dbg,"I am writing in %s\n",ffdirname); fflush(fdbg);//DBG

  for(i=0; i<nwin; i++){
    sprintf(name, "%s/%d_forcefield.dat",winnersdir, i); //winner file
    //open all the files
    if((infiles[i] = fopen(name, "r")) == NULL) printf("\nError reading input file");
    fprintf(dbg,"infiles %d : %s\n",i,name); fflush(fdbg);//DBG
  }

  for(i=0; i<lessfit; i++){ //these are the losers (or random generated)
    int k=nwin+i;
    int last=npop-lessfit+i;
    if(randff==0){
      sprintf(name, "%s/%d_forcefield.dat",winnersdir, last); //winner file
    }else{
      char namein[100];
      sprintf(name, "%s/%d_randff.dat",winnersdir, last); //rand file
      //print_randforcefield(name, namein); //print random forcefield on file
    }

    //open all the files
    if((infiles[k] = fopen(name, "r")) == NULL) printf("\nError reading input file");
    fprintf(dbg,"infiles %d : %s  %d %d\n",k,name,i,last);  fflush(fdbg); //DBG
  }

  sprintf(name, "%s/forcefield.dat",ffdirname); //out file
  if((outfile = fopen(name, "w")) == NULL) printf("\nError reading output file");

  //check length
  fgets(line[0],sizeof(line[0]), infiles[0]); //get first line
  fgets(line[0],sizeof(line[0]), infiles[0]); //get second line
  int ffentries=0;
  fgets(line[0],sizeof(line[0]), infiles[0]); //get third line
  while(feof(infiles[0])==0){
    ffentries++;
    fgets(line[0],sizeof(line[0]), infiles[0]); //get next lines and count
  }
  fprintf(dbg,"ffentries = %d\tnff %d\n",ffentries,nff);  fflush(fdbg); //DBG
  rewind(infiles[0]);

  for(i=0; i<nff; i++){
    while(fgets(line[i],sizeof(line[0]), infiles[i]) == NULL) sleep(1.e-3); //fprintf(dbg,"%d NULLLLLLLLO\n",i); //get first line for each ff

    //fprintf(dbg,"%d:  %s\n",i,line[i]); fflush(dbg); //DBG
  }

  //}
  fprintf(outfile,"%s",line[0]);
  for(i=0; i<nff; i++){
    fgets(line[i],sizeof(line[0]), infiles[i]); //get line for each ff
    //fprintf(dbg,"%d:  %s\n",i,line[i]); fflush(dbg); //DBG
  }
  //fprintf(dbg,"%d:  %s\n",i,line[i]); fflush(dbg); //DBG
  //}
  fprintf(outfile,"%s",line[0]);

  for(i=0; i<ffentries ; i++){
    for(int j=0; j<nwin+lessfit; j++){
      fgets(line[j],sizeof(line[j]), infiles[j]); //get line for each ff
      //fprintf(dbg,"%d %d: %s\n",i,j,line[j]); fflush(dbg); //DBG
    }

    int block=(int)((double)i)/((double)BLOCKSIZE);
    int which=myffmix[block];
    fprintf(outfile,"%s",line[which]);
    fprintf(dbg,"%d:  block %d, which %d, line : %s\n",ffentries,block,which,line[which]); fflush(dbg); //DBG
  }

  //close files
  for(i=0; i<nwin+lessfit; i++) fclose(infiles[i]);
  fclose(outfile);
  free(infiles);
  free(line);
  fclose(dbg); //DBG
  return;
}



double compute_nc(chain_struct chain[])
{
    int i, j, q, qtot;
    double r, d, dsq, xsq, ysq, zsq, xdiff, ydiff, zdiff, d_zero, nc;

    q = 0;
    qtot = 0;

    for (i = 0; i < beads-1; i++)
    {
      for(j = i+1; j < beads; j++)
      {
        xdiff = chain[i].pos.x - chain[j].pos.x;
        ydiff = chain[i].pos.y - chain[j].pos.y;
        zdiff = chain[i].pos.z - chain[j].pos.z;

        xsq = xdiff*xdiff;
        ysq = ydiff*ydiff;
        zsq = zdiff*zdiff;

        dsq = xsq + ysq + zsq;

        d = sqrt(dsq);

        d_zero = DIST[i][j];

        r = d - d_zero;

        if(d_zero>0.0)
          {
            if(fabs(r) <= 1.0) q++;
            qtot++;
          }

      }
    }

    nc=((double)q)/((double)qtot);
    return(nc);
    //fclose(fp);

}
