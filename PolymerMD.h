/* header file for PolymerMD.c */



long int get_seed(void);
void initialize_chain(chain_struct chain[]);
void copy_chain(chain_struct old_chain[], chain_struct new_chain[]);
void copy_chain_and_parameters(chain_struct old_chain[], chain_struct new_chain[]);
void remove_com(chain_struct chain[]);
void update_neighbors_VL (chain_struct chain[], neighbor_list neighbors[], double **DISP);
void stop_chain(chain_struct chain[]);

long int get_seed(void)
{
  long int a;
  a = (long int) getpid();
  return a;
    
}


void update_neighbors_VL(chain_struct chain[], neighbor_list neighbors[], double **DISP)
{

  int i, j, ni, m;
  double delta, xdiff, ydiff, zdiff;

  for ( i = 0 ; i < beads ; i++ )
    {
      neighbors[i].n = 0;
      DISP[i][0] = 0.0;
      DISP[i][1] = 0.0;
      DISP[i][2] = 0.0;
    }


  for ( i = 0 ; i < beads; i++ )
    {

      m = 1;
      j = i + 1;
     // printf("+++%d %d %d\n", i, j, beads);
      //do
	while(j < beads)
	{ 
	  m = 1;

	  xdiff = chain[i].pos.x - chain[j].pos.x;
	  ydiff = chain[i].pos.y - chain[j].pos.y;
	  zdiff = chain[i].pos.z - chain[j].pos.z;

	  delta = xdiff*xdiff + ydiff*ydiff + zdiff*zdiff;

	  if( (delta > RV*RV))// && (j > i) )
	    {
	      m = (int)floor(sqrt(delta) - RV);
	      if(m < 1) m = 1;
	    }
      
	  if( delta < RV*RV )
	    {
	      ni = neighbors[i].n;
	      neighbors[i].index[ni] = j;
	      neighbors[i].n++;
	    }

	  j = j + m;

	//  printf("---%d %d\n", i, j);
    
	}//while(j < beads);

      m = 1;
      j = i - 1;
    
      if(j >= 0)
	{

	  do
	    {
	      m = 1;
	      xdiff = chain[i].pos.x - chain[j].pos.x;
	      ydiff = chain[i].pos.y - chain[j].pos.y;
	      zdiff = chain[i].pos.z - chain[j].pos.z;

	      delta = xdiff*xdiff + ydiff*ydiff + zdiff*zdiff;

	      if( (delta > RV*RV))// && (j > i) )
		{
		  m = (int)floor(sqrt(delta) - RV);
		  if(m < 1) m = 1;
		}
      
	      if( delta < RV*RV )
		{
		  ni = neighbors[i].n;
		  neighbors[i].index[ni] = j;
		  neighbors[i].n++;
		}

	      j = j - m;
    
	    }while(j >= 0);
	}

    }

 
}



void remove_com(chain_struct chain[])
{

    int i;
    double RX, RY, RZ;
    double VX, VY, VZ;
    double FX, FY, FZ;

    RX = 0.0;
    RY = 0.0;
    RZ = 0.0;
    
    for(i = 0; i < beads; i++)
    {
      RX += chain[i].pos.x;
      RY += chain[i].pos.y;
      RZ += chain[i].pos.z;
    }

    for(i = 0; i < beads; i++)
    {
      chain[i].pos.x -= RX/beads;
      chain[i].pos.y -= RY/beads;
      chain[i].pos.z -= RZ/beads;
    }

}


void initialize_chain(chain_struct chain[])
{

  int i, j, k;

  double x, y, z, d;
  double x2, y2, z2, d2;

  dzerosq = bond*bond;

  k = 0;

  for(i = 0; i < beads; i++)
    {
      if(i==0)
        {
          chain[i].pos.x = 0.0;
          chain[i].pos.y = 0.0;
          chain[i].pos.z = 0.0;
        }
      else
        {

	  /*
	    do
	    {
	    x = gasdev(&idum);
	    y = gasdev(&idum);
	    z = gasdev(&idum);

	    x = sqrt(x*x);

	    d = x*x + y*y + z*z;
	    d = sqrt(d);

	    x = twotoonesixth*x/d;
	    y = twotoonesixth*y/d;
	    z = twotoonesixth*z/d;

	    d = 100000.0;

	    for(j = 0; j < i; j++)
	    {

	    x2 = chain[i-1].pos.x + x - chain[j].pos.x;
	    y2 = chain[i-1].pos.y + y - chain[j].pos.y;
	    z2 = chain[i-1].pos.z + z - chain[j].pos.z;

	    d2 = x2*x2 + y2*y2 + z2*z2;
	    d2 = sqrt(d2);

	    if(d2<d) d = d2;

	    }

	    }while(d<1.1);
	  */


	  x = 1.0 + 0.1*gasdev(&idum);
	  //printf("seed = %ld \t rand = %lf \n",idum,(x-1.0)/0.1);
	  /*
	  if(k == 0)
	    {
	      y = 1.0 + 0.1*gasdev(&idum);
	      k = 1;
	    }
	  else
	    {
	      y = -1.0 + 0.1*gasdev(&idum);
	      k = 0;
	      }
      */

          y = 0.5*gasdev(&idum);
          z = 0.5*gasdev(&idum);

          x = sqrt(x*x);

          d = x*x + y*y + z*z;
          d = sqrt(d);

          x = twotoonesixth*x/d;
          y = twotoonesixth*y/d;
          z = twotoonesixth*z/d;




        

          chain[i].pos.x = chain[i-1].pos.x + x;
          chain[i].pos.y = chain[i-1].pos.y + y;
          chain[i].pos.z = chain[i-1].pos.z + z;
        }

      chain[i].vel.x = 0.0;
      chain[i].vel.y = 0.0;
      chain[i].vel.z = 0.0;

      chain[i].force.x = 0.0;
      chain[i].force.y = 0.0;
      chain[i].force.z = 0.0;

      chain[i].A_term.x = 0.0;
      chain[i].A_term.y = 0.0;
      chain[i].A_term.z = 0.0;

      chain[i].random_term.x = 0.0;
      chain[i].random_term.y = 0.0;
      chain[i].random_term.z = 0.0;
    }

}

void stop_chain(chain_struct chain[])
{
  //stops chain after equilibration
  int i;

  for(i = 0; i < beads; i++){
    chain[i].vel.x = 0.0;
    chain[i].vel.y = 0.0;
    chain[i].vel.z = 0.0;
  
    chain[i].force.x = 0.0;
    chain[i].force.y = 0.0;
    chain[i].force.z = 0.0;

    chain[i].A_term.x = 0.0;
    chain[i].A_term.y = 0.0;
    chain[i].A_term.z = 0.0;

    chain[i].random_term.x = 0.0;
    chain[i].random_term.y = 0.0;
    chain[i].random_term.z = 0.0;
  }

}

void copy_chain(chain_struct old_chain[], chain_struct new_chain[])
{

  int i;

  for(i = 0; i < beads; i++)
    {
      new_chain[i].pos.x = old_chain[i].pos.x;
      new_chain[i].pos.y = old_chain[i].pos.y;
      new_chain[i].pos.z = old_chain[i].pos.z;

      new_chain[i].vel.x = old_chain[i].vel.x;
      new_chain[i].vel.y = old_chain[i].vel.y;
      new_chain[i].vel.z = old_chain[i].vel.z;

      new_chain[i].force.x = old_chain[i].force.x;
      new_chain[i].force.y = old_chain[i].force.y;
      new_chain[i].force.z = old_chain[i].force.z;

      new_chain[i].A_term.x = old_chain[i].A_term.x;
      new_chain[i].A_term.y = old_chain[i].A_term.y;
      new_chain[i].A_term.z = old_chain[i].A_term.z;

      new_chain[i].random_term.x = old_chain[i].random_term.x;
      new_chain[i].random_term.y = old_chain[i].random_term.y;
      new_chain[i].random_term.z = old_chain[i].random_term.z;




    }

}


void copy_chain_and_parameters(chain_struct old_chain[], chain_struct new_chain[])
{

  int i;

  for(i = 0; i < beads; i++)
    {
      new_chain[i].pos.x = old_chain[i].pos.x;
      new_chain[i].pos.y = old_chain[i].pos.y;
      new_chain[i].pos.z = old_chain[i].pos.z;

      new_chain[i].vel.x = old_chain[i].vel.x;
      new_chain[i].vel.y = old_chain[i].vel.y;
      new_chain[i].vel.z = old_chain[i].vel.z;

      new_chain[i].force.x = old_chain[i].force.x;
      new_chain[i].force.y = old_chain[i].force.y;
      new_chain[i].force.z = old_chain[i].force.z;

      new_chain[i].A_term.x = old_chain[i].A_term.x;
      new_chain[i].A_term.y = old_chain[i].A_term.y;
      new_chain[i].A_term.z = old_chain[i].A_term.z;

      new_chain[i].random_term.x = old_chain[i].random_term.x;
      new_chain[i].random_term.y = old_chain[i].random_term.y;
      new_chain[i].random_term.z = old_chain[i].random_term.z;

      new_chain[i].bendparam.kb = old_chain[i].bendparam.kb;
      new_chain[i].bendparam.tetha = old_chain[i].bendparam.tetha;
        
      new_chain[i].torsionparam.kt1 = old_chain[i].torsionparam.kt1;
      new_chain[i].torsionparam.kt2 = old_chain[i].torsionparam.kt2;
      new_chain[i].torsionparam.phi = old_chain[i].torsionparam.phi;


    }

}













