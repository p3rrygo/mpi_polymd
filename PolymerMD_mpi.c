//
//  main.c
//
//  Created by Raffaello Potestio on September 24, 2013.
//  Copyright 2013 Max Planck Institute for Polymer Research. All rights reserved.
//
//  Modified by LT on October 05, 2013
//  Modified by SN since February 2014


#define EXCL_VOL
#define SELF_INT_CHAIN
#define BENDING
#define TORSION
#define FIRST_ORDER
//#define SECOND_ORDER
#define PRINT_GYR
#define PRINT_MSD
#define VERBOSE
#define ENDIAG 5000
#define MAX_NBEADS (2000)
#define F_MAX (10000000)

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include <mpi.h>
#include "random.lib"
#include "my_memory.h"
#include "global_variables.c"
#include "definitions_and_structures.c"
#include "integrators.c"
#include "PolymerMD.h"
#include "input_output.c"
#include "compute_force.c"
#include "compute_energy.c"
#include "kabsch_wrapper.c"

void map_coords_on_vector(chain_struct chain[], double str[][3]);


int main(int argc, char *argv[])
{

  int i, j, l, pid;
  double msd;
  double str_target[MAX_NBEADS][3], str[MAX_NBEADS][3];
  char name[100];
  int nprocs;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  
  if (argc <2 )
    {
#ifdef VERBOSE
      fprintf(stderr,"Requires a parameter input file. See source code.\n");
#endif
      exit(1);
    }
    
  int arg_ref=1;
  if (argc == 3)  arg_ref=2;
#ifdef VERBOSE
  if(world_rank == 0) printf("Target structure provided in %s.\n",argv[arg_ref]);      
#endif
    
  /*  SYSTEM SETUP  */
  /******************/

  chain_struct *chain, *temp_chain, *init_chain, *ref_chain;
  neighbor_list *neighbors;
  energy_struct energy;
  bridge_struct *bridge;
  
   

  initialize_energy_struct(&energy);
    

  chain = NULL;
  temp_chain = NULL;
  init_chain = NULL;
  neighbors = NULL;
   
  pid = getpid();
  //GENE : dir label computed
  sprintf(dirname, "run_%d_%d", pid,world_rank);
  sprintf(ffdirname, ".");
  char strdir[128];
  //create run directory
  struct stat st = {0};
  sprintf(strdir,"%s/",dirname);  //string is "run_pid_world_rank/"
  if (stat(strdir, &st) == -1) {
    mkdir(strdir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  }

  //open cpu-output file
  char rstr[80];
  sprintf(rstr,"%s/outfile.dat",dirname);
  outf = fopen(rstr, "w");

    
  system_parameter_setup(pid, argv[1], &chain, &temp_chain, &bridge);

  //fprintf(outf,"Seed :\t%ld\n\n",idum); //DBG
  //DBG
  //char dbgname[100];
  //sprintf(dbgname, "run_%d_%d/dbg1_%d.dat",pid, world_rank, pid);
  //if((fdbg = fopen(dbgname, "w")) == NULL) printf("\nError opening debug file");
  //sprintf(dbgname, "run_%d_%d/dbg2_%d.dat",pid, world_rank, pid);
  //if((fdbg2 = fopen(dbgname, "w")) == NULL) printf("\nError opening debug file");
  
    
  //initialize bridge
  for(l=0; l<nbridges; l++){ 
    bridge[l].Vco = V_dsb(bridge[l].rco*bridge[l].sigma,bridge[l]); //the function take code units input
    bridge[l].fco = -dVdr_dsb(bridge[l].rco*bridge[l].sigma,bridge[l]); 
  }
    
  remove_com(chain);
    
  allocate_chain_struct(&init_chain);
  copy_chain(chain, init_chain);
  read_conf(argv[1], init_chain);
  if(argc==3){
    allocate_chain_struct(&ref_chain);
    copy_chain(chain, ref_chain);
    read_conf(argv[2], ref_chain);
  }else{
    allocate_chain_struct(&ref_chain);
    copy_chain(chain, ref_chain);
    read_conf(argv[1], ref_chain);
  }
        

    
  //for(int i=0; i<beads;i++) printf("%lf %lf %lf %lf %lf %lf\n", chain[i].pos.x, chain[i].pos.y, chain[i].pos.z, chain[i].vel.x, chain[i].vel.y,chain[i].vel.z); //DBG
    
  allocate_neighbor_list(&neighbors);
  read_Native_distance();
    
  double **DISP;
  DISP = d2t(beads,3);

  //GENE dirname
  sprintf(name, "%s/msd_%d.dat",dirname, pid);
  FILE *msd_file;
#ifdef PRINT_MSD
  msd_file = fopen(name, "w");
#endif

  
  //open output files

  sprintf(name, "%s/traj/traj_%d.vtf",dirname, pid);
  FILE *trj_vtf;
  trj_vtf = fopen(name, "w");
  FILE *gyration;
#ifdef PRINT_GYR
  sprintf(name, "%s/gyration/gyration_%d.vtf",dirname,pid);
  gyration = fopen(name, "w");
#endif
  sprintf(name, "%s/energy/potential_%d.dat",dirname,pid);
  FILE *potential;
  potential = fopen(name, "w");
  sprintf(name, "%s/energy/kinetic_%d.dat",dirname,pid);
  FILE *kinetic;
  kinetic = fopen(name, "w");
  sprintf(name, "%s/energy/totenergy_%d.dat",dirname,pid);
  FILE *totenergy;
  totenergy = fopen(name, "w");
  sprintf(name, "%s/native_contacts/native_contacts_%d.dat",dirname,pid);   
  FILE *nativecon;
  nativecon = fopen(name, "w");

  
  //bead energy diagnostic

#ifdef ENDIAG
  //int aven;
  double alpha,tau_expav;
  if(world_rank == 0) printf("\n Bending and torsion energy diagnostic active\n");
  //if( ENDIAG< 1 || (printfreq%ENDIAG !=0 && ENDIAG%printfreq !=0)){
  //  aven=printfreq;
  //  printf("Averaging over %d steps, automatically set to printfreq\n",aven);
  //}else{
  //  aven=ENDIAG;
  //  printf("Averaging over %d steps\n",aven);
  //}
  if( ENDIAG<= 0.0 ){
    alpha=1.0;
    if(world_rank == 0) printf("No averaging, no timespan given\n");
  }else{
    tau_expav=ENDIAG;
    alpha=1.-exp(-1./tau_expav);
    if(world_rank == 0) printf("Exponential averaging with tau = %.4f, equivalent to alpha = %f \n",tau_expav,alpha);
  }
  chain_struct *chain_en  = (chain_struct*  ) malloc(beads * sizeof(chain_struct));
  char fbename[100];
  char ftename[100];
  FILE *f_ben,*f_ten,*f_bang,*f_tang,*f_fp;
  
  //discrete fingerprint

  int Nt = Nsteps / tau_expav ;
  printf("nsteps = %d \n",Nt);

  sprintf(fbename, "%s/benergies_%d.dat",dirname, pid);
  sprintf(ftename, "%s/tenergies_%d.dat",dirname, pid);
  if((f_ben = fopen(fbename, "w")) == NULL) printf("\nError opening benergies file");
  if((f_ten = fopen(ftename, "w")) == NULL) printf("\nError opening tenergies file");
  
  sprintf(fbename, "%s/bangles_%d.dat",dirname, pid);
  sprintf(ftename, "%s/tangles_%d.dat",dirname, pid);
  if((f_bang = fopen(fbename, "w")) == NULL) printf("\nError opening benergies file");
  if((f_tang = fopen(ftename, "w")) == NULL) printf("\nError opening tenergies file");
  
  //sprintf(fbename, "%s/ang_fp_%d.dat",dirname, pid);
  //if((f_fp = fopen(fbename, "w")) == NULL) printf("\nError opening benergies file");

  //equilibrate
  double dtrun=dt;
  dt=dteq; //change step to dteq
  update_neighbors_VL(chain, neighbors, DISP);
  for(i = 1; i <= Neqsteps; i++)
    {
      
      bool continue_run = true;
#ifdef FIRST_ORDER
      continue_run = force(chain, neighbors,bridge);
      evolve_v_first_order(chain);
      evolve_x_first_order(chain, DISP);
#endif
      
#ifdef SECOND_ORDER
      copy_chain(chain, temp_chain);
      continue_run = force(temp_chain, neighbors,bridge);
      evolve_x(chain, temp_chain, DISP);
#endif
      
      for ( j = 0 ; j < beads ; j ++)
        {
          if (DISP[j][0]*DISP[j][0] + DISP[j][1]*DISP[j][1] + DISP[j][2]*DISP[j][2] >= 0.25 * DR2 )
	    {
	      update_neighbors_VL(chain, neighbors, DISP);
	      break;
	    }
        }
      
#ifdef SECOND_ORDER
      continue_run = force(chain, neighbors,bridge);
      evolve_v(chain, temp_chain);
      remove_com(chain);
#endif
      
      if(continue_run == false){
	fprintf(outf,"FENE bond broken at %8.2e of %8.2e equilibration steps\n Run over.\n", (double)i, (double)Nsteps); fflush(outf);
	fprintf(outf,"my seed is now %ld\n", idum);
	break;
      }
    }
  //stop_chain(chain);
  dt=dtrun;
#ifdef VERBOSE
  if(i==Neqsteps) fprintf(outf,"Equilibration done\n");
#endif



  
  bending_energy_diagno(chain,chain_en,1.0); //Step 0 calculation (no averaging) ENDIAG
  torsion_energy_diagno(chain,chain_en,1.0);
  double av_ben=0.0;
  double av_ten=0.0;
  double av_bang=0.0;
  double av_tang=0.0;
  for(j=1; j< beads-1; j++){
    fprintf(f_ben,"%.8f\t",chain_en[j].bendparam.kb); 
    fprintf(f_ten,"%.8f\t",chain_en[j].torsionparam.kt1);
    fprintf(f_bang,"%.8f\t",chain_en[j].bendparam.tetha); 
    fprintf(f_tang,"%.8f\t",chain_en[j].torsionparam.phi);
    //if(j==8) fprintf(fdbg,"%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\n",chain_en[j].pos.x,chain_en[j].pos.y,chain_en[j].pos.z,chain_en[j].force.x,chain_en[j].force.y,chain_en[j].force.z,chain_en[j].vel.x,chain_en[j].vel.y,chain_en[j].vel.z,chain_en[j].torsionparam.kt1,chain_en[j].kinetic.x,chain_en[j].kinetic.y,chain_en[j].kinetic.z);
    av_ben+=chain_en[j].bendparam.kb;
    av_ten+=chain_en[j].torsionparam.kt1;
    av_bang+=chain_en[j].bendparam.tetha;
    av_tang+=chain_en[j].torsionparam.phi;
  }
  fprintf(f_ben,"%.8f\n",av_ben/(beads-2));
  fprintf(f_ten,"%.8f\n",av_ten/(beads-3));
  fprintf(f_bang,"%.8f\n",av_bang/(beads-2));
  fprintf(f_tang,"%.8f\n",av_tang/(beads-3));

    
#endif

  //DBG
  //chain_struct *chain_en  = (chain_struct*  ) malloc(beads * sizeof(chain_struct));
  //torsion_energy_diagno(chain,chain_en,1.); //DBG
  //torsion_force_dbg(chain); //DBG
  //bending_energy_diagno(chain,chain_en,1.);
  //torsion_energy_diagno(chain,chain_en,1.);
  //fclose(fdbg);
  //fclose(fdbg2);

    
  /*  RUN  */
  /*********/

  //fprintf(outf,"Seed :\t%ld\n\n",idum); //DBG
  update_neighbors_VL(chain, neighbors, DISP);
    
  /*for(int i=0; i<beads;i++){ //DBG
    printf("%d %d : \t",i,neighbors[i].n); //DBG
    for(int j=0; j<neighbors[i].n; j++)  printf("%d\t",neighbors[i].index[j]); //DBG
    printf("\n"); //DBG
    }  //DBG*/

  if(printfreq>0)
    {
      print_traj(chain, 0, trj_vtf, gyration);
      tmp_Nsteps = Nsteps;
      Nsteps = ((Nsteps/printfreq))*printfreq;
      printfinal = printfreq * 100;
#ifdef VERBOSE
      if(tmp_Nsteps != Nsteps) printf("Changed Nsteps from %d to %d\n", tmp_Nsteps, Nsteps);
#endif
    }

    
    
    
  for(i = 1; i <= Nsteps; i++)
    {

      //fprintf(outf,"%d Seed :\t%ld\n\n",i,idum); //DBG
      bool continue_run = true;
#ifdef FIRST_ORDER
      continue_run = force(chain, neighbors,bridge);
      evolve_v_first_order(chain);
      evolve_x_first_order(chain, DISP);
#endif


#ifdef SECOND_ORDER
      copy_chain(chain, temp_chain);
      continue_run = force(temp_chain, neighbors,bridge);
      evolve_x(chain, temp_chain, DISP);
#endif
        
      for ( j = 0 ; j < beads ; j ++)
        {
          if (DISP[j][0]*DISP[j][0] + DISP[j][1]*DISP[j][1] + DISP[j][2]*DISP[j][2] >= 0.25 * DR2 )
	    {
	      update_neighbors_VL(chain, neighbors, DISP);
	      break;
	    }
        }

#ifdef SECOND_ORDER
      continue_run = force(chain, neighbors,bridge);
      evolve_v(chain, temp_chain);
      remove_com(chain);
#endif

      if(continue_run == false){
	fprintf(outf,"FENE bond broken at %8.2e of %8.2e steps\n Run over.\n", (double)i, (double)Nsteps); fflush(outf);
	fprintf(outf,"my seed is now %ld\n", idum);
	break;
      }
	
#ifdef ENDIAG
      //compute bead energies and average in chain_en
      /*if(i% > printfreq-aven && i%printfreq !=0){
	if(i%aven != 0){ //accumulate averages
	bending_energy_diagno(chain,chain_en);
	torsion_energy_diagno(chain,chain_en);
	}else{
	sum last time
	bending_energy_diagno(chain,chain_en);
	torsion_energy_diagno(chain,chain_en);
	average, print and empty chain_en
	double av_ben=0.0;
	double av_ten=0.0;
	for(j=1; j< beads-1; j++){
	if(i%printfreq==0){ //don't print every average (printfreq MUST be multiple or factor of aven)
	fprintf(f_ben,"%.8f\t",chain_en[j].bendparam.kb/aven);
	fprintf(f_ten,"%.8f\t",chain_en[j].torsionparam.kt1/aven);
	}
	av_ben+=chain_en[j].bendparam.kb/aven;
	av_ten+=chain_en[j].torsionparam.kt1/aven;
	chain_en[j].bendparam.kb=0.0;
	chain_en[j].torsionparam.kt1=0.0;
	}
	if(i%printfreq==0){
	fprintf(f_ben,"%.8f\n",av_ben/(beads-2));
	fprintf(f_ten,"%.8f\n",av_ten/(beads-3));
	}
	}*/
      //}
      bending_energy_diagno(chain,chain_en,alpha);
      torsion_energy_diagno(chain,chain_en,alpha);
      if(i%printfreq==0){
	av_ben=0.0;
	av_ten=0.0;
	av_bang=0.0;
	av_tang=0.0;
	for(j=1; j< beads-1; j++){
	  fprintf(f_ben,"%.8f\t",chain_en[j].bendparam.kb); //bendparam.kb); 
	  fprintf(f_ten,"%.8f\t",chain_en[j].torsionparam.kt1);
	  fprintf(f_bang,"%.8f\t",chain_en[j].bendparam.tetha); //bendparam.kb); 
	  fprintf(f_tang,"%.8f\t",chain_en[j].torsionparam.phi);
	  //	    if(j==8) fprintf(fdbg,"%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\t%.8f\n",chain_en[j].pos.x,chain_en[j].pos.y,chain_en[j].pos.z,chain_en[j].force.x,chain_en[j].force.y,chain_en[j].force.z,chain_en[j].vel.x,chain_en[j].vel.y,chain_en[j].vel.z,chain_en[j].torsionparam.kt1,chain_en[j].kinetic.x,chain_en[j].kinetic.y,chain_en[j].kinetic.z);
	  av_ben+=chain_en[j].bendparam.kb;
	  av_ten+=chain_en[j].torsionparam.kt1;
	  av_bang+=chain_en[j].bendparam.tetha;
	  av_tang+=chain_en[j].torsionparam.phi;
	}
	fprintf(f_ben,"%.8f\n",av_ben/(beads-2));
	fprintf(f_ten,"%.8f\n",av_ten/(beads-3));
	fprintf(f_bang,"%.8f\n",av_bang/(beads-2));
	fprintf(f_tang,"%.8f\n",av_tang/(beads-3));
      }
	
#endif
	
      if(printfreq>0)
        {
	  if(i%printfreq==0)
            {
	      print_traj(chain, i, trj_vtf, gyration);
	      Energy(chain, &energy,bridge);
	      print_Energy(energy, i, potential, kinetic, totenergy);
	      print_native_contact_number(chain, i, nativecon);
#ifdef PRINT_MSD
	      map_coords_on_vector(chain, str);
	      map_coords_on_vector(ref_chain, str_target);
	      //msd_file = fopen(name, "a");
	      msd = best_rmsd(str, str_target, beads);
	      fprintf(msd_file, "%d %lf\n", i, msd);
	      //fclose(msd_file);
#endif
	    }
	  if(i%printfinal==0 ) print_final_conf(pid, chain,i);
        }
        
#ifdef VERBOSE
      if(i%10000==0 && world_rank == 0) fprintf(outf,"Done %8.2e of %8.2e steps\n", (double)i, (double)Nsteps);
#endif
	
	
    }

  print_final_conf(pid, chain,i);
  free_d2t(DISP);

  for(i = 0; i < beads; i++) free_i1t(neighbors[i].index);
  fclose(outf);
#ifdef ENDIAG
  fclose(f_ben);
  fclose(f_ten);
  fclose(f_bang);
  fclose(f_tang);
#endif
  fclose(trj_vtf);
  fclose(kinetic);
  fclose(potential);
  fclose(totenergy);
  fclose(nativecon);
#ifdef PRINT_GYR
  fclose(gyration);
#endif
#ifdef PRINT_MSD
  fclose(msd_file);
#endif

  MPI_Finalize();
  //fclose(fdbg);
  return(0);
    
}

void map_coords_on_vector(chain_struct chain[], double str[][3])
{
  int i;
  for(i = 0; i < beads; i++)
    {
      str[i][0] = chain[i].pos.x;
      str[i][1] = chain[i].pos.y;
      str[i][2] = chain[i].pos.z;
    }
}
