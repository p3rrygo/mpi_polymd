## -- LT 12 - 10 - 2013 | RP 8 - 4 - 2-14 -------------------------------------------
## Makefile for program Polymer_MD
#
#
## ----------------------------------------------------------------------------------

## -- COMPILER AND FLAGS ------------------------------------------------------------
CC = mpicc  
#gccGENERAL opt flags
C_FLAGS = -O3 -Wall -std=c99 -funroll-loops 
#C_FLAGS_W = -O3 -Wall -std=c99 -funroll-loops  -fopenmp -lpthread

BINDIR = ./bin
$(shell mkdir -p $(BINDIR))

#GENERAL opt flags WITH OPENMP
#C_FLAGS = -O3 -Wall -std=c99  -funroll-loops  -fopenmp -lpthread
#C_FLAGS = -O3 -Wall -std=c99  -ftree-vectorize  -fopenmp -lpthread
#C_FLAGS = -Wall -std=c99  -fopenmp  -m64 -xSSE3 -ipo -funroll-loops

#INTEL CORE 2
#C_FLAGS = -O3 -Wall -std=c99  -funroll-loops -march=core2 -m64 -p -save-temps # intel core 2

#AMD64 opt flags
#C_FLAGS = -O3 -Wall  -std=c99 -ansi-alias -align -align -fno-alias -fno-fnalias -m64  #amd 64

#DEBUG
#C_FLAGS = -g -Wall -std=c99 

#DEBUG MAC OS X
#C_FLAGS = -g -Wall -std=c99 -save-temps #debugging on mac os X lion

## -- LIBRARIES ---------------------------------------------------------------------
C_LIB_LM = -lm  
#C_LIB_MKL =  -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread

## -- SEARCH DIRECTORIES  -----------------------------------------------------------
VPATH=./lib/
## -- HEADERS
DIR_HEADERS=./lib   #remember to link it! 
## -- GENERAL DEFINITIONS -----------------------------------------------------------
#OBJ1=my_geom.o my_memory.o  
OBJ1=my_memory.o
OBJ2=messages.o
OBJS= $(OBJ1) $(OBJ2) #randomlib.o
# Where to find GSL if local install
GSL_H=~/Work/Src/GSL_local/include
GSL_L=~/Work/Src/GSL_local/lib

all: polymerMD.x wrapper.x genetic.x

#randomlib.o :

#	g++ -std=c++0x -o randomlib.o randomlib.h

### -- LINEAR POLYMER, CRM + PIVOT, NON MKL -------------------------------------------
polymerMD.x: PolymerMD_mpi.c $(OBJS) 
	$(CC) $(C_FLAGS) -O3 -I $(DIR_HEADERS)  $^   -lm -o $(BINDIR)/$@
###
wrapper.x: wrapper_mpi.c $(OBJS)
	$(CC) $(C_FLAGS) -O3 -I $(DIR_HEADERS)  $^   -lm -o $(BINDIR)/$@
genetic.x: genetic_mpi.c $(OBJS)
	$(CC) $(C_FLAGS) -O3 -I $(DIR_HEADERS)  $^   -lm -o $(BINDIR)/$@

## -- DEPENDENCIES -------------------------------------------------------------------
#my_geom.o : my_geom.c  my_geom.h
#	$(CC) -c $(C_FLAGS) -fast   $<

my_memory.o : my_memory.c my_memory.h
	$(CC) -c $(C_FLAGS)  $< # -fast  $<
messages.o : messages.c messages.h
	$(CC) -c $(C_FLAGS)  $< #-fast  $<
#integrators.o : integrators.c integrators.h
#	$(CC) -c $(C_FLAGS)  -fast  $<
## -- PHONY CALLS ---------------------------------------------------------------------
clean:
	rm -rf *.o $(BINDIR)
